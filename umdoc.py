# -*- encoding: iso-8859-1 -*-
"""
Die Basisklasse f�r Dokumente -- die Basisfunktionalit�t ist f�r
HTML mit generiertem header (wie rootpath/index.html)
"""

import os
import random
import re
import time
import weakref

import conf
import localconf
import termincache
import umstruct

lowerLines = [
"""Nachrichten aus der Region: Der
<a href="/unimut/aktuell/" class="ml">UNiMUT aktuell</a> --
auch im E-Mail-Abo""",
'Entkommt den corporate media: '
	'<a href="http://de.indymedia.org" class="ml">de.indymedia.org</a>',
'Infos f�r lohnarbeitende Studis: <a href="http://www.students-at-work.de/"'
	' class="ml">students-at-work</a>',
]

#lowerLines = ["""<a href="/unimut/kontakt.html">Meet the UNiMUT</a> -- jetzt am Dienstag."""]
#lowerLines = ["""Endlich wieder: Unser <a href="/unimut/gags/wahlkrampf09">Wahlplakate-Ranking</a>, bis zum Wahltag st�ndig aktualisiert und von Instituten eurer Wahl."""]

#lowerLines = ["""<a href="/unimut/ersti"><span style="font-size:120%;color:green">Die aktuellen Ersti-Einf�hrungen der Fachschaften</span></a>"""]


lowerLines = ["""<strong>UNiMUT im Winterschlaf</strong> --
fast alle Inhalte hier sind
mindestens f�nf Jahre alt und vor allem historisch interessant.
Wenn du die Seite magst: Wir lesen unsere Mail noch und helfen dir gerne,
den Online-UNiMUT wiederzubeleben."""]

#lowerLines = ["""Warnung: Diese Seite ist im Winterschlaf <a href="/">(siehe auch)</a>"""]

commonHeadMaterial = """
<script src="/unimut/unimut.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/unimut/%(styleSheet)s">
<link rel="shortcut icon" href="/unimut/favicon.ico">
"""

class Document:
	"""Wir sammeln die Ausgabe in Document-Klassen.

	Im Groben werden sie von Request.dispatch ausgesucht und konstruiert.
	Sie sollen dann einen dokument-K�rper aus getData() zur�ckgeben und dann
	noch Kopf- und Fu�-HTML in getTop und getFooter.

	Zus�ztliche Header-Zeilen solen aus moreheaders rauskommen.
	content_type und status (ein tuple) sind so in etwa das, was mensch erwarten
	kann.

	Wenn encoding non-None ist, muss der getTop+getData+getFooter vor der Ausgabe
	noch entsprechend enkodiert werden.
	"""
	def __init__(self, request, sourcefile=None):
		self.request = weakref.proxy(request)
		self.data = []
		self.sourcefile = None
		self.linkline = ['<a href="/unimut/index.html">[Home]</a>'
			' <a href="/unimut/aktuell">[Aktuell]</a>']
		if request.prefs.isMaintainer:
			self.linkline.append('<a id="adminanchor" onClick='
				'"showAdminMenu()">[Admin]</a>')

		self.date = None
		self.title = None
		self.encoding = "iso-8859-1"
		self.content_type = "text/html"
		self.overtitle = None
		self.suppressTermine = 0
		self.status = (200, "Ok")
		self.moreheaders = []
		self.footstuff = []
		self.binary = None
		self.ifModifiedDate = None
		self.localHeadMaterial = []
		self.decorate = 1
		self.replaceAbbrevs = 0

		try:
			self.decorate = "nodecor" not in request.form and\
				not localconf.isMirror
		except:
			pass
		if request.requestMethod=="HEAD":
			self.headerOnly = 1
		else:
			self.headerOnly = 0
		self.lowerline = random.choice(lowerLines)

		if sourcefile:
			if os.path.isdir(sourcefile):
				sourcefile = os.path.join(sourcefile, "index.html")
			with open(sourcefile, "r", encoding=conf.defaultencoding) as f:
				tx = f.read()

			self.replaceAbbrevs = 1
			redirect = umstruct.getfield("redirect",tx)
			if redirect:
				if not redirect.startswith("http"):
					redirect = conf.homeurl+redirect
				raise conf.Redirect(redirect)
			self.data = umstruct.getfield("body",tx)
			hs = umstruct.getfield("headstuff", tx)
			if hs:
				self.localHeadMaterial.append(hs)
			if not self.data:
				if re.search("(?i)<body",tx):
					self.data = tx
					self.getTop = lambda a=self: ""
			else:
				self.title = umstruct.getfield("title", tx)
				ll = umstruct.getfield("linkline", tx)
				if ll:
					self.linkline.append(ll)
				if umstruct.getfield("nosidebar", tx):
					self.suppressTermine = 1
			self.sourcefile = sourcefile
			self.date = os.path.getmtime(sourcefile)
	

	def getMenu(self):
		with open(os.path.join(conf.headpath, "menu.html"),
				"r", encoding=conf.defaultencoding) as f:
			return f.read()

	def getContentType(self):
		if self.content_type.startswith("text"):
			return f"{self.content_type};charset={self.encoding}"
		return self.content_type

	def getResponseHeaders(self):
		"""gibt http-header-Zeilen wie von WSGI gew�nscht zur�ck.
		"""
		res = [
			("Content-type", self.getContentType())]

		if self.date:
			res.append(("Last-Modified", time.strftime(
				"%a, %d %b %Y %H:%M:%S GMT",time.gmtime(self.date))))
		res.append(("Expires", time.strftime("%a, %d %b %Y %H:%M:%S GMT",
			time.gmtime(time.time()+84000))))

		return res+[(k.strip(), v.strip()) for k, v in
			[s.split(":", 1) for s in self.moreheaders]]
	
	def getTop(self, moreHeadMaterial=None, bodyspecs=""):
		"""gibt den <HEAD>-Teil des HTML und den weitgehend festen Kopf der
		UNiMUT-Seite zur�ck
		"""
		if self.content_type!="text/html":
			return ""
		doctype = ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 '
			'Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n')
		if not self.decorate:
			return ('%s<html><head><meta name="robots" content="noindex, nofollow">'
				'<meta http-equiv="Content-Type"'
				' content="text/html;charset=%s"/>'
				'</head>\n<body%s>'%(doctype, self.encoding, bodyspecs))
		if self.title:
			title = self.title
		else:
			title = "UNiMUT -- ohne Titel"
		if self.overtitle:
			overtitle = self.overtitle
		else:
			overtitle = re.sub("<.*?>", "", title)

		reslist = ["%s<html><head>\n"
			'<meta http-equiv="Content-Type"'
				' content="text/html;charset=%s"/>'
			"<title>%s</title>"%(doctype, self.encoding, overtitle)]
		reslist.append(commonHeadMaterial%{
			"styleSheet": "umstyle.css"})
		reslist.append("\n".join(self.localHeadMaterial))
		if moreHeadMaterial:
			reslist.append(moreHeadMaterial)
		if self.request.prefs.isMaintainer:
			reslist.extend(['<script language="javascript">',
				"<!--",
				'function showAdminMenu()',
				'{',
				"	anchor = document.getElementById('adminanchor');",
				"	menu = document.getElementById('adminmenu');",
				"	menu.style.visibility = 'visible';",
				'	menu.style.top = anchor.style.top+20;',
				'	menu.style.left = anchor.style.left+30;',
				'}',
				'function hideAdminMenu()',
				'{',
				"	menu = document.getElementById('adminmenu');",
				"	menu.style.visibility = 'hidden';",
				'}',
				'-->',
				'</script>',])
		reslist.append("</HEAD>")

		linkline = " ".join(self.linkline)

		adminStuff = ""
		if self.request.prefs.isMaintainer:
			adminStuff = ('\n<div id="adminmenu" style="visibility:hidden; '
				'position:absolute;background:yellow;border:1px solid black"'
				' onClick="hideAdminMenu()">\n'
				'<p '
				'style="margin-top:2pt;margin-bottom:2pt;margin-left:2pt'
				';margin-right:2pt">\n'
				'<a href="/unimut/termine/newrec">Neuer Termin</a><br>\n'
				'<a href="/unimut/termine/expire">Terminliste neu laden</a><br>\n'
				'<a href="/unimut/todo">To Do</a><br>\n'
				'<a href="/unimut/wussi/edit">Wussis �ndern</a><br>\n'
				'<a href="/unimut/upload">Bilder oder Dokumente hochladen</a><br>\n'
				'<a href="/unimut/aktuell/newrec">Neuer UNiMUT aktuell</a><br>\n'
				'<a href="/unimut/abkuerz/newent">Neue Abk�rzung</a><br>\n'
				'<a href="/unimut/setprefs/tipform">Tipp setzen</a><br>\n'
				'<a href="/unimut/setprefs/printwand">Wandzeitung auf '
				'Kopierer drucken</a><br>\n'
				'<a href="/unimut/ersti">Erstikram</a><br>\n'
				'<a href="/unimut/termine/print?upTo=all&amp;kats=Vortreffen">'
				'Vortreffen</a><br>\n'

				'</div>')

		reslist.append(re.sub(r"(?m)^\s+","",
			f"""<body {bodyspecs}>{adminStuff}
			<p id=linkline>{linkline}</p>
			<h1 id="maintitle">{title}</h1>
			"""))
		return "\n".join(reslist)

	def getData(self):
		if self.content_type=="text/html":
			lowerline = self.lowerline or "&nbsp;"
			procContent = re.sub("%%akturl%%","/unimut/aktuell",
				re.sub("%%(base|home)url%%","/",
				self.data))
			if not self.decorate or self.suppressTermine:
				return procContent
			else:
				if self.request.prefs.isMaintainer:
					termEdit = ('<p><a href="http://%s/list?tag=unimut">'
						'Termine �ndern</a>'%conf.eventServer)
				else:
					termEdit = ""
				return ('<table border=0 cellpadding=0 cellspacing=0><tr>\n'
					'<td class="sidebar" valign="top" width="20%%">\n'
					'<p class="lowerline">%(lowerline)s</p>\n'
					'<hr>\n'
					'<div class="menu">%(menu)s</div>\n'
					'<h2>Termine</h2>'
					'%(termine)s'
					'%(termEdit)s'
					'<p align="center"><a href="http://www.iz3w.org/"><img src='
					'"/unimut/images/iz3w_100x100.gif" width="100" height="100"'
					'border="0" alt="iz3w -- zwischen Nord und S�d"></a></p>'
					'</td>'
					'<td class="content" valign="top">%(content)s</td>\n'
					'</tr></table>')%{
						"content": procContent,
						"menu": self.getMenu(),
						"lowerline": lowerline,
						"termEdit": termEdit,
						"termine": termincache.getList()}
		else:
			return self.data

	def getFooter(self):
		if self.content_type!="text/html":
			return ""

		if not self.decorate:
			return "</body>"

		reslist = ["<HR>"]
		if self.footstuff:
			reslist = reslist+self.footstuff
		else:
			reslist.append("""<p>Diese Seite darf unter der
				<a href="/unimut/dokumente/fdl.html">GNU FDL</a>
				(auch ver�ndert) weiterverbreitet werden.  N�heres in unserem
				<a href="/unimut/impressum.html">Impressum</a>.
				</p>""")
			pUri = self.request.unparsedURI
			if '?' in pUri:
				pUri = pUri+'&nodecor=1'
			else:
				pUri = pUri+'?nodecor=1'
			reslist.append('<p align="right"><a href="%s">'
				'Druckfassung</a></p>'%pUri)
		if self.date:
			reslist.append('<p align="right">Erzeugt am %s'%time.strftime("%d.%m.%Y",
				time.localtime(self.date)))
		reslist.append('<address><a href="mailto:%s">%s</a></address>'%(
			conf.maintainer,conf.maintainer))
		reslist.append("</BODY>")
		return "\n".join(reslist)

	def done(self):
		"""Diese Methode sollte gerufen werden, bevor das Ding gedruckt wird und
		nachdem das Objekt fertig aufgebaut ist.
		"""
		if self.sourcefile and not self.date:
			self.date = os.path.getmtime(self.sourcefile)
		if type(self.data)==type([]):
			self.data = "\n".join(self.data)
