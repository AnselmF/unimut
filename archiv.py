# -*- coding: iso-8859-1 -*-
"""
Die Document-Klasse hier versteht Anfragen ans Archiv der Papierunimute
"""


import re, os, glob
import conf, umdoc
	
def makeTable(ll):
	"""
	Hilfsfunktion f�rs Archiv --
	nimmt eine liste von Zeilen <Ausgabe> <Datum> (wie im Index des
	Archivs und gibt ein tupel (startyear,endyear,yeardict) zur�ck.
	yeardict ist ein dictionary mit Jahren als Schl�ssel und (Ausgabe,Datum)
	Paaren als Werte"""

	yeardict = {}
	startyear = 3000
	endyear = 0
	for l in ll:
		num,dat = l.split(" ")
		dats = dat.strip().split(".")
		year = int(dats[2])
		year = year+1900
		if year<1985:
			year = year+100
		if year<startyear: startyear=year
		if year>endyear: endyear=year
		if year in yeardict:
			yeardict[year].append([num,dats])
		else:
			yeardict[year] = [[num,dats]]
	return startyear,endyear,yeardict


class Document(umdoc.Document):
	
	def __init__(self,request,*args,**kwargs):
		umdoc.Document.__init__(*(self,request)+args, **kwargs)
		path = request.path_info
		mat = re.match("archiv/(alt.*)",path)
		if mat:
			self.altesZeug(mat.group(1))
			return
		mat = re.match("archiv/um(?P<num>[0-9]+)$",path)
		if mat:
			self.neuesZeug(mat.group("num"))
			return
		mat = re.match("archiv/um(?P<num>[0-9]+).*",path)
		if mat:
			raise conf.Redirect("/unimut/archiv/um%03d"%int(mat.group("num")))
		if re.match("archiv/inhalte(.html)?",path):
			self.inhalte()
			return
		mat = re.match("archiv/aktuell/(...)(..).html",path)
		if mat:
			raise conf.Redirect("/unimut/aktuell/show?month=%s+%s"%(
				mat.group(1).capitalize(),mat.group(2)))
		mat = re.match("archiv/browse/um(\d+).(\d+)",path)
		if mat:
			self.browse(mat.group(1),int(mat.group(2)))
			return
		self.toC()
	

	def neuesZeug(self, nummer):
		"""
		verhandelt Requests nach HTMLisierten UNiMUTen"""

		self.replaceAbbrevs = 1
		if type(nummer)==type(""):
			try:
				nummer = int(nummer)
			except ValueError:
				raise conf.Error("'%s' kann ganz unm�glich "%nummer+\
					"eine Ausgabe des UNiMUT selektieren")
		fn = os.path.join(conf.archivpath,"um%03d.html"%nummer)
		if not os.path.exists(fn):
			raise conf.Error("""Der UNiMUT %d ist noch nicht online -- wir
			brauchen noch dringend Menschen, die alte Ausgaben bearbeiten"""%nummer)
		self.sourcefile = fn

		with open(fn, "r", encoding=conf.defaultencoding) as f:
			tx = f.read()
		mat = re.match("@D8([0-9.]+)",tx)

		if tx.find("&thumbnails;")!=-1:
			tx = re.sub("&thumbnails;",self.thumbnails("%03d"%nummer),tx)

		if tx.find("&pdfthumbs;")!=-1:
			tx = re.sub("&pdfthumbs;",self.pdfthumbs("%03d"%nummer),tx)

		try:
			with open(conf.lastPublishedIssueFile) as f:
				lastPublishedIssue = int(f.read())
		except:
			lastPublishedIssue = 10000

		if nummer==1:
			self.linkline.append(
				'[Zur�ck] <a href="/archiv/um%d">[Vor]</a>'%(nummer+1))
		elif nummer==lastPublishedIssue:
			self.linkline.append(
				'<a href="/archiv/um%d">[Zur�ck]</a> [Vor]'%(
				nummer-1))
		else:
			self.linkline.append(
				'<a href="/archiv/um%d">[Zur�ck]</a> <a href="/archiv/um%d">[Vor]</a>'%(
				nummer-1,nummer+1))
		self.title = "UNiMUT %d vom %s"%(nummer,mat.group(1))
		reslist = [tx[mat.end():]]

		self.data = "\n".join(reslist)


	def thumbnails(self,iss):
		files = glob.glob(os.path.join(conf.headpath,"scans/um%s.*"%iss))
		files.sort()
		linkline = []
		for a in files:
			image = "/unimut/scans/%s?scale=16"%os.path.basename(a)
			link = "/unimut/archiv/browse/%s"%os.path.basename(a)
			linkline.append('<a href="%s" border=0><img src="%s"></a>'%(
				link,image))

		self.suppressTermine = 1
		return ('<p><a href="/unimut/scans/pdf/um%s">[als PDF]</a></p>'%iss)+\
			'<p>'+("&nbsp;".join(linkline))+'</p>'

	def pdfthumbs(self,iss):
		files = glob.glob(os.path.join(conf.rootpath,"pdfarch/thumb%s-*.png"%iss))
		files.sort()
		linkline = []
		for a in files:
			pg = int(os.path.splitext(os.path.basename(a))[0].split("-")[1])
			image = "/unimut/pdfarch/%s"%os.path.basename(a)
			linkline.append('<a href="/unimut/pdfarch/um%s.pdf#page=%d">'
				'<img src="%s"></img></a>'%(iss, pg, image))
		return '<p class="imseq">%s</p>'%("".join(linkline))

	def browse(self,id,page):
		"""
		verhandelt Requests nach gescannten UNiMUTen"""

		fn = os.path.join(conf.archivpath,"um%s.html"%id)
		if not os.path.exists(fn):
			raise conf.Error("""Der UNiMUT %s ist wahrscheinlich nicht erschienen.
			"""%id)
		self.sourcefile = fn

		scale = 4
		if "scale" in self.request.form:
			scale = int(self.request.form["scale"].value)

		with open(fn, encoding=conf.defaultencoding) as f:
			tx = f.read()
		mat = re.match("@D8([0-9.]+)",tx)
		self.title = "UNiMUT %s vom %s"%(re.sub("^0+","",id),mat.group(1))

		try:
			nummer = int(id)
			self.linkline.append(
				'<a href="/archiv/um%d">[Zur�ck]</a> <a href="/archiv/um%d">[Vor]</a>'%(
				nummer-1,nummer+1))
		except ValueError:
			pass

		import glob
		scanpat = os.path.join(conf.headpath,"scans/um%s.*"%id)
		pagelist = [a.split(".")[-1] for a in list(map(os.path.basename,glob.glob(scanpat)))]
		pages = []
		for a in pagelist:
			try:
				a = int(a)
				if a==page:
					pages.append("Seite %d"%a)
				else:
					pages.append(
						'<a href="/unimut/archiv/browse/um%s.%d?scale=%d">Seite %d</a>'%(
						id,a,scale,a))
			except ValueError:
				pass
		self.data.append('<p><a href="/unimut/archiv/um%s">Thumbnails</a>'%id)

		scales = [1,2,3,4,6,8]
		if scale<8:
			self.data.append(
				'&nbsp;<a href="/unimut/archiv/browse/um%s.%s?scale=%d">'%(
				id,page,scales[scales.index(scale)+1])+'Kleiner</a>')
		else:
			self.data.append("&nbsp;Kleiner")
		
		if scale>1:
			self.data.append(
				'&nbsp;<a href="/unimut/archiv/browse/um%s.%s?scale=%d">'%(
				id,page,scales[scales.index(scale)-1])+'Gr��er</a></p>')
		else:
			self.data.append("&nbsp;Gr��er</p>")

		self.data.append('<p>%s</p>'%("&nbsp;".join(pages)))

		self.data.append(
			'<img src="/unimut/scans/um%s.%02d?scale=%d">'%(id,page,scale))

		self.suppressTermine = 1

	def altesZeug(self,path):
		"""
		Behandelt das alte glump.  Wenn nichts Erkennbares in path steht,
		gibt es ein Inhaltsverzeichnis zur�ck"""

		fname = os.path.join(os.path.join(conf.archivpath),path)
		if os.path.exists(fname) and os.path.isfile(fname):
			title = re.sub("^([0-9])",r"Nummer \1",
				re.sub(r"UNiMUT\.","",
				re.sub(".*/","",
				re.sub("_([0-9][0-9])([0-9][0-9])([0-9][0-9])",
				r" vom \1.\2.\3",path))))
			self.sourcefile = fname
			self.title = "UNiMUT-Archiv: <BR>"+title

			reslist = ["<PRE>"]
			with open(fname, encoding=conf.defaultencoding) as f:
				reslist.append(f.read())
			reslist.append("</PRE>")
			self.data = "\n".join(reslist)
		else:
			self.title = ("UNiMUT-Archiv: <BR>Altes und Reste")
			self.sourcefile = os.path.join(conf.archivpath,"alt/index.html")
			with open(self.sourcefile, encoding=conf.defaultencoding) as f:
				self.data = f.read()

	def toC(self):
		"""
		Das Verzeichnis der Ausgaben, die Online zu haben sind."""

		indexName = os.path.join(conf.archivpath,"index")
		with open(indexName, "r", encoding=conf.defaultencoding) as f:
			ll = f.readlines()

		self.sourcefile = indexName
		self.title = "Das UNiMUT-Archiv"
		self.linkline.append('<a href="/aktuell/archiv">[Aktuell-Archiv]</a>'
			' <a href="/termine/archiv">[Termine-Archiv]</a>'
			' <a href="/archiv/inhalte.html">[Gesamtinhalt]</a>')

		self.reslist = [
			"""<P>Unten findet ihr Links zu fast allen bisher erschienenen
			UNiMUTen. Es gibt auch ein
			<a href="/archiv/inhalte">Gesamtinhaltsverzeichnis</a>.
			Ein paar Reste sind auch noch im
			<a href="/archiv/alt">ASCII-Archiv</a> zu finden, vor allem
			einige Extra-Ausgaben.</p>
			<p>Auch die Online-Geschichte im UNiMUT werden archiviert,
			und zwar getrennt im
			<a href="/aktuell/archiv">Archiv des UNiMUT aktuell</a> und
			im <a href="/termine/archiv">Termin-Archiv</a>.
			</p>"""]
		self.reslist.append("<TABLE CELLPADDING=10 BORDER=0>")
		self.reslist.append('<TR><TD VALIGN="TOP" ALIGN="LEFT">')

		startyear,endyear,yeardict = makeTable(ll)
		inline = 0
		for i in range(startyear,endyear+1):
			if i in yeardict:
				self.reslist.append('<table cellpadding=3 cellspacing=0 border=0>'+
					'<tr><th colspan=2 bgcolor="#333333"><p><br></p>'+
					'<font color="#ffffff"><h2>%d</h2></font></th></tr>'%i)
				yeardict[i].reverse()

				for numdat in yeardict[i]:
					self.reslist.append(("<tr><td>"
						'<a href="/archiv/um%(num)s">UNiMUT'
						" %(cnum)s</a></td><td>(%(dat)s)</td></tr>")%{
							'num':numdat[0],
							'cnum':re.sub(r"^\s*0+","",numdat[0]),
							'dat':"%d.%d.%02d"%tuple(map(int, numdat[1]))})
				self.reslist.append("</table>")
				inline = inline+1
				if inline<3:
					self.reslist.append('</TD><TD VALIGN="TOP" ALIGN="LEFT">')
				else:
					inline = 0
					self.reslist.append('</TD></TR>\n<TR><TD VALIGN="TOP" ALIGN="LEFT">')
		self.reslist.append("</tr></td></table>")
		self.data = "\n".join(self.reslist)
		self.suppressTermine = 1

	def inhalte(self):
		"""
		Das hier wirft einfach nur die inhalte.html-Datei ins data-attribut"""

		self.sourcefile = os.path.join(conf.archivpath,"inhalte.html")
		self.title = "UNiMUT-Archiv: <BR>Gesamtinhalt"
		with open(self.sourcefile, "r", encoding=self.defaultencoding) as f:
			self.data = f.read()
