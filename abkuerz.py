#!/usr/bin/env python
# -*- encoding: iso-8859-1 -*-
"""
Dieses Modul definiert einerseits eine Document-Klasse, die mit
Abk�rzungen umgehen kann; andererseits definiert es auch etliche
Funktionen zur Handhabung der Abk�rzungs-Datenbank und kann als
Standalone Abk�rzungslinks in Dateien einf�gen"""

import re, sys, os, time, pickle
import urllib.parse as urlparse
import conf, umdoc, umutil


def makeeditlink(str):
	return '<a href="/unimut/abkuerz/editent?editent=%s">%s</a>'%(
		urlparse.quote_plus(str),str)


class Document(umdoc.Document):

	def __init__(self, request, *args, **kwargs):
		super().__init__(request, *args, **kwargs)
		form = request.form
		path = request.path_info
		self.date = os.path.getmtime(conf.abkuedata)
		self.suppressTermine = 1
		self.replaceAbbrevs = 1
		if path.endswith("nojs"):
			self.expl(form,javascript=0)
		elif path.endswith("js"):
			self.expl(form,javascript=1)
		elif path.endswith("editent"):
			self.editent(form)
		elif path.endswith("savekey"):
			self.saveent(form)
		elif path.endswith("newent"):
			self.editent(form, newent=1)
		else:
			self.dump(form)

	def expl(self, form, javascript=0):
		"""gibt die Erkl�rung zur�ck; wenn javascript wahr ist, wird ein
		Spezialformat erzeugt, das besser in die kleinen pop-up-Fenster
		passt
		"""
		if "wort" not in form:
			raise conf.Error("Du hast nicht gesagt,"
				" was du erkl�rt haben m�chtest")
		term = form["wort"].value
		abklist = loaddb(conf.abkuedata)
		if term not in abklist:
			raise conf.Error(("Was %s bedeutet, wei� ich auch nicht."
				" Tut mir leid.")%term)
		explanation, termType = abklist[term][0], {
				0: "Abk�rzung",
				1: "Glossar",
			}[abklist[term][1]]
		if javascript:
			self.getTop = lambda term=term:"""<head><title>%s</title></head>
				<body bgcolor="white" onLoad="setTimeout('self.close()',10000)"> """%(
				"%s: %s"%(termType, term))
		else:
			self.title = "%s: %s"%(termType, term)
			if "HTTP_REFERRER" in self.request.httpHeaders:
				self.linkline.append('[<a href="%s">[Zur�ck]</a>]'%\
					self.request.httpHeaders["HTTP_REFERRER"])
			if self.request.prefs.isMaintainer:
				self.linkline.append(makeeditlink(term))
		self.data = ["<h1>%s: %s</h1><p>%s</p>"%(
			termType, term, explanation)]
		self.data.append(
			'<p>(Mehr zur <a href="/abkuerz">Abk�rzungsdatenbank</a>)</p>')


	def editent(self, form, newent=None):
		abklist = loaddb(conf.abkuedata)
		if not newent:
			targent = form["editent"].value
			if targent not in abklist:
				raise conf.Error("%s ist nicht in der Datenbank"%targent)

		self.title = "Abk�rzungsdatenbank ver�ndern"
		self.date = time.time()

		self.data = ['<FORM METHOD="POST" ACTION="/unimut/abkuerz/savekey">']
		abkChecked = ''
		if newent:
			self.data.append("<h1>Abk�rzung neu anlegen</h1>")
			self.data.append('<INPUT TYPE="TEXT" NAME="savekey" VALUE="">')
			textfield = ""
		else:
			self.data.append("<H1>%s ver�ndern</H1>"%targent)
			self.data.append('<INPUT TYPE="HIDDEN" NAME="savekey" VALUE="%s">'%
				targent)
			textfield = abklist[targent][0]
			if abklist[targent][1]:
				abkChecked = " checked"
		self.data.append("""
			<TEXTAREA WRAP="VIRTUAL" NAME="newexpl" ROWS=10 COLS=70>%s</TEXTAREA>
			"""%umutil.textareaEscape(textfield))
		self.data.append("""<INPUT TYPE="SUBMIT" VALUE="Ok"><br>
			<input type="checkbox" name="abkChecked"%s> Keine Abk�rzung
			</FORM>
			<P>Im Textfeld kann mensch Querverweise zu anderen Abk�rzungen
			in der gewohnten ^Abk�rzung-Schreibweise notieren.</P>"""%abkChecked)


	def saveent(self, form):
		"""Speichert einen Eintrag aus einem Form
		"""
		if not self.request.prefs.isMaintainer:
			raise conf.Error("Das darfst du nicht")
		db = loaddb(conf.abkuedata)
		targent = form["savekey"].value
		noAbk = "abkChecked" in form
		newtext = umutil.cleanTextarea(form["newexpl"].value)
		db[targent] = (newtext, noAbk)
		try:
			savedb(db, conf.abkuedata)
			self.dump()
		except:
			raise conf.Error(("Irgendwas ist beim Speichern schiefgegangen."
				"  Check doch mal die permissions auf %s")%conf.abkuedata)


	def dump(self, form=None):
		"""gibt die ganze Datenbank aus
		"""
		try:
			category = int(form["cat"].value)
		except (KeyError, TypeError):
			category = 0
		db = loaddb(conf.abkuedata)
		self.title = {
			0: "UNiMUT: Abk�rzungen",
			1: "UNiMUT: Glossar",
		}[category]
		self.linkline.append('<a href="/unimut/abkuerz/dump">[Abk�rzungen]</a>'
			' <a href="/unimut/abkuerz/dump?cat=1">[Glossar]</a>')
		self.data = []
		terms = [a for a in list(db.keys()) if a[0]!="_"]
		terms.sort()

		if self.request.prefs.isMaintainer:
			self.data.append("""<P><a href="abkuerz/newent">Neue
				Abk�rzung anlegen</a></P>""")

		self.data.append("<dl>")
		if self.request.prefs.isMaintainer:
			for a in filter(lambda a, db=db, cat=category: db[a][1]==cat, terms):
				self.data.append(
					"<dt>%s</dt><dd>%s</dd>"%(makeeditlink(a), db[a][0]))
		else:
			for a in filter(lambda a, db=db, cat=category: db[a][1]==cat, terms):
				self.data.append('<dt>%s</dt><dd>%s</dd>'%(a,db[a][0]))
		self.data.append("</dl>")
		self.data.append('<p><strong>Anmerkungen</strong>:'
		'<p>Diese Datenbank ist in zwei Teile geteilt, die Abk�rzungen'
		' und das Glossar.  Wechseln zwischen den zwei Teilen'
		' k�nnt ihr �ber die beiden Links in der Kopfzeile.</p>\n'
		'<p>Das <em>Linken</em> auf'
		' einzelne Eintr�ge'
		' von au�en ist ausdr�cklich erw�nscht.  Die Struktur der Links'
		' sollte aus ein paar Beispielen ausdr�cklich ersichtlich sein.'
		' Die JavaScript-Spielerei geht nat�rlich nur vom UNiMUT aus.'
		' Ebenfalls erw�nscht ist eifrige <em>Mitarbeit</em>.  Auf Anfrage gibt'
		' es ein Passwort, mit dem die Eintr�ge editiert und erg�nzt'
		' werden k�nnen.</p>')

def _makeRE(db):
	keys = list(db.keys())
	keys.sort(key=lambda item: -len(item))
	db["_R_E_"] = re.compile(r"([^^])\^(%s)"%"|".join(keys))


def loaddb(fname):
	with open(fname, "rb") as f:
		db = pickle.load(f, encoding=conf.defaultencoding)
	if "_R_E_" not in db:
		_makeRE(db)
	return db


def savedb(db, fname):
	_makeRE(db)
	with open(fname,"wb", encoding=conf.defaultencoding) as f:
		pickle.dump(db, f)


def cleanText(tx):
	return re.sub("<[^>]*>", "", re.sub(r"\s+", " ", tx)).replace(
		'"', '&quot;').replace(
		"'", "&acute;").replace(
		"^", "")


def insabkNew(text, nojs=None):
	db = loaddb(conf.abkuedata)
	return db["_R_E_"].sub(
		lambda mat, db=db: r'%s<a href="/unimut/abkuerz/nojs?'
		'''wort=%s" onMouseOver="sh('%s')" onMouseOut="hh()"'''
		' class="abk">%s</a>'%(
		mat.group(1), urlparse.quote_plus(mat.group(2)),
		cleanText(db[mat.group(2)][0]), mat.group(2)), text)


insabk = insabkNew

def findAbkues(sourceFName):
	db = loaddb(conf.abkuedata)
	with open(sourceFName, "r", encoding=conf.defaultencoding) as f:
		source = f.read()

	for key in list(db.keys()):
		pos = source.find(key)
		if pos!=-1:
			print("%30s   %-30s"%(source[pos-30:pos].replace("\n", " "),
				source[pos:pos+30].replace("\n", " ")))

if __name__=="__main__":
	findAbkues(sys.argv[1])
