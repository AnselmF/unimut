"""
Die Logik zur Auswertung evenuteller Cookies
"""

import re


class Namespace:
	def __init__(self, **kwargs):
		for k, v in kwargs.items():
			setattr(self, k, v)


def setDefaults():
	return dict(
		numArt = 5,
		fancy = None,
		leftMargin = 10,
		rightMargin = 30,
		black = None,
		menuFsize = -1,
		isMaintainer = None,
		eMailAddress = "")


#	When encoded in a string:
#		a<int>: numart
#		f:      fancy design on
#		r<int>:	right margin
#		l<int>: left margin
#		b:			black header
#		m:			fontsize in header links
#		#:			browser has maintainer rights

def interpretCookie(environ):
	try:
		rawcookie = environ["HTTP_COOKIE"]
		mat = re.search("email=([^;]*)", rawcookie)
		if mat:
			if mat.group(1)=="forget":
				eMailAddress = ""
			else:
				eMailAddress = mat.group(1)

		mat = re.search("prefs=([^;]*)", rawcookie)
		del rawcookie
		if mat:
			cookiestring = mat.group(1)
			mat = re.search("a([0-9]+)",cookiestring)
			if mat:
				numArt = int(mat.group(1))
			mat = re.search("r([0-9]+)",cookiestring)
			if mat:
				rightMargin = int(mat.group(1))
			mat = re.search("l([0-9]+)",cookiestring)
			if mat:
				leftMargin = int(mat.group(1))
			mat = re.search("m([-+0-9]+)",cookiestring)
			if mat:
				menuFsize = int(mat.group(1))
			if "f" in cookiestring:
				fancy = 1
			if "b" in cookiestring:
				black = 1
			if "#" in cookiestring:
				isMaintainer = 1
			del cookiestring
		del mat
	except KeyError:
		return {}
	del environ
	return locals()


def getPrefs(environ):
	prefs = setDefaults()
	prefs.update(interpretCookie(environ))
	return Namespace(**prefs)
		
