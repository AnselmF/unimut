# -*- encoding: iso-8859-1 -*-

from urllib import parse as urlparse
import re, os, time
import locale

import looker, conf, umdoc


def _getContext(srcF, pattern):
	"""returns some context for pattern in srcF.  This only is sensible
	for HTML or plain text input (we need to do something about all that
	pickled junk) and gives the context of the first matches of each
	term.
	"""
	contexts = []
	if os.path.exists(srcF) and os.path.getsize(srcF)<70000:
		with open(srcF, encoding=conf.defaultencoding) as f:
			data = re.sub("<[^>]*>", "", f.read()).lower()
		if data.startswith("(i"):  # quick hack for pickled stuff
			data = re.sub(r"\\x([a-fA-F0-9]{2})",
				lambda mat: chr(int(mat.group(1), 16)),
				" ".join(re.findall("'[^']*'", data)))
		terms = re.sub(r"[^\w*]+", " ", pattern).lower().split()
		for term in terms:
			termPat = re.compile(r"\b%s\b"%re.sub(r"\*", ".*?", term))
			mat = termPat.search(data)
			if mat:
				pos = mat.start()
				contexts.append(termPat.sub("<strong>%s</strong>"%mat.group(),
					re.sub("#([^&]*)&", "", data[pos-20:pos+100])))
	return "..."+"... ".join(contexts)


def _getFilesForMatches(candidates):
	fileLooker = looker.Looker(
		os.path.join(conf.indexPath, "files.list"),
		encoding=conf.defaultencoding)
	res = []
	for cand in candidates:
		for w in fileLooker.look("%06d"%cand):
			res.append(w.strip().split("\t")[1:])
	for rec in res:
		rec[3] = float(rec[3])
	return res


def getMatches(searchString):
	"""returns matching files for a conjunctive search for all terms
	in searchString, based on the files generated by collectWords.
	"""
	terms = [(term.endswith("*") and term[:-1]) or term+"\t" for term in
		re.sub(r"[^\w*]+", " ", searchString).lower().split(" ")]
	wordLooker = looker.Looker(
		os.path.join(conf.indexPath, "words.index"),
		encoding=conf.defaultencoding)

	removeWordPat = re.compile("^[^\t]*\t")
	matches = []
	for term in terms:
		matches.extend(removeWordPat.sub(" ", ln) for ln in wordLooker.look(term))
	matches.sort(key=lambda w: len(w))
	if not matches:
		return None
	firstmatch = matches[0]

	candidates = set(map(int, firstmatch.split()))
	for match in matches[1:]:
		candidates.intersection_update(
			set(list(map(int, match.split()))))
		if not candidates:
			break
	else:
		return _getFilesForMatches(candidates)
	return None

class Document(umdoc.Document):
	
	def __init__(self, request):
		locale.setlocale(locale.LC_ALL, "de_DE")
		umdoc.Document.__init__(self,request)
		self._parseForm(request.form)
		if self.pattern is None:
			self._serveForm()
		else:
			self.searchFor(request.form)

	def _parseForm(self, form):
		self.pattern = None
		if "pattern" in form:
			self.pattern = form["pattern"].value

		self.showrange = (0, 10)
		if "showrange" in form:
			try:
				self.showrange = list(map(int,
					re.match(r"(\d+)-(\d+)", form["showrange"].value).groups()))
			except ValueError:
				raise conf.ParameterError("showrange")

		self.excludeStrings = []
		if "suppress" in form:
			self.excludeStrings.append(form["suppress"].value)

	def _formatMatches(self, matches):
		numMatches = len(matches)
		self.data.append("<p>Ergebnisse %d bis %d von %d</p>"%(
			self.showrange[0]+1, min(numMatches, self.showrange[1]),
				numMatches))
		self.data.append("<ul>")
		for srcF, targUrl, title, mtime in matches[
			self.showrange[0]:self.showrange[1]]:
			if targUrl.endswith("?"):
				targUrl = "%ssearchWords=%s"%(targUrl,
					urlparse.quote_plus(self.pattern))
			self.data.append('<li><a href="%s">%s</a>'
			'<br><font size="-1">%s</font></li>'%(
			targUrl, title, _getContext(srcF, self.pattern)))
		self.data.append("</ul>")

	def _getCurrentSearch(self):
		exclusions = ["suppress=%s"%a for a in self.excludeStrings]
		return "&".join(["pattern=%s"%urlparse.quote_plus(self.pattern)]+
			exclusions)

	def _addFurtherResultLinks(self, numMatches):
		self.data.append("<p>Weitere Ergebnisse:")
		for ind in range(0, numMatches, 10):
			self.data.append('<a href="/unimut/search?%s&'
				'showrange=%d-%d">%d-%d</a> '%(self._getCurrentSearch(),
				ind, ind+10, ind+1, min(ind+10, numMatches+1)))
		self.data.append("</p>")

	def searchFor(self, form):
		self.date = time.time()
		self.title = "Ergebnis der Suche nach %s"%self.pattern
		self.data.append(self._getBigForm())
		
		matches = getMatches(self.pattern)
		if not matches:
			self.data.append("<p>Nichts passendes gefunden.</p>")
			return
		for excludeStr in self.excludeStrings:
			matches = list(filter(lambda a, s=excludeStr:a[1].find(s)==-1,
				matches))
		
		matches.sort(key=lambda w:w[3])
		self._formatMatches(matches)
		if len(matches)>10:
			self._addFurtherResultLinks(len(matches))

		self.data.append('<p>Suchen auf <a href="http://www.google.de/search'
			'?as_sitesearch=unimut.stura.uni-heidelberg.de&as_q=%s">Google'
			' (UNiMUT)</a> oder <a href="http://www.google.de/search'
			'?as_q=%s">Google global</a></p>'%(urlparse.quote_plus(self.pattern),
			urlparse.quote_plus(self.pattern)))

	def _getBigForm(self):
		pattern = ""
		noterms = ""
		if self.pattern is not None:
			pattern = re.sub(r"[^\w*]+", " ", self.pattern.lower())
		if "termine" in self.excludeStrings:
			noterms = ' checked'

		return ('<FORM METHOD="GET" ACTION="/unimut/search">\n'
			'<P>Suchmuster: <input type="text" name="pattern"'
			' value="%(pattern)s" size=40><BR>\n'
			'<input type="checkbox" name="suppress" value="termine"'
			'%(noterms)s> Nicht in Terminen suchen</p>\n'
			'<P ALIGN="RIGHT"><input type="Submit" value="Suchen"></P>\n'
			'</FORM>'%{
				'pattern': pattern,
				'noterms': noterms,
			})

	def _serveForm(self):
		self.date = time.time()
		self.title = "Suchen im UNiMUT"
		self.data = (
			'<TABLE BORDER=0 CELLPADDING=3 ALIGN="CENTER"><TR><TD>'
			'%(form)s</TD></TR></TABLE>'
			'<p>Unsere Suchmaschine ist etwas simple-minded.  Gebt einfach'
			' alle W�rter ein, nach denen ihr sucht, und ihr bekommt alle'
			' Dokumente zur�ck, in denen diese W�rter vorkommen.'
			' Gro�-/Kleinschreibung wird ignoriert.'
			' Wenn ihr ein * hinter ein Wort setzt, matcht es auf alle'
			' W�rter, die mit diesem Wort anfangen (d.h. Schaf* passt'
			' auf Schafhirte, Schafzucht, schaffen usf.).  Das wars.</p>\n'
			' <p>Wer Tricks machen will, kann es mit einer'
			' <form method="get" action="http://www.google.de/search">'
			'<input type="hidden" name="as_sitesearch"'
			'value="unimut.stura.uni-heidelberg.de">'
			'<input type="text" name="as_q" width="60">'
			'<input type="submit" value="Google-Suche"> probieren.  Google'
			' ist aber weniger aktuell als unsere eigene Maschine und'
			' indiziert auch nicht alles.</p>'
			)%{
				"form": self._getBigForm(),
			}


if __name__=="__main__":
	locale.setlocale(locale.LC_ALL, "de_DE")
	m = getMatches("Filbinger Studiengeb�hren")
	print(m)
