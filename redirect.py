# -*- coding: iso-8859-1 -*-
"""
Eine ziemlich schlichte Ableitung von umdoc.Document, die lediglich
auf eine andere URL umleitet.
"""

import time

import conf
import umdoc


class Document(umdoc.Document):

	def __init__(self, request, newurl=None, *args, **kwargs):
		super().__init__(request, *args, **kwargs)

		if not newurl:
			raise conf.Error("Umleitung ohne neue URL")
		if "://" not in newurl:
			newurl = conf.homeurl+newurl

		self.date = time.time()
		self.title = "Nicht mehr hier"
		self.data = """<h1>Nicht mehr hier...</h1>
			<p>aber <a href="%s">hier</a>.  Eigentlich solltest du das
			hier gar nicht sehen, weil dich dein Browser gleich
			weiterschicken sollte.  Wenn er das nicht getan hat, kannst du ja
			manuell weiterklicken.</p>"""%newurl
		self.status = (301, "Moved Permanently")
		self.moreheaders.append("Location: %s"%newurl)
