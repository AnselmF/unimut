# -*- encoding: iso-8859-1 -*-
"""
Das hier verhandelt die Schwerpunkte -- es ist ein bisschen was von einer
Krankheit, weil ich zwei bestehende Klassen roh in das Document-Konzept
geklebt habe.
"""

import glob
import os
import pickle
import re
import time
import urllib.request as urlrequest
import urllib.parse as urlparse

import conf,umdoc

	
class SchwerpunktURL:
	"""repr�sentiert eine URL samt Kommentar
	"""
	def __init__(self, form=None, url=None, forMaintainer=False):
		self.forMaintainer = forMaintainer
		self.elter = ""
		self.url = ""
		self.kommentar = ""
		try:
			self.elter = form["titel"].value
			self.url = form["url"].value
			self.kommentar = form["kommentar"].value
			try:
				mat = re.match(r"(\d+)\.(\d+)\.(\d+)",form["datum"].value)
				self.datumcache = time.mktime((int(mat.group(3)),int(mat.group(2)),
					int(mat.group(1)),0,0,0,-1,-1,-1))
			except:	
				pass
		except:
			pass
		if url:
			self.url = url
		self.normalise()

	def asText(self):
		return "%s (%s)"%(self.kommentar, self.url)

	def datum(self):
		if "datumcache" in self.__dict__ and self.datumcache!=None:
			return self.datumcache
		url = self.url
		if url=="":
			return 0
		if url[0]=="/":
			mat = re.search(r"rec=(\d+)",url)
			if mat:
				self.datumcache = int(mat.group(1))
				return self.datumcache
			mat = re.search(r"archiv.*?(\d+)",url)
			if mat:
				self.datumcache = os.stat(os.path.join(conf.headpath,
					"archiv.db/um%s.html"%mat.group(1)))[8]
				return self.datumcache
		try:
			f = urlrequest.urlopen(url)
			self.datumcache = time.mktime(f.info().getdate("Last-Modified"))
		except (TypeError,IOError):
			self.datumcache = time.time()
		return self.datumcache

	def normalise(self):
		self.url = re.sub("^http://unimut.stura.uni-heidelberg.de(/unimut)?", "",
			self.url.strip())
		self.kommentar = self.kommentar.strip()

	def __str__(self):
		showurl = self.url
		if len(self.url)>35:
			showurl = re.sub(r"http://(www\.)?","",self.url)[:15]+"..."+self.url[-17:]
		retstr = """<tr><td valign="top"><a href="%s">%s</a></td>
			<td valign="top">%s</td>"""%(
			self.url,showurl,self.kommentar)

		if getattr(self, "forMaintainer", False):
			addit = ""
			if "datumcache" in self.__dict__ and self.datumcache!=None:
				addit = '&datum=%s"'%(
					time.strftime("%d.%m.%Y",time.localtime(self.datumcache)))
			retstr = retstr+"""<td>
				<a href="/unimut/schwerpunkt/urlaendern?url=%s&titel=%s&kommentar=%s%s">�ndern</a>
				</td>"""%(urlparse.quote_plus(self.url),
				urlparse.quote_plus(self.elter),
				urlparse.quote_plus(self.kommentar),addit)
		return retstr

	
	def __cmp__(self,other):
		d1, d2 = self.datum(), other.datum()
		if d1<d2:
			return -1
		elif d1==d2:
			return 0
		else:
			return 1

	

class Schwerpunkt:
	"""
	Diese Klasse sollte alles wissen, was mensch f�r einen Schwerpunkt
	braucht:  Einen Einleitungstext und eine Liste von SchwerpunktURLs"""

	def __init__(self, form=None, titel=None, forMaintainer=False):
		self.forMaintainer = forMaintainer
		if form:
			if "origtitel" in form:
				self.titel = form["origtitel"].value
			elif "titel" in form:
				self.titel = form["titel"].value
			else:
				self.titel = None
		elif titel:
			self.titel = titel
		else:
			self.titel = None
		self.einleitung = ""
		self.urls = []
		self.load()
	

	def asText(self):
		return "Schwerpunkt: %s\n\n%s\n\n%s"%(self.titel, self.einleitung,
			"\n".join([u.asText() for u in self.urls]))

	def save(self):	
		with open(self.fname(), "wb") as f:
			pickle.dump(self, f, encoding=conf.defaultencoding)
		try:
			os.chmod(self.fname(),0o666)
		except os.error:
			pass
	
	def load(self):
		try:
			with open(self.fname(), "rb") as f:
				mycont = pickle.load(f, encoding=conf.defaultencoding)
			self.urls = mycont.urls
			self.einleitung = mycont.einleitung
		except:
			pass

	def form_speichern(self, form):
		if not self.forMaintainer:
			raise conf.Error("Du hast kein Schreibrecht")
		titel = form["titel"].value
		origtitel = form["origtitel"].value
		einleitung = form["einleitung"].value
		if titel!=origtitel:
			try:
				os.unlink(self.fname())
			except os.error:
				pass
		self.titel = titel
		self.einleitung = einleitung
		self.save()

	def urlspeichern(self,form):
		if not self.forMaintainer:
			raise conf.Error("Du hast kein Schreibrecht")
		nu = SchwerpunktURL(form, forMaintainer=True)
		for a in self.urls:
			if a.url==nu.url:
				self.urls[self.urls.index(a)] = nu
				break
		else:
			self.urls.append(nu)
		self.urls.sort()
		self.save()

	def urlloeschen(self,form):
		if not self.forMaintainer:
			raise conf.Error("Du hast kein Schreibrecht")
		nu = SchwerpunktURL(form, forMaintainer=True)
		for a in self.urls:
			if a.url==nu.url:
				del self.urls[self.urls.index(a)]
				self.save()
				break
		else:
			raise conf.Error("Diese URL ist gar nicht im Schwerpunkt")

	def fname(self):
		return os.path.join(conf.spdbpath,re.sub("/","%2F",
			urlparse.quote_plus(self.titel)+".sp"))



class Document(umdoc.Document):
	
	def __init__(self, request, *args, **kwargs):
		umdoc.Document.__init__(self, request, *args, **kwargs)

		path = request.path_info
		if path.endswith("anzeigen"):
			raise conf.Redirect("/unimut/schwerpunkt/anzeigen/%s"%request.form["titel"].value)
		elif re.search("anzeigen/.*", path):
			self.schwerpunkt = Schwerpunkt(
				titel=urlparse.unquote_plus(re.sub(".*/anzeigen/", "", path)),
				forMaintainer=request.prefs.isMaintainer)
			self.render()
		elif path.endswith("neu"):
			self.schwerpunkt = Schwerpunkt(form=request.form,
				forMaintainer=request.prefs.isMaintainer)
			self.editSchwerpunkt()
		elif path.endswith("anlegen"):
			self.schwerpunkt = Schwerpunkt(form=request.form,
				forMaintainer=request.prefs.isMaintainer)
			self.schwerpunkt.form_speichern(request.form)
			self.render()
		elif path.endswith("urlaendern"):
			self.surl = SchwerpunktURL(form=request.form,
				forMaintainer=request.prefs.isMaintainer)
			self.editSurl()
		elif path.endswith("urlspeichern"):
			self.schwerpunkt = Schwerpunkt(form=request.form,
				forMaintainer=request.prefs.isMaintainer)
			self.schwerpunkt.urlspeichern(request.form)
			self.render()
		elif path.endswith("urlloeschen"):
			self.schwerpunkt = Schwerpunkt(form=request.form,
				forMaintainer=request.prefs.isMaintainer)
			self.schwerpunkt.urlloeschen(request.form)
			self.render()
		else:
			self.index()

	def editSurl(self):
		if "datumcache" in self.surl.__dict__ and \
			self.surl.datumcache!=None:
			addit = """<p>Datum:
				<input name="datum" type="text" value="%s"></p>"""%(
				time.strftime("%d.%m.%Y",time.localtime(self.surl.datumcache)))
		else:
			addit = ""
		self.title = "URL zu %s hinzuf�gen/�ndern"%self.surl.elter
		self.linkline.append('<a href="/unimut/schwerpunkt/">Schwerpunkte</a>')

		self.data = """
			<form method="get" action="/unimut/schwerpunkt/urlspeichern">
			<p>URL: <input type="text" name="url" value="%s" width=70></p>
			<p>Kommentar:
			<textarea name="kommentar" cols=60 rows=3 wrap="virtual">%s</textarea></p>
			<input type="hidden" name="titel" value="%s">
			%s
			<input type="submit" value="Speichern">
			</form>"""%(self.surl.url,self.surl.kommentar,self.surl.elter,addit)+\
			"""<p align="right">
			<a href="/unimut/schwerpunkt/urlloeschen?url=%s&titel=%s">L�schen</a>
			</p>"""%(
			urlparse.quote_plus(self.surl.url),urlparse.quote_plus(self.surl.elter))

	def editSchwerpunkt(self):
		if self.schwerpunkt.titel:
			titel = self.schwerpunkt.titel
		else:
			titel = "(neu)"
		self.title = "Schwerpunkt %s anlegen/�ndern"%titel
		self.linkline.append('<a href="/unimut/schwerpunkt/">Schwerpunkte</a>')

		self.data.append(
			"""<form method="post" action="/unimut/schwerpunkt/anlegen">
			<p>Titel: <input type="text" name="titel" value="%s"></p>
			<p>Einleitung:
			<textarea name="einleitung" cols=60 rows=10 wrap="virtual">%s
			</textarea></p>
			<input type="hidden" name="origtitel" value="%s">
			<input type="submit" value="Speichern">
			</form>"""%(titel,self.schwerpunkt.einleitung,titel))
	
	def render(self):
		self.title = "Schwerpunkt: %s"%self.schwerpunkt.titel
		self.linkline.append('<a href="/unimut/schwerpunkt">Schwerpunkte</a>')
		addit = ""
		if self.request.prefs.isMaintainer:
			addit = """<p align="left">
				<a href="/unimut/schwerpunkt/neu?titel=%s">�ndern</a>
				<a href="/unimut/schwerpunkt/urlaendern?titel=%s">URL
				hinzuf�gen</a>
				</p>"""%(urlparse.quote_plus(self.schwerpunkt.titel),
				urlparse.quote_plus(self.schwerpunkt.titel))
		self.replaceAbbrevs = 1
		self.data.append("""<P>%s</P>%s
			<table border=0 cellspacing=10>%s</table>"""%(self.schwerpunkt.einleitung
				,addit, "\n".join("<TR>%s</TR>"%a for a in self.schwerpunkt.urls)))

	def index(self):
		self.title = "Schwerpunkte"
		self.replaceAbbrevs = 1

		spe = [urlparse.unquote_plus(
				re.sub(".*/", "", a),
				encoding=conf.defaultencoding)[:-3]
			for a in glob.glob(os.path.join(conf.spdbpath,"*.sp"))]
		self.data.append("<ul>%s</ul>"%(
			"\n".join('<li><a href="/unimut/schwerpunkt/anzeigen/'
				'%s">Schwerpunkt %s</a></li>'%(
					urlparse.quote_plus(a),a) for a in spe)))
		if self.request.prefs.isMaintainer:
			self.data.append("""<p align="right">
				<a href="/unimut/schwerpunkt/neu">Neuen Schwerpunkt
				anlegen</a></p>""")
