#!/usr/bin/env python
# -*- encoding: iso-8859-1 -*-
"""
Hiermit kann man eine einfache To-Do-Liste verwalten
"""

import re, os, time, pickle
import conf, umdoc

class TodoItems:
	"""eine Klasse, die letztlich nicht viel mehr als ein persistentes
	Dictionary darstellt und dabei nicht ganz race-freies Locking macht.
	"""
	def __init__(self, srcFile=os.path.join(conf.headpath,
			"writable", "todo.pickle")):
		self.srcFile = srcFile
		self.lockFile = srcFile+".lock"
		self.checkLock()
		try:
			with open(srcFile, "rb") as f:
				self.todoItems = pickle.load(f, encoding=conf.defaultencoding)
		except IOError:
			self.todoItems = {}
		self.get = self.todoItems.get
		self.values = self.todoItems.values
		self.keys = self.todoItems.keys
	
	def get(self, key):
		return self.todoItems[key]
	
	def delete(self, key):
		del self.todoItems[key]
	
	def set(self, key, text):
		self.todoItems[key] = text

	def newKey(self):
		while 1:
			newKey = int(time.time())
			if newKey not in self.todoItems:
				self.todoItems[newKey] = ""
				break
			time.sleep(1)
		return newKey
	
	def save(self):
		with open(self.srcFile, "wb") as f:
			pickle.dump(self.todoItems, f, encoding=conf.defaultencoding)

	def __iter__(self):
		return iter(self.todoItems.items())

	def checkLock(self):
		if os.path.exists(self.lockFile):
			if time.time()-os.path.getctime(self.lockFile)>300:
				os.remove(self.lockFile)
			else:
				raise conf.Error("Todo ist gerade gelockt")
		with open(self.lockFile, "w") as f:
			f.write(" ")
	
	def __del__(self):
		os.remove(self.lockFile)


def formatTodo(someText):
	if someText.startswith("http://"):
		someText = re.sub("(?m)^(http://[^\s]+?)($|\s|[.,:;]\s)",
			r'<a href="\1">\1</a>\2', someText)
	return someText

class Document(umdoc.Document):

	def __init__(self,request,*args,**kwargs):
		umdoc.Document.__init__(*(self, request)+args, **kwargs)
		if not request.prefs.isMaintainer:
			raise conf.PermissionError("Das hier ist privat")
		self.todoItems = TodoItems()
		try:
			if request.path_info=="%s/newrec"%__name__:
				self.createNew()
			elif request.path_info=="%s/saverec"%__name__:
				self.saveRec(request.form)
			elif request.path_info=="%s/editrec"%__name__:
				self.editRec(key=request.form["key"].value)
			elif request.path_info=="%s/delrec"%__name__:
				self.delRec(key=request.form["key"].value)
			else:
				self.show()
		finally:
			del self.todoItems

	def createNew(self):
		self.editRec(self.todoItems.newKey())

	def saveRec(self, form):
		self.title = "Todo gespeichert"
		self.todoItems.set(int(form["oldhash"].value),
			form["todotext"].value)
		self.todoItems.save()
		raise conf.Redirect("/unimut/%s"%__name__)

	def delRec(self, key):
		key = int(key)
		self.title = "Todo %d gel�scht"%key
		self.todoItems.delete(key)
		self.todoItems.save()
		raise conf.Redirect("/unimut/%s"%__name__)
		
	def editRec(self, key):
		key = int(key)
		self.title = "Todo %s �ndern"%key
		self.data.append('<form action="/unimut/%(modname)s/saverec"'
			'method="post">\n'
			'<input type="hidden" name="oldhash" value="%(oldhash)d">'
			'<textarea name="todotext" cols=50 style="width:100%%"'
			' rows=4 wrap="virtual">%(todotext)s</textarea>'
			'<br><input type="submit" value="Ok"></form>'%{
				'modname': __name__,
				'oldhash': key,
				'todotext': self.todoItems.get(key),
			})
	
	def show(self):
		self.title = "Offene Sachen"
		self.linkline.append('<a href="/unimut/%s/newrec">[Neuer Eintrag]</a>'%
			__name__)
		keys = list(self.todoItems.keys())
		keys.sort()
		for key in keys:
			self.data.append("<pre>%s</pre>\n"%formatTodo(
				self.todoItems.get(key)))
			self.data.append('<p><a href="/unimut/%(modname)s/editrec?key=%(key)d"'
				'>[�ndern]</a>  <a href="/unimut/%(modname)s/delrec?key=%(key)d"'
				'>[L�schen]</a></p><hr>'%{"key": key, "modname": __name__})
			
