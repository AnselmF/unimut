# -*- encoding: iso-8859-1 -*-
"""
Ein paar n�tzliche Funktionen und anderes Zeug, das von
mehreren Modulen gebraucht wird
"""

import re
import os
import time
from html import entities
from urllib import parse as urlparse

import conf

fullmonth = ["Januar","Februar","M�rz","April","Mai","Juni","Juli",
		"August","September","Oktober","November","Dezember"]

shortmonth = ["Jan","Feb","Mar","Apr","Mai","Jun","Jul","Aug","Sep",
	"Okt","Nov","Dez"]

shortdict = {"Jan":0,"Feb":1,"Mar":2,"Apr":3,"Mai":4,"May":5,
	"Jun":5,"Jul":6,"Aug":7,"Sep":8,"Okt":9,"Oct":9,"Nov":10,
	"Dez":11,"Dec":11}


class Month:
	def __init__(self,monthcode=None,date=None,unixtime=None):
		"""
		monthcode ist was wie "Mar 2000", date ein Tupel (1 99) f�r
		Januar 1999"""

		try:
			if monthcode:
				month,year = monthcode.split()
				month = shortdict[month.capitalize()]+1
			elif date:
				month,year = date
			elif unixtime:
				year,month = time.localtime(unixtime)[:2]

			year = int(year)
			if year<88:
				year = year+2000
			if year<1987:
				year = year+1900

			if month<12:
				self.enddate = time.mktime((year,month+1,1,0,0,0,-1,-1,-1))-1
			else:
				self.enddate = time.mktime((year+1,1,1,0,0,0,-1,-1,-1))-1
			self.startdate = time.mktime((year,month,1,0,0,0,-1,-1,-1))
			self.year,self.month = year,month
		except:
			if monthcode:
				raise conf.Error("Ung�ltiges Datumsformat %s"%monthcode)
			else:
				raise conf.Error("Ung�ltiges Datumsformat")

	def timeRange(self):
		return self.startdate,self.enddate

	def timeRangeStr(self):
		return "%d-%d"%(self.startdate,self.enddate)

	def shortName(self):
		return "%s %02d"%(shortmonth[self.month-1],self.year%100)
	
	def longName(self):
		return "%s %04d"%(fullmonth[self.month-1],self.year)

	def urlEncoded(self):
		return urlparse.quote_plus(self.shortName())

	def previous(self):
		month,year = self.month-1,self.year
		if month==0:
			month = 12
			year = year-1
		return Month(date=(month,year))

	def __next__(self):
		month,year = self.month+1,self.year
		if month==13:
			month = 1
			year = year+1
		return Month(date=(month,year))


	def __repr__(self):
		return "Month(date=(%d,%d))"%(self.month,self.year)
	

	def __str__(self):
		return self.shortName()


def urljoin(url1, url2):
	"""
	Noch ein Versuch, url1 und url2 sinnvoll zu verbinden."""

	u1 = urlparse.urlparse(url1)
	u2 = urlparse.urlparse(url2)
	if u2[0]:
		# url2 hat scheme und ist also offentichtlich komplett
		return url2
	if u2[1]:
		# urlparse deklariert alles ohne einen / davor als host, was
		# wenigstens hier Quatsch ist
		u2 = ('','',os.path.join(u2[1],u2[2]),u2[3],u2[4],u2[5])
	
	# die zweite URL k�nnte ein Pfad sein -- oder nur ein Tag
	if not u2[2] and u2[5]:
		return urlparse.urlunparse((u1[0],u1[1],u1[2],u1[3],u1[4],u2[5]))
	if not u2[2]:
		u2 = (u2[0],u2[1],"/",u2[3],u2[4],u2[5])
	newpath = os.path.join(os.path.dirname(u1[2]),u2[2])
	# extra ugly hack: Wenn URL1 nicht auf einen / endet, aber kein
	# Punkt im Dateinamen ist, h�nge ich einen slash an, damit
	# os.path.dirname das richtige tut.  Wie soll das richtig
	# gehen?
	# Ich mach das erstmal nicht mehr...
	#if not u1[2].endswith("/"):
	#	ex = os.path.splitext(u1[2])[1]
	#	if ex=="" or len(ex)>5:
	#		newpath = os.path.join(u1[2], u2[2])

	return urlparse.urlunparse((u1[0],u1[1],re.sub("^/","",newpath),
		u2[3],u2[4],u2[5]))


def uniq(aList):
	"""gibt eine Liste aller Eintr�ge in aList ohne Duplikate zur�ck
	"""
	d = {}
	for a in aList:
		d[a] = 1
	return list(d.keys())


def deEntifyMatch(matob):
	"""gibt den Inhalt von matob zur�ck, wobei alle bekannten Entities
	durch ihre Latin-1-�quivalente ersetzt worden sind (entities sind dabei
	einfach matches von &\w+;).  Unbekannte entities werden nicht ersetzt.
	Warnung: entifyMatch und deEntify sind nicht notwendig Umkehrfunktionen
	(sie sind es nur, wenn der urspr�ngliche Match bereits komplett
	entifiziert war).
	"""
	return re.sub(r"&(\w+);", lambda mat:
		entities.entitydefs.get(mat.group(1), mat.group()), matob.group())

# entifyMatch braucht vorgekaute Daten aus htmlentitydefs -- ich mach
# das hier mit der Fu�g�ngerInnenmethode
_isoToEntDict = {}
for key, val in list(entities.entitydefs.items()):
	_isoToEntDict[val] = key
del _isoToEntDict["<"]
del _isoToEntDict[">"]
del _isoToEntDict['"']
_entifyPat = re.compile("|".join(list(_isoToEntDict.keys())))

def entifyMatch(matob):
	"""gibt den Inhalt von matob zur�ck, wobei alle Zeichen, die in
	htmlentitydefs als Werte vorkommen, durch HTML-Entities ersetzt
	werden.  matob.group() muss in Latin-1 sein.  Da der Kram auf
	den *Inhalt* von Tags anwendbar sein soll, aber die Tags nicht
	zerschie�en, wenn ein kompletter Tag-Match reinkommt, werden < und
	> ausgenommen.
	"""
	return _entifyPat.sub(lambda mat: "&%s;"%_isoToEntDict[mat.group()],
		matob.group())

def textareaEscape(tx):
	"""in textareas m�ssen alle Vorkommen von ^ durch ^^ ersetzt werden, um
	sie vor der Ersetzung insabk zu bewahren.  Au�erdem gehen wir durch
	alle URLs und ersetzen &xxx; durch ihre latin-1-�quivalente (wir k�nnten
	das auch sonst machen -- aber wir wollen keine Probleme durch wilde
	entities, die irgendwer in den Text geschrieben hat.
	"""
	return re.sub("<a href=[^>]*>", deEntifyMatch,
		re.sub(r"\^", r"^^", tx))


def cleanTextarea(tx):
	"""s�ubert Kram aus allen m�glichen Textareas.

	Zum einen k�mmern wir uns um Ampersands und Entities in Links
	(in einer etwas d�mlichen Art), dann machen wir die Operation
	von textareaEscape r�ckg�ngig, und dann fangen wir noch die
	bl�den Zeichen von cp1256 (oder was immer) ab.
	"""
	return re.sub("<a href=[^>]*>", entifyMatch,
		re.sub(r"\^\^", r"^", tx)).replace("\x84", '"'
		).replace("\x93", '"'
		).replace("\x82", "'"
		).replace("\x96", "--"
		).replace("\x85", "..."
		).replace("\x92", "'")



def getPathInfoParameters(pathInfo, globalPath):
	return re.sub("^/", "", pathInfo[len(globalPath):])


def getCurrentYear():
	return time.localtime(time.time())[0]


def killHtml(someText):
	"""Warning: we assume iso-8859-1 output here!  This function tries
	to get something usable to embed within XML.  So there.
	"""
	return re.sub("&([^;]*);", lambda mat:entities.entitydefs.get(
			mat.group(1), mat.group(1)[0]),
		re.sub("<[^>]*>", "", someText)
			).replace("&", "&amp;"
			).replace(">", "&gt;"
			).replace("<", "&lt;")


def getAktuellTip():
	try:
		with open(conf.aktuTippath, "r", conf.defaultencoding) as f:
			return f.read()
	except IOError:
		return ""


def setAktuellTip(tipText):
	try:
		with open(conf.aktuTippath, "w", conf.defaultencoding) as f:
			f.write(tipText)
	except IOError:
		raise conf.Error("Konnte %s nicht schreiben."%conf.aktuTippath)


if __name__=="__main__":
	print(urljoin("http://www.rzuser.uni-heidelberg.de/~gp4", "Button.gif"))
