# -*- encoding: iso-8859-1 -*-
"""
Das hier tut alles, was mit UM aktuells zu tun hat.
"""

import re, string, sys, os, time, operator, pickle, bisect, glob
import conf
from urllib import parse as urlparse
from functools import reduce

import umdoc, umstruct, umutil
import termincache


def _ampEscape(htmlStr):
	"""versucht, in htmlStr Ampersands sinnvoll zu escapen.  Die Regel
	dabei ist: ein & wird nur dann ein &amp;, wenn es nicht in einem
	Tag steht.
	"""
	return re.sub(">[^>]+<", lambda matob: matob.group().replace("&", "&amp;"),
		htmlStr)


def makeCaches():
	"""sammelt Titel und Querverweise der UNiMUT aktuells uns schreibt
	die Information in diverse Caches.
	"""
	flist = glob.glob(os.path.join(conf.aktuellpath, "[0-9]*"))
	titdict = {}
	crossRefs = {}
	for a in flist:
		with open(a, "r", encoding=conf.defaultencoding) as f:
			tx = f.read()
		try:
			showArt = umstruct.getfield("showme", tx)
			if not showArt is None and showArt.strip()=="0":
				continue
			titdict[int(a.split("/")[-1])] = \
				[umstruct.getfield("date", tx).strip(),
				umstruct.getfield("title", tx).strip(),
				(umstruct.getfield("subtitle", tx) or "").strip()]
			source = a.split("/")[-1]
# Hier noch vorsehungen f�r tags machen.
			for target in re.findall(r'href="[^"]*aktuell[^0-9]+([0-9]{3,})"',
				umstruct.getfield("body", tx)):
				crossRefs.setdefault(target, []).append(source)
		except AttributeError:
			raise
	
	# Write title cache
	titlecache = os.path.join(conf.aktuellpath, "titles.cache")
	with open(titlecache, "wb+") as f:
		pickle.dump(titdict, f, encoding=conf.defaultencoding)

	try: os.chmod(titlecache,0o666)
	except os.error: pass

	# Write directory cache
	flist = list(map(int, list(titdict.keys())))
	flist.sort()
	dircache = os.path.join(conf.aktuellpath, "dir.cache")

	with open(dircache,"wb+") as f:
		pickle.dump(flist, f, encoding=conf.defaultencoding)
	try: os.chmod(dircache,0o666)
	except os.error: pass

	# Add crossref fields
	for refname in list(crossRefs.keys()):
		fname = os.path.join(conf.aktuellpath, refname)
		try:
			art = umstruct.getAsDict(fname)
			art["refs"] = ["/unimut/aktuell/%s"%s for s in crossRefs[refname]]
			art["refs"].sort()
			umstruct.savedict(fname, art)
			try:
				os.utime(fname, (int(refname), int(refname)))
			except os.error:
				pass
		except IOError:
			sys.stderr.write("Dead link: %s from %s\n"%(fname,
				crossRefs[refname]))
	

def getArtList():
	try:
		with open(os.path.join(conf.aktuellpath,"dir.cache"), "rb") as f:
			return pickle.load(f, encoding=conf.defaultencoding)
	except IOError:
		makeCaches()
		with open(os.path.join(conf.aktuellpath,"dir.cache"), "rb") as f:
			return pickle.load(f, encoding=conf.defaultencoding)


def articlesChanged(format):
	"""
	gibt einen String mit links/urls von "ge�nderten Artikeln" (was
	immer in changed.recs steht) zur�ck.  format kann "mail" or "html"
	sein."""

	srcfname = os.path.join(conf.aktuellpath, "changed.recs")
	if not os.path.exists(srcfname):
		return ""
	churls = list(map(lambda a, srcurl=umutil.urljoin(conf.homeurl,
		"aktuell/%s"):srcurl%a,
		umutil.uniq(
		list(filter(operator.truth,
		list(map(string.strip,
		open(srcfname).readlines())))))))
	churls.sort()

	if format=="mail":
		res = "\n\n-------\nGe�nderte Artikel seit letztem Mailing:\n\n"+\
			"\n".join(churls)
	else:
		raise ValueError("Format %s not known or implemented in articlesChanged"%format)
	try:
		os.unlink(srcfname)
	except os.error:
		pass
	return res


def mailArticle(form):
	"""Den Artikel mit dem index form["id"] vermailen, wenn das noch nicht
	geschehen ist
	"""
	theArticle = Aktuell(key=form["rec"].value)
	if theArticle.ismailed:
		return
	theArticle.ismailed = "1"
	theArticle.save()

	targurl = umutil.urljoin(conf.homeurl, "aktuell/%s"%
		form["rec"].value)
	nextDates = ""
	try:
		nextDates = termincache.getList().asText()
	except:
		pass
	tip = umutil.getAktuellTip()
	if tip:
		tip = "> Tipp der Redaktion:\n"+tip+"\n\n"

	os.environ["TERM"]="vt100" # Hack to make lynx work
	rawtext = os.popen('lynx -dump "%s?nodecor=1"'%targurl).read()
	mailtext = """From: No-Reply <www-data@fsk.uni-heidelberg.de>
Reply-To: %(replyTo)s
To: unimut-aktuell@fsk.uni-heidelberg.de
Content-type: text/plain; charset=iso-8859-1
Subject: %(subject)s

%(tip)s
Neuer Artikel am UNiMUT aktuell:
%(subject)s

In HTML unter %(targetUrl)s

%(rawText)s

%(nextDates)s
"""%{
		"tip": tip,
		"replyTo": conf.maintainer,
		"subject": theArticle.title,
		"targetUrl": targurl,
		"rawText": rawtext,
		"nextDates": nextDates
	}
	os.popen("sendmail -i -t","w").write(mailtext)
	

class Aktuell:
	"""fasst die Informationen zu einem Aktuell-Artikel zusammen
	"""
	subtitle = ""
	def __init__(self, key=None, form=None, forMaintainer=False):
		self.forMaintainer = forMaintainer
		self.crossRefs = ""
		if form:
			self.key = form["rec"].value
			self.date = form["date"].value
			if "title" in form:
				self.title = form["title"].value
			else:
				self.title = ""
			if "subtitle" in form:
				self.subtitle = form["subtitle"].value
			else:
				self.subtitle = ""
			if "body" in form:
				self.body = umutil.cleanTextarea(form["body"].value)
			else:
				self.body = ""
			if "isnotmailed" in form:
				self.ismailed = form["isnotmailed"].value!='1'
			else:
				self.ismailed = '1'
			if "showArt" in form:
				self.showArt = form["showArt"].value=='1'
			else:
				self.showArt = 0
		elif key==None:
			self.allockey()
			self.title = ""
			self.subtitle = ""
			self.body = ""
			self.date = time.strftime("%d.%m.%Y",time.localtime(int(self.key)))
			self.ismailed = None
			self.showArt = 0
		else:
			self.key = str(key)
			self.load()

	def render_plain(self):
		return ('%s<h2>%s (%s)</h2>\n'
			"%s %s\n%s\n"
			"<table width='100%%'><tr><td> </td></tr></table>")%(
			self.getSubtitleHtml(), self.title,
			self.date, self.getBody(), self.maintlink(),
			self.renderCrossrefs())

	def render_tex(self):
		"""returns the article HTML with markers for later TeX processing.
		"""
		return ('\\beginarticle{%s}{%s}{%s}%s\\endarticle'
			)%(self.date, self.subtitle, self.title, self.getBody())

	def _processPrivateMarkup(self, source):
		"""returns source with private aktuell markup processed.

		Private markup currently is: <footnote>...</footnote>

		We do this without using an actual html parser because we may have
		non-html private markup.
		"""
		allFootnotes = re.findall("(?s)<footnote>.*?</footnote>", source)
		if not allFootnotes:
			return source
		footnoteText = ['<hr width="20%" align="left">']
		for footIndex, footnote in enumerate(allFootnotes):
			footAnchor = ('<a name="%s-a%03d">'
				'<sup><a href="#%s-f%03d" class="footanchor">'
				'%d</a></sup></a>')%(self.key, footIndex,
				self.key, footIndex, footIndex+1,)
			source = source.replace(footnote, footAnchor)
			footnoteText.append('<p class="footnote">'
				'<a name="%s-f%03d"><sup>%d</sup></a> '
				'%s <a href="#%s-a%03d">[Zur�ck]</a></p>'%(self.key, footIndex,
					footIndex+1,
					re.search("(?s)<footnote>(.*?)</footnote>", footnote).group(1),
					self.key, footIndex))
		return source+"\n".join(footnoteText)

	def getBody(self):
		"""returns the body processed for display.

		You need to access the body attribute directly to get the source for,
		e.g., editing.
		"""
		return self._processPrivateMarkup(self.body)

	def getSubtitleHtml(self):
		if self.subtitle:
			return '<p class="subhead"><em>%s</em></p>'%self.subtitle
		return ""

	def renderCrossrefs(self):
		if hasattr(self, "crossRefs") and self.crossRefs:
			return '<p class="crossref">Dieser Artikel wurde zitiert am: %s</p>'%(
				", ".join(['<a href="%s">%s</a>'%(s,
				time.strftime("%d.%m.%Y", time.localtime(int(s.split('/')[-1]))))
				for s in eval(self.crossRefs)]))
		else:
			return ""

	def maintlink(self):
		stuff = '<p align="right"><font size="-1">'
		if self.forMaintainer:
			stuff = stuff+'<a href="/unimut/aktuell/editrec?rec=%s">�ndern</a>/'%(
				self.key)
		return stuff+'<a href="/unimut/aktuell/%s">Link me</a></font></p>'%(
			self.key)

	def allockey(self):
		while True:
			self.key = str(int(time.time()))
			fname = os.path.join(conf.aktuellpath, self.key)
			if not os.path.exists(fname):
				with open(fname,"w") as f:
					f.write("")
				break
			time.sleep(1)
		time.sleep(2)
		os.unlink(fname)

	def load(self):
		try:
			with open(os.path.join(conf.aktuellpath, self.key),
					"r", encoding=conf.defaultencoding) as f:
				data = f.read()

			self.date = umstruct.getfield("date",data)
			self.title = umstruct.getfield("title",data)
			self.subtitle = umstruct.getfield("subtitle",data)
			self.body = umstruct.getfield("body",data)
			self.ismailed = umstruct.getfield("ismailed",data)
			self.crossRefs = umstruct.getfield("refs",data)
			showArt = umstruct.getfield("showme",data)
			if self.subtitle is None:
				self.subtitle = ""
			if showArt is None:
				self.showArt = 1
			else:
				self.showArt = eval(showArt)
		except IOError:
			self.title = ""
			self.body = ""
			self.ismailed = None
			self.crossRefs = None
			self.date = time.strftime("%d.%m.%Y",time.localtime(int(self.key)))
			self.showArt = 0

	def save(self):
		artDict = {"date":self.date,
			"title":self.title,
			"subtitle":self.subtitle,
			"body":self.body}
		if self.ismailed:
			artDict["ismailed"] = '1'
		artDict["showme"] = str(self.showArt)
	
		umstruct.savedict(os.path.join(conf.aktuellpath, self.key),
			artDict)
		try:
			os.utime(os.path.join(conf.aktuellpath, self.key),
				(int(self.key), int(self.key)))

			with open(os.path.join(conf.aktuellpath, "changed.recs"), "a") as f:
				f.write("%s\n"%self.key)
		except:
			pass

	def editForm(self):
		"""gibt ein Formular zum �ndern von sich selbst aus
		"""
		reslist = ['<FORM METHOD="POST" ACTION="/unimut/aktuell/saverec">']
		reslist.append("<table>")
		reslist.append('<TR><TD><P>Datum:</TD><TD><INPUT TYPE="TEXT"'+
			' NAME="date" VALUE="%s"</P></TD></TR>'%self.date)
		reslist.append('<TR><TD><P>Obertitel:</TD><TD><INPUT TYPE="TEXT"'
			" NAME='subtitle' SIZE=40 style='width:100%%' VALUE='%s'>"
			"</P></TD></TR>"%self.subtitle)
		reslist.append('<TR><TD><P>Titel:</TD><TD><INPUT TYPE="TEXT"'
			" NAME='title' SIZE=40 style='width:100%%' VALUE='%s'>"
			"</P></TD></TR></TABLE>"%self.title)
		reslist.append('<P>'+
			'<textarea name="body" cols="60" rows="12" wrap="virtual"'
			' style="width:100%%; font-family: Helvetica, Arial">%s</TEXTAREA></P>'%
			_ampEscape(umutil.textareaEscape(self.body)))
		reslist.append('<INPUT TYPE="HIDDEN" NAME="rec" VALUE="%s">'%self.key)
		reslist.append('<table width="100%"><tr><td align="left">'+
			'<INPUT NAME="Speichern" TYPE="SUBMIT" Value="Speichern"></td>')
		reslist.append('<td align="RIGHT"><INPUT NAME="Fertig" '+
			'TYPE="SUBMIT" Value="Fertig"></td></tr></table>')

		if self.ismailed:
			reslist.append('<br><INPUT TYPE="checkbox" NAME="isnotmailed" VALUE="1">')
		else:
			reslist.append('<br><INPUT TYPE="checkbox" NAME="isnotmailed" checked'+
				' VALUE="1">')
		reslist.append("Bei 'Fertig' verschicken")

		if self.showArt:
			reslist.append('<INPUT TYPE="checkbox" NAME="showArt" checked'+
				' VALUE="1">')
		else:
			reslist.append('<INPUT TYPE="checkbox" NAME="showArt" VALUE="1">')
		reslist.append("Artikel im �ffentlichen Index anzeigen")
		
		reslist.append('</form>')
		reslist.append(str(self))
		reslist.append(re.sub(r"(?m)^\s+","","""<p><strong>Kurzanleitung</strong>
		<p>Text tippen, zwischendurch immer mal auf "Speichern" gehen.  �ber
		diesem Text erscheint dann ein "Preview" des Artikels (ja, dort, wo
		jetzt das harmlose Geblubber �ber den Kaputten Artikel steht).
		Wenn alles fertig ist, auf den Fertig-Button klicken, der Artikel
		erscheint dann auf der Aktuell-Einstiegsseite und wird auf die
		Mailingliste gesetzt.</p>
		<P>Hier wird die
		Abk�rzungsdatenbank unterst�tzt.  Einfach ein Dach (^) vor eine
		Abk�rzung aus der Datenbank setzen, abschicken, fertig.</P>
		<P>Wenn ihr euch das mit dem Schreiben doch anders �berlegt, geht unten �ber
		"Artikel l�schen" raus.</P>
		<P><a href="/unimut/aktuell/delrec?rec=%s">Artikel l�schen</a></P>
		<P><a href="/unimut/aktuell/movetofront?rec=%s">Artikel
		nach oben schieben</a></P>
    """%(self.key, self.key)))
		return "\n".join(reslist)

	def __str__(self):
		if not self.showArt:
			template = '''<div style="background-color:#FFFF77;">%s</div>'''
		else:
			template = "%s"
		if not self.title:
			if self.forMaintainer:
				return template%(
					'<a href="/unimut/aktuell/editrec?rec=%s">Kaputten Artikel %s'%(
					self.key,self.key)+" �ndern</a>")
			else:
				return ""
		return template%self.render_plain()


class Document(umdoc.Document):

	def __init__(self, request, *args, **kwargs):
		umdoc.Document.__init__(*(self,request)+args, **kwargs)
		self.linkline.append(
			'<a href="/unimut/aktuell/archiv">[Aktuell-Archiv]</a>')
		self.moreheaders.append("pragma: no-cache")
		self.localHeadMaterial.append('<link rel="alternate"'
			' type="application/rss+xml" title="Artikel, Termine und Jahrestage"'
			' href="%s/rss">'%conf.homeurl)
		self.replaceAbbrevs = 1
		
		path = request.path_info
		self.loadFlist()

		if path.endswith("editrec"):
			self.editrec(request.form)
		elif path.endswith("newrec"):
			self.editrec({})
		elif path.endswith("saverec"):
			self.editrec(request.form, save=1)
		elif path.endswith("delrec"):
			self.delrec(request.form)
		elif path.endswith("reindex"):
			self.reindex(request.form)
		elif path.endswith("archiv"):
			self.index()
		elif path.endswith("toc"):
			self.toc(request.form)
		elif path.endswith("inhalt"):
			self.toc({})
		elif path.endswith("movetofront"):
			self.moveToFront(request.form)
		elif path.endswith("show") or "rec" in request.form:
			self.show(request.form)
		elif path.endswith("getTeX"):
			self.texSource(request.form)
		elif re.match(r"aktuell/\d+$", path):
			self.showOne(path[8:])
		else:
			self.show({})

	def loadFlist(self):
		if self.request.prefs.isMaintainer:
			self.flist = [int(a.split("/")[-1]) for a in
				glob.glob(os.path.join(conf.aktuellpath, "[0-9]*"))]
			self.flist.sort()
		else:
			self.flist = getArtList()

	def reindex(self, form):
		makeCaches()
		mailArticle(form)
		raise conf.Redirect("/unimut/aktuell")


	def readableRange(self,lokey,hikey):
		"""
		formatiert lokey und hikey als lesbare Daten"""

		return (time.strftime("%d.%m.%y",time.localtime(lokey)),
			time.strftime("%d.%m.%y",time.localtime(hikey)))


	def artsBetween(self, start, end, daterange=None):
		if daterange:
			mat = re.match(r"(\d+)-(\d+)", daterange)
			if not mat:
				raise conf.Error("%s ist kein Zeitbereich, den ich verstehe")
			start = int(mat.group(1))
			end = int(mat.group(2))
		loind = bisect.bisect(self.flist, start)
		hiind = bisect.bisect(self.flist, end)

		return self.readableRange(start, end), self.flist[loind:hiind]


	def artsMonth(self,monthcode):
		"""
		gibt die Artikel f�r einen ganzen Monat zur�ck"""

		month = umutil.Month(monthcode=monthcode)
		start,end = month.timeRange()
		return month.longName(),self.artsBetween(start,end)[1]


	def homeHeadmatter(self):
		"""gibt den Krempel �ber dem eigentlichen Text auf der
		UM aktuell-Startseite zur�ck.
		"""
		headmatter = ""
		try:
			with open(conf.fixedaktupath, "r", encoding=conf.defaultencoding) as f:
				fixed = f.read()
		except IOError:
			fixed = "&nbsp;"

#		try:
#			recom = re.sub("<a ",
#				'<a style="color:white;text-decoration:underline"',
#				open(conf.recompath).read())
#			if self.request.prefs.isMaintainer:
#				recom = recom+'<br><a href="/unimut/aktuell/maywediag">�ndern</a>'
#		except:
#			recom = "&nbsp;"
#		recom = ""
#
#		headmatter = headmatter+"""<TR><TD VALIGN="TOP">%s</TD>
#			<TD VALIGN="TOP" BGCOLOR="#666666">
#			<p><IMG SRC="/unimut/images/umtermi.gif" WIDTH=200 HEIGHT=40
#			ALT="May we recommend...">
#			<BR><FONT COLOR="WHITE">
#			%s
#			</P></TD></TR></TABLE>"""%(fixed,recom)
		headmatter = headmatter+"%s"%fixed
		return headmatter

	def showOne(self, article):
		article = int(article)
		self.title = "UNiMUT aktuell -- Artikel %d"%article
		self.linkline.append(self.monthlinks([article]))
		self.display([article], "")

	def texSource(self, form):
		if "range" in form:
			readableRange, artlist = self.artsBetween(None, None,
				form["range"].value)
		else:
			artlist = self.flist[-self.request.prefs.numArt:]
		artlist.reverse()
		self.data = "\n\n".join([Aktuell(a, self.request.prefs.isMaintainer
			).render_tex() for a in artlist])
		self.content_type = "text/x-tex"

	def show(self, form):
		"""Die zentrale Funktion zum Anzeigen von aktuell-Artikeln --
		versteht
		rec=<key>
		range=<key1>-<key2>
		indrange=<index1>-<index2>
		oder zeigt ansonsten die letzten paar neuen Artikel an.
		"""
		headmatter = ""
		self.title = "UNiMUT aktuell"
		if "rec" in form:
			raise conf.Redirect("/unimut/aktuell/%s"%form["rec"].value)

		elif "range" in form:
			readableRange,artlist = self.artsBetween(None,None,form["range"].value)
			self.title = "UNiMUT aktuell -- %s bis %s"%readableRange
			self.linkline.append(self.monthlinks(artlist))

		elif "month" in form:
			readableMonth,artlist = self.artsMonth(form["month"].value)
			self.title = "UNiMUT aktuell -- %s"%readableMonth
			self.linkline.append(self.adjacentMonths(form["month"].value))

		else:
			artlist = self.flist[-self.request.prefs.numArt:]
			artlist.reverse()
			headmatter = self.homeHeadmatter()
			self.linkline.append(self.adjacentMonths(
				umutil.Month(unixtime=time.time()).shortName()))

		self.display(artlist, headmatter)

	def monthlinks(self,artlist):
		"""Erzeugt links auf die vollen Monate aus den keys in artlist
		"""
		months = [umutil.Month(unixtime=a).shortName() for a in artlist]
		return " ".join(
			['[<a href="/unimut/aktuell/show?month=%s">'%(
			urlparse.quote_plus(a))+a+'</a>]' for a in months])
	
	def adjacentMonths(self,monthcode):
		"""Erzeugt links auf die Monate vor und nach monthcode
		"""
		month = umutil.Month(monthcode)
		prev = month.previous()
		following = next(month)
		reslist = []
		if prev.enddate>self.flist[0]:
			reslist.append('[<a href="/unimut/aktuell/show?month=%s">%s</a>]'%(
				prev.urlEncoded(),prev.shortName()))
		if following.startdate<self.flist[-1]:
			reslist.append('[<a href="/unimut/aktuell/show?month=%s">%s</a>]'%(
			following.urlEncoded(),following.shortName()))
		return "".join(reslist)

	def display(self, artlist, headmatter=""):
		"""Schiebt die Artikel, deren keys in artlist stehen, nach data.
		headmatter wandert �ber die artikel
		"""
		self.data.append('<div class="aktuell">')
		self.data.append(headmatter)
		if not artlist:
			self.data.append("<h2>Keine Artikel aus dieser Zeit.</h2>")
			return
		self.date = reduce(max, list(map(int, artlist)))
		for a in artlist:
			theArticle = Aktuell(a, forMaintainer=self.request.prefs.isMaintainer)
			self.data.append(str(theArticle))
		if len(artlist)==1:
			self.title = "UNiMUT aktuell: "+theArticle.title
		self.data.append('</div>')

	def editrec(self, form, save=None):
		if not self.request.prefs.isMaintainer:
			raise conf.Error("Das darfst du nicht")
		if "body" in form:
			art = Aktuell(None, form,
				forMaintainer=self.request.prefs.isMaintainer)
		elif "rec" in form:
			art = Aktuell(key=form["rec"].value,
				forMaintainer=self.request.prefs.isMaintainer)
		else:
			art = Aktuell(None,
				forMaintainer=self.request.prefs.isMaintainer)
		if save:
			art.save()
		if "Fertig" in form:
			self.reindex(form)
		else:
			self.data = art.editForm()
			self.title = "%s �ndern"%art.key
	
	def delrec(self,form):
		if not self.request.prefs.isMaintainer:
			raise conf.Error("Das darfst du nicht")

		art = Aktuell(key=form["rec"].value,
			forMaintainer=self.request.prefs.isMaintainer)
		if "sicher" not in form:
			self.title = "L�schen best�tigen"
			self.data = """<form action="/aktuell/delrec">
				<p>Du bist ganz sicher, dass du den Artikel unten l�schen willst
				(du musst "Ja" (genau so, nur ohne Anf�hrungszeichen) eingeben)?</p>
				<input type="text" name="sicher" value="">
				<input type="hidden" name="rec" value="%s">
				<input type="submit" value="Ok">
				<p>Sonst geh einfach woanders hin oder zur�ck...</p>
				</form>"""%(art.key)+str(art)
		else:
			if form["sicher"].value!="Ja":
				raise conf.Error("Artikel wurde nicht gel�scht.  Das war knapp.")
			try:
				os.unlink(os.path.join(conf.aktuellpath,art.key))
			except os.error:
				raise conf.Error("Den Artikel %s gibts gar nicht"%art.key)
			makeCaches()
			raise conf.Redirect("unimut/aktuell")


	def index(self):
		"""Anklickbare Monate
		"""
		startdate,enddate = self.flist[0],self.flist[-1]
		startyear,startmonth,_,_,_,_,_,_,_ = time.localtime(startdate)
		endyear,endmonth,_,_,_,_,_,_,_ = time.localtime(enddate)
		years = list(range(startyear,endyear+1))
		years.reverse()

		self.title = "UNiMUT aktuell: Archiv-Index"
		self.linkline.append('<a href="/aktuell/inhalt">[Gesamtinhalt]</a>')

		self.data = ["<TABLE CELLSPACING=10>"]
		subtab=0
		for year in years:
			if subtab%2:
				self.data.append('<TD VALIGN="TOP">')
			else:
				self.data.append('<TR><TD VALIGN="TOP">')
			self.data.append("<P><TABLE CELLPADDING=3 CELLSPACING=0 BORDER=0>")
			self.data.append('<TR><TD COLSPAN=3 BGCOLOR="#333333"><P><BR></P>'+
				'<FONT COLOR="#ffffff"><H2>%02d</H2></FONT></TD></TR>'%year)

			amonth=1
			bmonth=12
			if year==startyear:
				amonth = startmonth
			if year==endyear:
				bmonth = endmonth
			linind = 0
			for month in range(amonth,bmonth+1):
				m = umutil.Month(date=(month,year))
				if linind%2:
					curbg = "#cccccc"
				else:
					curbg = "#ffffff"
				linind = linind+1
				self.data.append('<TR><TD WIDTH=50%% BGCOLOR="%s">%s</TD>'%(
					curbg,umutil.fullmonth[month-1]))
				self.data.append(('<TD BGCOLOR="%s"><a href="/aktuell/toc?'%curbg)+
					'month=%s">[Inhalt]</a></TD>'%m.urlEncoded())
				self.data.append(('<TD BGCOLOR="%s"><a href="/aktuell/show?'%curbg)+
					'month=%s">[Volltext]</a></TD></TR>'%m.urlEncoded())

			self.data.append("</TABLE></P>")
			if subtab%2:
				self.data.append("</TD></TR>")
			else:
				self.data.append("</TD>")
			subtab = subtab+1
		self.data.append("</TABLE>")

	def toc(self, form):
		"""gibt ein Inhaltsverzeichnis mit titeln aus
		"""
		if "range" in form:
			readableRange,artlist = self.artsBetween(None, None,
				form["range"].value)
			self.title = "UNiMUT aktuell: Inhalt %s-%s"%readableRange
		elif "month" in form:
			readableMonth, artlist = self.artsMonth(form["month"].value)
			self.title = "UNiMUT aktuell: Inhalt %s"%readableMonth
		else:
			self.title = "UNiMUT aktuell: Gesamtinhalt"
			artlist = self.flist
			
		if not artlist:
			self.data = "<p>Keine Artikel aus dieser Zeit verf�gbar.</p>"
			return

		try:
			with open(os.path.join(conf.aktuellpath, "titles.cache"), "rb") as f:
				titles = pickle.load(f, encoding=conf.defaultencoding)
		except IOError:
			makeCaches()
			with open(os.path.join(conf.aktuellpath, "titles.cache"), "rb") as f:
				titles = pickle.load(f, encoding=conf.defaultencoding)
		self.data = ["<ul>"]
		for a in artlist:
			try:
				if titles[a][2]:
					subtitle = "<em><font size='-1'>%s</font></em><br>"%titles[a][2]
				else:
					subtitle = ""
				self.data.append('<li>%s<a href="%s">%s</a> (%s)</li>'%(
					subtitle, "/unimut/aktuell/%s"%a, titles[a][1], titles[a][0]))
			except KeyError:
				pass
		self.data.append("</ul>")

	def moveToFront(self, form):
		"""benennt den Artikel form["rec"] so um, dass er zur augenblicklichen
		Uhrzeit passt (d.h. ganz oben auf der Liste auftaucht).  Das Datum
		im Artikel selbst muss dann noch per Hand gesetzt werden.
		"""
		if not self.request.prefs.isMaintainer:
			raise PermissionError("Das darfst du nicht")
		srcPath = os.path.join(conf.aktuellpath, form["rec"].value)
		if not os.path.exists(srcPath):
			raise conf.ParameterError("Der Artikel %s existiert nicht."%
				form["rec"].value)
		newKey = str(int(time.time()))
		newPath = os.path.join(conf.aktuellpath, newKey)
		os.rename(srcPath, newPath)
		raise conf.Redirect("/unimut/aktuell/editrec?rec=%s"%newKey)

if __name__=="__main__":
	print(makeCaches())
