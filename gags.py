# -*- coding: iso-8859-1 -*-


import umdoc, os, time, random, conf, urllib.request, urllib.parse, urllib.error
from functools import reduce


def _backupToMarker(marker, f):
	try:
		f.seek(-512, 1)
		while 1:
			if f.tell()==0:
				return
			stuff = f.read(512)
			markerPos = stuff.rfind(marker)
			if markerPos!=-1:
				f.seek(markerPos-508, 1)
				return
			f.seek(-1024, 1)
	except IOError:
		f.seek(0, 0)

def _collectToMarker(marker, f):
	res = b""
	while 1:
		newStuff = f.read(512)
		markerPos = newStuff.find(marker)
		if markerPos!=-1:
			return res+newStuff[:markerPos]
		else:
			res = res+newStuff


def randomQuote(fromFile):
	maxSize = os.path.getsize(fromFile)
	quotePos = random.randint(0, maxSize)
	with open(fromFile, "rb") as f:
		f.seek(quotePos)
		_backupToMarker(b"\n%%\n", f)
		return _collectToMarker(b"\n%%\n", f).decode(conf.defaultencoding)


class Ranking(object):
	"""A "ranking" of images based on a meta file.
	"""
	def __init__(self, sourcePath, cats):
		self.sourceFrag = sourcePath
		self.sourcePath = os.path.join(conf.rootpath, sourcePath)
		self.cats = cats
		self._loadContenders()

	def _loadContenders(self):
		self.contenders = []
		with open(os.path.join(self.sourcePath, "krampf.meta"),
				encoding=conf.defaultencoding) as f:
			for descr in f.read().split("%%"):
				try:
					# contenders are triples of serno, img, comment
					self.contenders.append(descr.strip().split("\n", 2))
				except ValueError:
					continue

	def getMTime(self):
		return os.path.getmtime(self.sourcePath)

	def compute(self, institute):
		ranking = []
		random.seed(int(reduce(lambda a,b: a*17.+b, list(map(ord, institute)))))
		weightSum = sum([w for w, t in self.cats])
		for serno, img, comment in self.contenders:
			scores, total = {}, 0
			for weight, title in self.cats:
				scores[title] = random.random()
				total += (scores[title]*weight)/weightSum
			ranking.append((total, serno, img, comment, scores))
		ranking.sort()
		ranking.reverse()
		return ranking

	def _formatScores(self, scores):
		return "<br>".join(["%s: %2.2f"%(title, scores[title]*100)
			for _, title in self.cats])

	def format(self, ranking):
		stuff = []
		count = 1
		for total, serno, img, comment, scores in ranking:
			stuff.append("".join(['<tr><td width="20%%"><a name="s%02d">'%int(serno),
				'Platz %d</a><br>'%count,
				'<a href="/%s/%s" target="pic" title="Klick f�r gro�e Fassung">'%(self.sourceFrag, img),
				'<img src="/%s/thumb_%s"></a></td>'%(self.sourceFrag, img),
				'<td width="60%%"><p>%s</p></td>'%comment,
				'<td width="20%%" align="right"><p><strong>Gesamt: %2.2f</strong><br>%s</p></td>'%(
					total*100, self._formatScores(scores)),
				'</tr>']))
			count += 1
		return '<table cellpadding="6pt">%s</table>'%("\n".join(stuff))


class Document(umdoc.Document):

	def __init__(self, request, *args, **kwargs):
		umdoc.Document.__init__(*(self,request)+args, **kwargs)
		if request.path_info=="%s/juralette"%__name__:
			self.doJuralette()
		elif request.path_info=="%s/wahlkrampf05"%__name__:
			self.doWahlkrampf05(request.form)
		elif request.path_info=="%s/wahlkrampf09"%__name__:
			self.doWahlkrampf09(request.form)
		else:
			raise conf.UnknownPath
	
	def doJuralette(self):
		self.title = "Das Juralette"
		self.date = time.time()
		self.decorate = 0
		self.data = [randomQuote(
			os.path.join(conf.headpath, "lhg.quotes"))]
		self.data.append("<hr><p>Das Juralette ist ein Spezialservice des"
			" UNiMUT: Bei jedem Aufruf bekommt ihr einen neuen Artikel"
			' des <a href="http://unimut.stura.uni-heidelberg.de/unimut/'
			'aktuell/1069874988">LHG-Entwurfs f�r Baden-W�rttemberg von 2003</a>'
			'<p><a href="/unimut/gags/juralette">Noch ein Artikel</a></p>'
			)
	
	def doWahlkrampf05(self, form):
		if "inst" in form:
			institute = form["inst"].value
		else:
			institute = "UNiMUT"
		self.title = "Wahlkrampf 2005: Das %s-Ranking"%institute
		ranking = Ranking("features/wahlkrampf05",
			[(1.876, "Mensa"), (0.875, "Bibliothek"), (1.1, "Leben")])
		self.date = ranking.getMTime()
		self.suppressTermine = 1

		self.data.append(ranking.format(ranking.compute(institute)))
		self.data.append('<p>Nur f�r die Humorlosen: Sollten auf dieser'
			' Seite irgendwo bekannte Namen stehen, k�nnen wir nichts daf�r.'
			'  Die habt ihr schon selbst reingeschrieben.  Mehr dazu auf unserer'
			' <a href="/unimut/features/wahlkrampf05">Rankingseite</a>.</p>')
	
	def doWahlkrampf09(self, form):
		if "inst" in form:
			institute = form["inst"].value.replace("<", "")
		else:
			institute = "UNiMUT"
		self.title = "Wahlkrampf 2009: Das %s-Ranking"%institute
		ranking = Ranking("features/wahlkrampf09",
			[(0.1, "Nachhaltigkeit"), (0.4, "Exzellenz"), (0.3, "Krisenfestigkeit"),
			(0.8, "Zukunfsorientierung")])
		self.date = ranking.getMTime()
		self.suppressTermine = 1
		
		self.data.append('<p>Unsere <a href="/unimut/features/wahlkrampf05">'
			'Methodik</a> zur Erstellung eines Rankings des Angebote der'
			' diversen mehr oder weniger demokratischen Parteien von 2005 darf'
			' als bew�hrt gelten, weshalb wir sie auch zur Bundestagswahl'
			' 2009 beibehalten haben.  In diesem Jahr haben wir aber'
			' als Einzelkriterien Nachhaltigkeit (0.1), Exzellenz (0.4),'
			' Krisenfestigkeit (0.3) sowie Zukunftsorientierung (0.8)'
			' ausgew�hlt.</p>'
			'<p style="position:relative;float:right;width:200px;'
			'margin:5pt;background-color:#eedddd">'
			'Hat das Institut richtig gerankt?  Dann ab mit diesem'
			' Code auf eure Webseite:<br>'
			'<tt style="font-size:60%%">&lt;a href="http://unimut.stura.uni-'
			'heidelberg.de/unimut/gags/wahlkrampf09?inst=%s"&gt;Das'
			' einzig wahre Wahlplakate-Ranking&lt;/a&gt;</tt></p>'
			'<p>Wie schon 2005 bieten wir auch dieses Jahr die M�glichkeit,'
			' beliebige Institute zu fragen -- irgendeines wird schon euren'
			' Lieblingsbeitrag auf Platz 1 ranken.  Einfach mal probieren:'
			' <form action="wahlkrampf09" method="GET">'
			'<input type="text" name="inst" size="50" value="%s">'
			' <input type="submit" value="Anzeigen"></form></p>'
			'<p>Links zu den sch�nsten Rankings k�nnt ihr -- Web 2.0! -- an'
			' eure FreundInnen verschicken, und wir nehmen nat�rlich auch gerne'
			' h�bsche Fotos an unsere Adresse unten entgegen</p>'%(
				urllib.parse.quote(institute),
				institute.replace('"', " ")))

		self.data.append(ranking.format(ranking.compute(institute)))
		self.data.append('<p>Nur f�r die Humorlosen: Sollten auf dieser'
			' Seite irgendwo bekannte Namen stehen, k�nnen wir nichts daf�r.'
			'  Die habt ihr schon selbst reingeschrieben.</p>')

