# -*- encoding: iso-8859-1 -*-
"""
Dieses Modul managet bin�re Dateien (also vor allem Bilder)
"""

import os, conf
import umdoc, httpstuff

class Document(umdoc.Document):
	

	def __init__(self, request, sourcefile):
		if not os.path.exists(sourcefile):
			raise conf.UnknownPath("Das finde ich nicht.")
		umdoc.Document.__init__(self, request)
		self.encoding = None
		with open(sourcefile, "rb") as f:
			self.data = f.read()
		self.sourcefile = sourcefile
		self.binary = 1

		self.content_type = httpstuff.getMimeType(sourcefile)

	def getTop(self):
		return b""

	def getFooter(self):
		return b""
