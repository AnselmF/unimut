#!/usr/bin/env python2.2
# -*- encoding: iso-8859-1 -*-

import sys, os, fnmatch, operator, re, locale, pickle, time
from functools import reduce

sys.path.append(os.path.join(os.path.dirname(sys.argv[0]), "../cgi-bin"))

import conf, umstruct

# If this is true, a wordlevel index is written (i.e., the inverted
# index contains positions of words instead of words only).
wordlevel = 0

def _scanDir(dir, pat):
	"""recursively scans the directory dir for files matching pat and returns
	a list for absolute paths (if dir is absolute, which is not checked)
	to the matching files.  Pat may also be a list, in which case we examine
	each pattern.
	"""
	if type(pat)==type([]):
		matchFun = lambda name, pats: reduce(operator.__or__,
			list(map(lambda p, name=name: fnmatch.fnmatch(name, p), pats)))
	else:
		matchFun = fnmatch.fnmatch
	files = []
	for fname in os.listdir(dir):
		abspath = os.path.join(dir, fname)
		if matchFun(fname, pat) and os.path.isfile(abspath):
			files.append(abspath)
		else:
			if os.path.isdir(abspath):
				files.extend(_scanDir(abspath, pat))
	return files

def collectFiles(checkList):
	"""recursively scans the directories mentioned in the first
	element of each tuple in checkList for files matching the
	shell pattern in the second element and returns a list of
	complete paths
	"""
	fileList = []
	for dir, pat, handler in checkList:
		fileList.extend([(fn, handler) for fn in _scanDir(dir, pat)])
	return fileList

def _toRelPath(path):
	"""tries to guess what path a file might be visible as on the
	server and returns that.  The heuristics is that the last
	unimut in the path corresponds to the server path.
	"""
	path = path.replace(conf.headpath, "")
	if not path.startswith("/"):
		path = "/"+path
	return path
	
def collectWords(fileList):
	"""creates an inverted index of files (or rather, their indices
	into fileList) mentioned in fileList, which is contains pairs of
	filenames and handlers for these files.

	fileList is amended with titles, target URLs and dates (yikes!).  Trust
	me, this design started out better.
	"""
	invIndex = {}
	for fIndex, fNAndHandler in enumerate(fileList):
		try:
			result = eval("_handle%s(fNAndHandler[0])"%fNAndHandler[1])
		except KeyboardInterrupt:
			sys.exit(1)
		except:
			sys.stderr.write("Trouble in %s: %s.\n"%(fNAndHandler[0],
				str(sys.exc_info()[1])))
			result = None
		if result is None:
			continue
		targUrl, title, recWords, mDate = result
		if wordlevel:
			for word in list(recWords.keys()):
				invIndex.setdefault(word, []).append((fIndex, recWords[word]))
		else:
			for word in list(recWords.keys()):
				invIndex.setdefault(word, []).append(fIndex)
		fileList[fIndex] = (fileList[fIndex][0], targUrl, title, mDate)
	return invIndex

def _getWordsFromHtml_wordlevel(data):
	words = {}
	for wInd, word in enumerate(re.sub(r"\W+", " ",
		re.sub("<[^>]*>", " ", data)).lower().split()):
		words.setdefault(word, []).append(wInd)
	return words

def _getWordsFromHtml(data):
	if wordlevel:
		return _getWordsFromHtml_wordlevel(data)
	words = {}
	for word in re.sub(r"\W+", " ",
		re.sub("<[^>]*>", " ", data)).lower().split():
		words[word] = None
	return words

def _getWordsFromTxt_wordlevel(data):
	words = {}
	for wInd, word in enumerate(
		re.sub(r"\W+", " ", data).lower().split()):
		words.setdefault(word, []).append(wInd)
	return words

def _getWordsFromTxt(data):
	if wordlevel:
		return _getWordsFromTxt_wordlevel(data)
	words = {}
	for word in re.sub(r"\W+", " ", data).lower().split():
		words[word] = None
	return words

def _handleAltArchiv(fName):
	with open(fName, encoding=conf.defaultencoding) as f:
		return ("/unimut/archiv/alt/%s"%os.path.basename(fName),
			os.path.basename(fName),
			_getWordsFromTxt(f.read()),
			str(os.path.getmtime(fName)))

def _handleArchiv(fName):
	if os.path.basename(fName) in {"inhalte.html":0, "index.html":0}:
		return None
	with open(fName, encoding=conf.defaultencoding) as f:
		data = f.read()
	pubDate = re.match(r"\s*@D8([\d.]*)", data).group(1)
	title = "UNiMUT %s vom %s"%(
		re.sub(r"[^\d]", "", os.path.basename(fName)), pubDate)
	day, month, year = list(map(int, pubDate.split(".")))
	if year<20:
		year += 2000
	elif 20<year<100:
		year += 1900
	return ("/unimut/archiv/%s"%os.path.basename(fName),
		title,
		_getWordsFromHtml(data),
		str(time.mktime((year, month, day, 0, 0, 0, -1, -1, -1))))

def _handleAktuell(fName):
	with open(fName, encoding=conf.defaultencoding) as f:
		data = f.read()
	title = umstruct.getfield("title", data)
	words = _getWordsFromHtml(umstruct.getfield("body", data))
	words.update(_getWordsFromTxt(title))
	return ("/unimut/aktuell/%s"%os.path.basename(fName),
		title,
		words,
		os.path.basename(fName))

def _handleEinf(fName):
	with open(fName, encoding=conf.defaultencoding) as f:
		return ("/unimut/ersti",
			"Ersti-Einf�hrungen",
			_getWordsFromTxt(f.read()),
			str(os.path.getmtime(fName)))

def _handleTermin(fName):
	with open(fName, "rb") as f:
		try:
			termin = pickle.load(f, encoding=conf.defaultencoding)
			return ("/unimut/termine/%s"%(os.path.basename(fName)[4:]),
				"Termin: %s"%re.sub(r"\s+", " ", termin["kurztext"]),
				_getWordsFromHtml(termin.longFormHtml()),
				str(termin.timestamp))
		except AttributeError:
			return None

def _handleDirectHtmlFile(fName):
	with open(fName, encoding=conf.defaultencoding) as f:
		data = f.read()
	if re.match("\s*#[^&]+&", data):
		title = umstruct.getfield("title", data)
		if title is None:
			return None
		toIndex = title+umstruct.getfield("body", data)
	else:
		mat = re.search("<title>(.*)</title>", data)
		if mat:
			title = mat.group(1)
		else:
			title = os.path.basename(fName)
		toIndex = data
	return (_toRelPath(fName),
		title,
		_getWordsFromHtml(toIndex),
		str(os.path.getmtime(fName)))

def _handleDirectTxtFile(fName):
	with open(fName, encoding=conf.defaultencoding) as f:
		return (_toRelPath(fName),
			os.path.basename(fName),
			_getWordsFromTxt(f.read()),
			str(os.path.getmtime(fName)))

def _handleAbkuerz(fName):
	return ("/unimut/abkuerz",
		"Abk�rzungen",
		_getWordsFromTxt(fName),
		str(os.path.getmtime(fName)))

def _handleSchwerpunkt(fName):
	with open(fName, "rb") as f:
		sp = pickle.load(f, encoding=conf.defaultencoding)
	return ("/unimut/schwerpunkt/%s"%os.path.basename(fName),
		"Schwerpunkt %s"%sp.titel,
		_getWordsFromTxt(sp.asText()),
		str(os.path.getmtime(fName)))

def _handleWussi(fName):
	return ("/unimut/wussi",
		"Wussis",
		_getWordsFromHtml(fName),
		str(os.path.getmtime(fName)))

def _saveIndex(fileList, invDict, indexPath):
	"""dumps fileList and invDict into indexPath in appropriate formats.
	"""
	with open(os.path.join(indexPath, "files.list"), "w",
			encoding=conf.defaultencoding) as f:
		for ind, rec in enumerate(fileList):
			f.write("%06d\t%s\n"%(ind, "\t".join(
				[field.replace("\t", " ") for field in rec])))

	words = list(invDict.keys())
	words.sort()
	if wordlevel:
		f = open(os.path.join(indexPath, "words.fineindex"), "w",
			encoding=conf.defaultencoding)
	else:
		f = open(os.path.join(indexPath, "words.index"), "w",
			encoding=conf.defaultencoding)
	try:
		for word in words:
			if wordlevel:
				f.write("%s\t%s\n"%(word,
					" ".join([" ".join(["%d:%d"%(docInd, wInd) for wInd in wInds])
						for docInd, wInds in invDict[word]])))
			else:
				f.write("%s\t%s\n"%(word, " ".join(map(str, invDict[word]))))
	finally:
		f.close()

if __name__=="__main__":
	if "-w" in sys.argv:
		wordlevel = 1
	locale.setlocale(locale.LC_ALL, "de_DE")
#	filesToIndex = [
#		("../einf.db", ["*.list", "*.html"]),
#	]
	fileList = collectFiles(conf.filesToIndex)
	invIndex = collectWords(fileList)
	_saveIndex(fileList, invIndex, conf.indexPath)
