# -*- encoding: iso-8859-1 -*-

"""
Uploads f�r Medien.  Dieser Kram kommt in das Verzeichnis ures und �berschreibt
(in defaultaction) vorhandene Dateien mit bestimmten �nderungen.

Die Idee ist, dass Leute Kram hochladen k�nnen und an den entsprechenden Stellen
bereits richtige Links eintragen k�nnen.  Sp�ter k�nnen Leute mit ad�quaten
Rechten die Dateien verschieben, ohne dass URLs angepasst werden m�ssen.
"""

import os
import re
import time

import umdoc, conf, umbin

uploadDir = os.path.join(conf.headpath, "ures")

legalUploadPat = re.compile("\d{4}-")

class Document(umdoc.Document):
	def __init__(self, request, *args, **kwargs):
		self.title = "Upload von Bildern und Dokumenten"
		umdoc.Document.__init__(self, request, *args, **kwargs)
		if not request.prefs.isMaintainer:
			raise conf.Error("Das darfst du nicht")
		if request.path_info=="%s/upload"%__name__:
			self.doUpload(request)
		else:
			self.doUploadForm(request)
	
	def doUploadForm(self, request):
		self.data.append("<p>Hier kannst du Bilder oder Dokumente hochladen."
			"  Bitte achte darauf, dass die Bilder nicht gr��er sind als n�tig --"
			" Bilder, die breiter als 400 Pixel sind, haben am UNiMUT in der"
			" Regel nichts verloren.</p>")
		self.data.append("""<form action="/unimut/upload/upload" method="POST"
			enctype="multipart/form-data">
			<table border="0" cellpadding="6px">
			<tr><td>Datei</td><td><input name="file" type="file"></td></tr>
			<tr><td>Zielname</td><td>
				<input type="text" name="destname" value="%(year)s-"></td></tr>
			<tr><td>&nbsp;</td><td><input name="Hochladen" type="submit"></td></tr>
			</table>
			</form>"""%{
				"year": time.strftime("%Y")})
		self.data.append("<p>Hochgeladene Dateien <em>m�ssen</em> Namen haben,"
			" die mit einer Jahreszahl beginnen, sinnvollerweise die der"
			" Entstehung des Bildes oder Dokuments.  Grunds�tzlich hilft es,"
			" wenn der Name die Form jahr-(quelle-)stichwort.ext hat; ein"
			" Text der GEW zur Notwendigkeit radikaler Ma�nahmen k�nnte"
			" 2009-gew-radikale.pdf hei�en, das von uns gemachte Foto des"
			" anschlie�enden Heidelberger Fenstersturzes 2010-fenstersturz.jpg.</p>")
		self.data.append('<p>Bitte ladet keine "komischen" Dinge hoch'
			" (komisch sind insbesondere alle Default-Formate, die �bliche"
			" Programme von Microsoft erzeugen, und meist auch das, was diese f�r"
			' "offen" halten).</p>  <p>Wir bestimmen'
			" den Typ des Dokuments <em>allein</em> aus der Endung.  Sie muss"
			" also vorhanden sein, den Typ richtig beschreiben und im Augenblick"
			" aus der Menge {%s} kommen.  Wer was anderes braucht, m�ge sich"
			" an Demi wenden.</p>"%(", ".join(self.okedExtensions)))


	def doUploadError(self, msg):
		self.title = "Fehler beim Upload"
		self.data.append("<p>Beim Upload ist ein Fehler aufgetreten:</p>")
		self.data.append('<p style="text-align: center;border:2pt dotted red">%s</p>'%msg)
		self.data.append("<p>Du kannst mit dem Zur�ck-Knopf auf"
			" das Formular auf der Vorseite gehen und das korrigieren.</p>")

	okedExtensions = set([".pdf", ".gif", ".jpg", ".png", ".mp3", ".ogg"])
	imageExtensions = set([".gif", ".png", ".jpg"])

	def doUpload(self, request):
		form = request.form
		if "file" not in form:
			return self.doUploadError("Keine Datei ausgew�hlt.")
		fileitem = form["file"]
		if "destname" not in form:
			return self.doUploadError("Kein Dateiname angegeben.")
		destname = os.path.basename(form["destname"].value)
		ext = os.path.splitext(destname)[1]
		if ext not in self.okedExtensions:
			return self.doUploadError("Der Zielname %s hat keine der"
				" zugelassenen Dateierweiterungen (die da sind: %s)"%(
					destname, ", ".join(self.okedExtensions)))
		if not re.match(r"\d{4}-", destname):
			return self.doUploadError("Der Zielname %s f�ngt nicht mit"
				" einer Jahreszahl an -- darauf bestehe ich aber."%destname)

		with open(os.path.join(uploadDir, destname), 'wb') as fout:
			totalSize = 0
			while 1:
				chunk = fileitem.file.read(100000)
				totalSize += len(chunk)
				if not chunk:
						break
				fout.write(chunk)

		self.data.append("<p>Die Datei %s (%d bytes) ist jetzt auf dem"
			" Server verf�gbar.</p>"%(destname, totalSize))
		if ext in self.imageExtensions:
			self.data.append("<p>Um sie einzubinden, verwende etwas wie"
				' <tt>&lt;img src="/unimut/images/%s" alt="..." title="..." ...&gt;'
				'</tt></p>'%destname)
		else:
			self.data.append("<p>Um sie einzubinden, verwende etwas wie"
				' <tt>&lt;a href="/unimut/dokumente/%s"&gt;</tt></p>'%destname)
		self.doWarnings(destname, totalSize)

	def doWarnings(self, destname, totalSize):
		ext = os.path.splitext(destname)[1]
		if ext in set([".gif", ".png"]
				) and totalSize>15000:
			self.data.append("<p><b>Warnung:</b> F�r ein Bild mit indizierten"
				" Farben ist diese Datei arg gro�.  Willst du nicht sehen,"
				" ob du sie noch skalieren kannst, anders komprimieren"
				" oder sowas?</p>")
		if ext==".jpg" and totalSize>50000:
			self.data.append("<p><b>Warnung:</b> Der UNiMUT ist keine Kunst-"
				" und Fotoseite.  Eigentlich sollte hier kein JPEG mit"
				" mehr als 50k n�tig sein.  Kannst du den Kram nicht"
				" etwas skalieren und komprimieren?</p>")


def getUploadedDoc(request, pathInfo):
	name = os.path.basename(pathInfo)
	if not legalUploadPat.match(name):
		return
	fn = os.path.join(uploadDir, name)
	if os.path.isfile(fn):
		return umbin.Document(request, sourcefile=fn)
