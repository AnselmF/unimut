#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
Der zentrale Dispatcher f�r den UNiMUT -- analysiert die Anfragen
und verteilt sie auf die einzelnen Module
"""

import os,sys,traceback,exceptions,re,cgi,string,urllib
import conf, time, upload

def getHttpHeaders():
	eVars = filter(lambda a:a.startswith("HTTP_"), os.environ.keys())
	heads = {}
	for eVar in eVars:
		heads[eVar[5:]] = os.environ[eVar]
	return heads


class Request:
	
	def __init__(self):
		if os.environ.has_key("REQUEST_METHOD"):
			self.requestMethod = os.environ["REQUEST_METHOD"]
		else:
			self.requestMethod = "GET"
	
		self.unparsedURI = os.environ["REQUEST_URI"]
		if os.environ.has_key("PATH_INFO"):
			self.path_info = os.environ["PATH_INFO"]
		else:
			self.path_info = re.sub(r"\?.*", "", self.unparsedURI)

		self.httpHeaders = getHttpHeaders()
		try:
			self.remoteHost = os.environ["REMOTE_ADDR"]
			if os.environ.has_key("REMOTE_HOST"):
				self.remoteHost = os.environ["REMOTE_HOST"]
		except KeyError:
			self.remoteHost = "[XXX anonymus]"

		self.path_info = re.sub("/+","/",self.path_info)
		if self.path_info.startswith("/"):
			self.path_info = self.path_info[1:]
		if self.path_info.startswith("unimut/"):
			self.path_info = self.path_info[7:]
		if self.path_info=="unimut":
			self.path_info = "/"

		self.gzipOk = None
		try:
			if os.environ.has_key("HTTP_ACCEPT_ENCODING"):
				if os.environ["HTTP_ACCEPT_ENCODING"].find("gzip")!=-1:
					self.gzipOk = 1
			if os.environ.has_key("HTTP_USER_AGENT"):
				if os.environ["HTTP_USER_AGENT"].find("MSIE")!=-1:
					self.gzipOk = None
		except:
			traceback.print_exc()


		self.form = cgi.FieldStorage()

		self.document = None


	def defaultaction(self):
		d = upload.getUploadedDoc(self, self.path_info)
		if d:
			self.document = d
			return
		source = os.path.join(conf.rootpath,self.path_info)
		if os.path.exists(source):
			if os.path.isdir(source):
				raise conf.Redirect(os.path.join("/unimut",
					os.path.join(self.path_info,"index.html")))
			if os.path.isfile(source) and not source.endswith(".html"):
				import umbin
				self.document = umbin.Document(self,sourcefile=source)
			else:
				import umdoc
				self.document = umdoc.Document(self,sourcefile=source)
		else:
			raise conf.UnknownPath,"URI not found"


	def cgiCompat(self):
		"""
		behandelt ein paar requests des alten Stils."""

		import redirect
		if self.path_info.startswith("cgi-bin/abnjs.cgi"):
			self.document = redirect.Document(self,"/unimut/abkuerz/nojs?wort=%s"%
				os.environ["QUERY_STRING"])
		elif self.path_info.startswith("cgi-bin/pyschwob"):
			self.document = redirect.Document(self,"/unimut/schwob?schwob_url=%s"%
				urllib.quote_plus(os.environ["QUERY_STRING"]))
		elif self.path_info.startswith("cgi-bin/aktuell.py"):
			self.document = redirect.Document(self,"/unimut/aktuell?%s"%
				os.environ["QUERY_STRING"])
		elif self.path_info.startswith("cgi-bin/pysearch") or \
			self.path_info.startswith("cgi-bin/searcnew.cgi"):
			self.document = redirect.Document(self,"/unimut/search?%s"%
				os.environ["QUERY_STRING"])
		elif self.path_info.startswith("cgi-bin/dummsch.py"):
			self.document = redirect.Document(self,"/unimut/dummschwaetz")
		elif self.path_info.startswith("cgi-bin/dummsch"):
			self.document = redirect.Document(self,"/unimut/dummschwaetz")
		else:
			raise conf.UnknownPath,"CGI ist abgebrannt"


	def dispatch(self):
		path = self.path_info
		module = string.split(path,"/")[0]
		if module and re.match(r"\w+$",module):
			try:
				exec("import %s"%module)
				self.document = sys.modules[module].Document(self)
				return
			except (ImportError, SyntaxError):
				pass
		if module=="cgi-bin":
			self.cgiCompat()
			return
		self.defaultaction()

	def log(self, s):
		sys.stderr.write("\n\n%s: %s\n----\n"%(time.asctime(), s))


def output(doc):
		# Das hier zuerst machen, sonst ist der header schon drau�en,
		# bevor hier drin b�se Exceptions kommen.
		if not doc.headerOnly:
			docData = doc.getData()
			if doc.replaceAbbrevs:
				import abkuerz
				docData = abkuerz.insabkNew(docData)

			stuff = doc.getTop()+\
					docData+\
					doc.getFooter()
		doGzip = 0 and doc.request.gzipOk and not doc.binary
		if doGzip:
			import zlib
			doc.moreheaders.append("Content-encoding: gzip")
			stuff = zlib.compress(stuff)
		doc.moreheaders.append("Content-length: %d"%len(stuff))

		sys.stdout.write(doc.getHead())
		sys.stdout.flush()

		if not doc.headerOnly:
			sys.stdout.write(stuff)


try:
	if len(sys.argv)>1:
		os.environ["PATH_INFO"]=sys.argv[1]
	try:
		request = Request()
		if os.environ.has_key("HTTP_HOST"):
			if os.environ["HTTP_HOST"].startswith("www"):
				raise conf.Redirect, os.environ["REQUEST_URI"]
		import patProtest
		if patProtest.checkCookie(request):
			request.dispatch()
		if not request.document:
			raise conf.Error,"Diese URL hat mich verwirrt" # Shouldn't happen
	except conf.Redirect,newurl:
		import redirect
		request.document = redirect.Document(request, str(newurl))
	except conf.UnknownPath:
		import umerr
		request.document = umerr.notFoundErr(Request())
	except conf.ParameterError, msg:
		import umerr
		request.document = umerr.parameterErr(Request(), message=msg)
	except conf.UserError, msg:
		import umerr
		request.document = umerr.userErr(Request(), message=msg)
	except conf.Error,msg:
		import umerr
		traceback.print_exc()
		request.document = umerr.umErr(Request(),message=msg)
	request.document.done()
	output(request.document)
except:
	import StringIO
	strFile = StringIO.StringIO()
	traceback.print_exc(None,strFile)
	try:
		if os.environ.has_key("HTTP_REFERER"):
			refstr = "\nfrom %s"%os.environ["HTTP_REFERER"]
		else:
			refstr = ""
		if os.environ.has_key("REMOTE_HOST"):
				if os.environ.has_key("REMOTE_USER"):
					hoststr = "von %s@%s"%(os.environ["REMOTE_USER"],
						os.environ["REMOTE_HOST"])
				hoststr = " von %s"%os.environ["REMOTE_HOST"]
		else:
				if os.environ.has_key("REMOTE_ADDR"):
					hoststr = " von %s"%os.environ["REMOTE_ADDR"]
				else:
					hoststr = " [ohne host]"
		sys.stderr.write("%s ---- %s/%s%s %s\n%s\n"%(
			time.asctime(time.localtime(time.time())),
			request.path_info,
			`[(a, request.form[a].value) for a in request.form.keys()]`,
			refstr, hoststr, strFile.getvalue()))
	except:
		traceback.print_exc()
	print "HTTP/1.0 500 CGI Error"
	print "Connection: close"
	print "content-type:text/html"
	print ""
	print """<HEAD><TITLE>Uff: Fehler</TITLE></HEAD>
		<BODY>
		<H1>Schwerer Fehler</H1>
		<P>Mir gehts schlecht: Deine Anfrage hat mich so verwirrt, dass
		ich nur noch diese Meldung ausspucken kann.  Damit anderen Leuten
		das Schicksal, sowas Schlimmes zu sehen erspart bleibt, solltest
		du dem <a href="mailto:%s">UNiMUT-Webmenschen</a> Bescheid sagen und
		auf jeden Fall Folgendes in die Mail schreiben:
		<PRE>%s</PRE>
		<p>Vielen Dank.</p></body>"""%(conf.maintainer,re.sub("\n","<BR>",
		re.sub(">","&gt;",
		re.sub("<","&lt;",
		strFile.getvalue()))))
