#-*- coding: iso-8859-1 -*-

"""
Zugriff auf Termin-Caches.

Das hier unterst�tzt ggf. noch den alten Code mit den schrecklichen
UmTermin-Geschichten.  In Wirklichkeit will man aber den NEW_CODE=True haben.
Die Termine kommen dann aus dem Netz, von conf.eventServer
"""

import errno
import os
import time
import urllib.request
import warnings
from email.utils import formatdate

import conf


NEW_CODE = True


if NEW_CODE:
	class EventCache(object):
		"""Ein cache f�r Kram von einem Terminserver (sofoterm).

		Das Ding hat nur Klassenmethoden, soll also nicht instanziert werden.

		Im wesentlichen geht es um die Default-Ansicht (d.h. "normale"
		UNiMUT-Termine in HTML).  F�r diese wird nur alle Stunde mal nachgefragt,
		ob sich was ge�ndert hat.

		Ansonsten ist das hier eine Fassade f�r die URLs hinter serverName.

		In der Regel laufen mehrere Prozesse, in denen das Zeug hier passiert.
		Drum locken wir die Cache-Datei (oder lassen die Finger von ihr, wenn
		das nicht geht).  Auf die Weise kann es zu mehreren mehr oder weniger
		gleichzeitigen Anfragen an den eventServer kommen, aber das ist wohl
		egal.
		"""
		lastUpdate = 0            # unix timestamp of the last update, UTC
		updateInterval = 3600     # seconds between refreshes
		cachedContent = "Broken"  # whatever was last returned

		def __init__(self, serverName, cacheLocation):
			self.status = "Not inited"
			self.serverName, self.cacheLocation = serverName, cacheLocation
			self.lockName = self.cacheLocation+".lock"
			self._loadCache()
	
		def _unlockCache(self):
			os.unlink(self.lockName)

		def _lockCache(self):
			try:
				os.symlink(self.cacheLocation, self.lockName)
			except (os.error, IOError) as ex:
				if ex.errno!=errno.EEXIST:
					raise
				# locks older than a couple of secs are stale.
				try:
					if time.time()-os.stat(self.lockName).st_mtime>20:
						self._unlockCache()
				except os.error: # probably unlocked in the meantime, but don't care
					raise IOError("Unlocking mystery")
				self._unlockCache()
				self._lockCache()

		def _saveCache(self):
			try:
				self._lockCache()
				try:
					with open(self.cacheLocation, "wb") as f:
						f.write(self.cachedContent)
					self.lastUpdate = os.path.getmtime(self.cacheLocation)
				finally:
					self._unlockCache()
			except IOError:
				warnings.warn("Could not save cache to %s"%self.cacheLocation)

		def _loadCache(self):
			try:
				self._lockCache()  # prevent races on reading as well
				try:
					with open(self.cacheLocation, "rb") as f:
						self.cachedContent = f.read()
					self.lastUpdate = os.path.getmtime(self.cacheLocation)
				finally:
					self._unlockCache()
			except IOError:
				import traceback;traceback.print_exc()
				warnings.warn("Could not load cache from %s"%self.cacheLocation)

		def _getCacheTime(self):
			try:
				return os.path.getmtime(self.cacheLocation)
			except os.error:
				return 0

		def _retrieve(self, plainText, requireCats=[]):
			if plainText:
				queryPath = "list/plain"
			else:
				queryPath = "dyn/unimut"
			queryString = "&".join(
				["tag=%s"%urllib.parse.quote(tag) for tag in requireCats])
			url = "http://%s/%s?%s"%(self.serverName, queryPath, queryString)
			inF = urllib.request.urlopen(urllib.request.Request(url,
				headers={"If-Modified-Since":
					formatdate(timeval=self.lastUpdate, usegmt=True)}))
			data = inF.read().decode("utf-8").encode("iso-8859-1", "xmlcharrefreplace")
			return data
				
		def _refreshCacheIfNecessary(self):
			if self.lastUpdate!=self._getCacheTime():
				# some other process has changed the cache
				self._loadCache()
			if time.time()>self.lastUpdate+self.updateInterval:
				try:
					self.cachedContent = self._retrieve(False,
						requireCats=["unimut", "uni", "stura"])
					self._saveCache()
				except (urllib.error.HTTPError, urllib.error.URLError, os.error) as ex:
					self.status = "Could not retrieve events: %s"%str(ex)
					#warnings.warn("Could not retrieve events: %s"%str(ex))
				else:
					self.status = "Ok"

		def getList(self, plain=False, cats=None):
			if not plain and cats is None:
				self._refreshCacheIfNecessary()
				res = self.cachedContent
			else:
				res = self._retrieve(plain, requireCats=cats)

			return res.decode("utf-8")

		def expire(self):
			try:
				os.unlink(self.cacheLocation)
			except os.error:
				pass

	_eventCache = EventCache(conf.eventServer, conf.eventCachePath)
	
	getList = _eventCache.getList
	expire = _eventCache.expire

else:
	import UmTerminListe
	def getList(plain=0, cats=None):
		if cats is None:
			cats = ['Uni', 'Linkes', 'Lokal', 'Gremien',
			'Party', 'Soziales', 'Kultur', 'Lehramt', 'Frauen',
			'Umwelt']
		return UmTerminListe.UmTerminListe(cats=cats).asHtml(plain)
	def expire():
		pass


if __name__=="__main__":
	getList()
