# -*- encoding: iso-8859-1 -*-
"""
Verschiedene globale Konfiguration
"""

import os, localconf

# URL des nph-unimut-Skripts
if localconf.isMirror:
	homeurl = "http://umneu.stura.uni-heidelberg.de"
else:
	homeurl = localconf.mirrorURL

# Encoding f�r alles, f�r das anderweitig kein Encoding definiert ist
defaultencoding = "iso-8859-1"

# Pfad zum Hauptverzeichnis
headpath = localconf.headpath

# Pfad zu Programmen, die mensch f�r cgis braucht
cgipath = os.path.join(headpath, "cgi-bin")

# Pfad zum Anfang der Dokumente
rootpath = os.path.join(headpath, "unimut")

# Pfad zum Archiv
archivpath = os.path.join(headpath, "archiv.db")

# Pfad zu den alten Terminen (DEPRECATED, demn�chst l�schen)
terminpath = os.path.join(headpath, "termin.db")

# Pfad zur Abk�rzungsdatenbank
abkuedata = os.path.join(headpath, "writable", "abkue.data")

# Pfad zu den Proto-Wussis
wussipath = os.path.join(headpath, "writable", "wussis")

# Pfad zur Datei mit dem Tipp f�r die aktuell-Mailings (aber:
# Zugriffsroutinen in umutil)
aktuTippath = os.path.join(headpath, "writable", "aktuell.tip")

# Pfad zu den UM aktuell-Dateien
aktuellpath = os.path.join(headpath, "aktuell.db")

# Pfad zum halbkonstanten Zeug �ber UM aktuell
fixedaktupath=os.path.join(aktuellpath, "fixed.html")

# Pfad zum "May we recommend"-Text
recompath=os.path.join(aktuellpath, "maywe.html")

# Pfad zu den Schwerpunkten
spdbpath = os.path.join(headpath, "schwerpunkt.db")

# Pfad zur Dummschw�tzliste
dummpath = os.path.join(headpath, "writable", "dummschwaetz.pickle")

# Termin-Cache
eventCachePath = os.path.join(headpath, "writable", "events.cache")

# Pfad zu den Daten der Ersti-Einf�hrungen
einfPath = os.path.join(headpath, "einf.db")

# Hier l�sst der Indizierer den Index der letzten Nummer
lastPublishedIssueFile = os.path.join(headpath, "archiv.db", "lastPublished")

# Pfad zu den Volltext-Indexdateien
indexPath = os.path.join(headpath, "index")

# Pfade zu den zu indizierenden Daten
filesToIndex = [(os.path.join(headpath, a[0]),)+a[1:] for a in [
		("archiv.db/alt", "*", "AltArchiv"),
		("archiv.db", "*.html", "Archiv"),
		("aktuell.db", "[0-9]*", "Aktuell"),
		("einf.db", "*.list", "Einf"),
		("heute.db", "heute.db", "Heute"),
#		("termin.db", "utm_*", "Termin"),
		("unimut", "*.html", "DirectHtmlFile"),
		("unimut/dokumente", "*.txt", "DirectTxtFile"),
		("writable", "abkue.data", "Abkuerz"),
		("schwerpunkt.db", "*.sp", "Schwerpunkt"),
		("writable", "wussis", "Wussi"),
#		("writable", "dummschwatz.pickle", "Dummschwaetz"),
#		("writable", ["vsb-wunsch.txt",
	]]

# Adresse zum Beschweren und f�r Kontakte (kommt unter die meisten Seiten)
maintainer = "unimut@stura.uni-heidelberg.de"

# Server, der die Termine liefert
eventServer = "sofo.tfiu.de"
#eventServer = "localhost:8888"


class Error(Exception):
	pass

class TimeoutError(Exception):
	pass

class UnknownPath(Error):
	pass

class PermissionError(Error):
	pass

class ParameterError(Error):
	pass

class UserError(Error):
	pass

class Redirect(Error):
	pass
