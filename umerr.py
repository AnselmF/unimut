# -*- coding: iso-8859-1 -*-
"""
Eine Ableitung von Document, die Fehler verk�ndet -- das ist im
Augenblick ein bisschen chaotisch und sollte �berarbeitet werden.
"""

import umdoc, conf, time, traceback

class umErr(umdoc.Document):

	def __init__(self, request, message="Fehlender Fehler", errTitle=None,
		dontLog=0, *args, **kwargs):
		umdoc.Document.__init__(*(self, request)+args, **kwargs)
		
		self.status = (500, "Internal Server Error")
		self.date = time.time()
		self.message = message
		self.dontLog = dontLog

		if errTitle:
			self.title = errTitle
		else:
			self.title = "UNiMUT: Fehler"
		self.data = """<p>Die Anfrage hat zu folgendem Fehler gef�hrt:</p>
			<center><p><font color="red">%s</font></p></center>
			<p>Wenn du glaubst, dass der UNiMUT k�nnen m�sste, was
			du gerade versucht hast, sag dem <a href="mailto:%s">Webmenschen</a>
			Bescheid."""%(message,conf.maintainer)
	

	def done(self):
		self.logEntry()
		umdoc.Document.done(self)


	def logEntry(self):
		if self.dontLog:
			return
		try:
			if "REFERER" in self.request.httpHeaders:
				refstr = " ab %s"%self.request.httpHeaders["REFERER"]
			else:
				refstr = " [ohne Referrer]"
			self.request.log("Fehler bei Anfrage %s von %s%s: %s\n"%(
				self.request.path_info, self.request.remoteHost,
					refstr, self.message))
		except:
			traceback.print_exc()


class notFoundErr(umErr):

	def __init__(self,request,*args,**kwargs):

		kwargs["message"] = "Dokument nicht gefunden"
		umErr.__init__(*(self,request)+args, **kwargs)

		self.title = "Dokument nicht gefunden"
		self.data = """<p>Ich wei� nicht, was ich mit der URL "%s" anfangen
			soll -- das Dokument existiert vermutlich nicht.  Vielleicht willst du
			<a href="/unimut/search">suchen</a>?  Wenn dieser
			Fehler vom Schwobifyer ausgel�st wurde, liegts sehr wahrscheinlich
			am Javascript auf der referierenden Seite.  Vielleicht hilfts,
			Javascript abzuschalten.</p>"""%request.path_info
		self.status = (404, "Document not Found")
#		self.dontLog = 1


class parameterErr(umErr):
	
	def __init__(self, request, message, *args, **kwargs):
		kwargs["message"] = "Falsche Parameter"
		umErr.__init__(*(self,request)+args, **kwargs)

		self.title = "Falsche Parameter"
		self.status = (400, "Bad Request")
		self.message = message
		self.data = """<p align="center">%s</p>
			<p>Diese Fehlermeldung kommt von einem Programm,
		  das die Anfrage oben nicht verstanden hat.
			Wenn du die URL nicht selbst geschrieben hast, ist das ein
			schlechtes Zeichen, und wir werden es in Ordnung bringen.
			Wenn du die URL selbst geschrieben hast, solltest du dich
			sch�men.</p>"""%message


class userErr(umErr):
	
	def __init__(self, request, message, *args, **kwargs):
		kwargs["message"] = "Falsche Eingabe"
		umErr.__init__(*(self,request)+args, **kwargs)

		self.title = "Falsche Parameter"
		self.status = (400, "Bad Request")
		self.message = message
		self.data = """<p align="center">%s</p>
			<p>Etwas in der oben zitierten Eingabe gef�llt mir nicht.
			Geh doch nochmal zur�ck und bringe es in Ordnung.</p>"""%message
