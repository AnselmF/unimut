# -*- encoding: iso-8859-1 -*-
"""
Die Document-Klasse hier ist sowas wie ein kleiner proxy-Server --
sie schickt Zeug durch den Schwabifizierer
"""

import urllib.request,urllib.parse,urllib.error, urllib.parse
import re, os, socket, operator
import subprocess
import tempfile

import umdoc, conf, umutil
from functools import reduce

# Ich brauche dringend einen timeout f�r die http-Verbindungen,
# und signal.alarm bringt wohl den apache durcheinander.
# Ob das hier andere Schwierigkeiten macht, kann ich nicht
# ganz beurteilen -- es sollte aber eigentlich nur python-Kram
# betreffen, und der braucht sockets wohl nur f�r http, wo mir
# wurst ist, wenns einen Timeout gibt.
socket.setdefaulttimeout(30)

langFilters = {
	'schwob_url': 'webschwob',
#	'sax_url': 'websax',
#	'frank_url': 'frankifier',
#	'hess_url': 'iwwersedser -s',
#	'koelsch_url': 'webkoelsch-neu',
}

uniHDExceptions = [
	"www.uni-heidelberg.de",
	"www.rzuser.uni-heidelberg.de",
	"www.urz.uni-heidelberg.de",
	"www.sai.uni-heidelberg.de",
	"unimut",
	"ruprecht",
	"www.cl",
	"tucana",
	"kassiopeia.gs.uni-heidelberg.de",
]

spinner = {
	"www.friwo.de": 1,
	"www.fw-laatzen.de": 1,
	"www.caatoosee.com": 1,
	"www.zmp.de": 1,
	"www.wiesenhof.de": 1,
	"www.electronic-teamplay.de": 1,
	"www.wasundwo.com": 1,
	"www.spk-vs.de": 1,
	"www.patrick-christ.de": 1,
	"www.africatwin.de": 1,
	"www.mididata.de": 1,
	"tom-cat.net": 1,
	"home1.tiscalinet.de": 1,
	"www.ubs.ch": 1,
	"www.ubs.com": 1,
	"www.sv-oberwolfach.de": 1,
	"people.freenet.de": 1,
	"www.wiwi.tu-clausthal.de": 1,
	"www.allenovery.com": 1,
	"www.allcredo.de": 1,
	"www.page4clans.de": 1,
	"www.page4players.de": 1,
	"www.diebirgit.de": 1,
	"plunx.de": 1,
	"www.ebert.de": 1,
	"www.poose.de": 1,
	"www.wbrence.com": 1,
	"www.rappler.de.vu":1,
	"www.gamer-generation.de.vu":1,
	"www.forum4free.org":1,
	"lbbw":1,
	"www.swiss.com":1,
}

noTranslateExtensions = {
".gif": 1,
".jpg": 1,
".pdf": 1,
".ps": 1,
".pdb": 1,
".jpeg": 1,
".mpg": 1,
".avi": 1,
}


def checkPermission(targURL):
	"""Wenn diese Funktion true zur�ckgibt, wird der Zugriff
	geblockt.  Im Augenblick wird nur geguckt, ob das
	Ziel im Inneren der Uni Heidelberg liegt.  Wenn das
	so ist, wird noch versucht, ob der Zielhost mit einem
	der Strings in uniHDExceptions beginnt.  Wenn das so ist,
	ist der Zugriff erlaubt, sonst nicht.
	"""
	try:
		hostname = re.sub(":.*", "", urllib.parse.urlparse(targURL)[1])
		adr = socket.gethostbyname(hostname)
		if adr.startswith("147.142") or adr.startswith("129.206")\
			or adr.startswith("192.168") or adr.startswith("10."):
			for s in uniHDExceptions:
				if hostname.startswith(s):
					return 0
			return 1
		canonHost = hostname.lower()
		if canonHost in spinner or reduce(operator.__or__,
				list(map(lambda a, d=spinner: a in d, canonHost.split(".")))):
			return 1
	except IndexError:
		pass
	except socket.error:
		raise conf.Error("Ich finde %s nicht"%hostname)


def workAroundUrllibBug(url):
	"""Urllib doesn't like http://host?bla and needs http://host/?bla.
	A bug report is filed, meanwhile we're working around it.
	"""
	if not '?' in url:
		return url
	pre, post = url.split("?", 1)
	if "/" in pre[7:]:
		return url
	return pre+"/?"+post


import urllib.request, urllib.error, urllib.parse
class SchwobErrHandler(urllib.request.BaseHandler):
	"""This class just adds raises more appropriate errors than
	urllib2's original handler (that's still in the opener's chain,
	see below).
	"""
	def http_error_401(self, req, fp, code, msg, hdrs):
		raise conf.Error(("Die Seite %s ist passwortgesch�tzt.  Der"
			" USP �bersetzt so etwas nicht.")%req.get_full_url())

	def http_error_403(self, req, fp, code, msg, hdrs):
		raise conf.Error(("Die Seite %s ist f�r mich verboten.  K�nnte sein,"
			" dass die Seitenbetreiber kein Schw�bisch m�gen.")%(
			req.get_full_url()))
	
	def http_error_404(self, req, fp, code, msg, hdrs):
		raise conf.Error(("F�r die Seite %s habe ich ein 404 bekommen."
			" Das bedeutet meistens, dass es die Seite nicht mehr gibt,"
			" es kann aber auch sein, dass ich was falsch verstanden habe."
			" So oder so, die Seite gibt es so nicht.")%(
			req.get_full_url()))


class SchwobOpenerDirector(urllib.request.OpenerDirector):
	"""Here, I mainly want to set a sane user agent string.  But while
	I'm at it, I also set all the handlers I want.  This is a bit
	kludgey, since addheaders isn't documented, but what the heck.
	"""
	def __init__(self):
		urllib.request.OpenerDirector.__init__(self)
		self.addheaders = [('User-agent', "UNiMUT Schwobifying Proxy,"
			" http://unimut.stura.uni-heidelberg.de/schwob")]
		for handler in [urllib.request.HTTPHandler(), SchwobErrHandler(),
			urllib.request.HTTPDefaultErrorHandler(), urllib.request.HTTPRedirectHandler(),
			urllib.request.UnknownHandler()]:
			self.add_handler(handler)

SCHWOB_OPENER = SchwobOpenerDirector()


def raiseTimeout(signum, stackframe):
	raise conf.TimeoutError("Die Maschine am anderen Ende hat sich"
		" nicht rechtzeitig gemeldet")


class Document(umdoc.Document):

	def __init__(self,request,*args,**kwargs):
		umdoc.Document.__init__(*(self,request)+args, **kwargs)

		if "transVerb" in request.form:
			self.translateVerbatim(request.form)
			self.suppressTop = False
			return

		self.suppressTermine = True
		self.suppressTop = True

		for processor in list(langFilters.keys()):
			if processor in request.form:
				self.processor = processor
				break
		else:
			raise conf.Redirect(umutil.urljoin(conf.homeurl, "/schwob.html"))

		self.showFooter = 1
		if "nofooter" in request.form:
			self.showFooter = 0

#		if localconf.isMirror:
#			self.proxyURL = localconf.mirrorURL
#		else:
#			self.proxyURL = random.choice(localconf.unimutMirrorURLs)
		self.proxyURL = umutil.urljoin(conf.homeurl, "/schwob")
		if type(request.form[self.processor].value)!=type(""):
			raise conf.ParameterError("""Bitte den schwob_url-Parameter
				nur einmal angeben.  Der wahrscheinlichste Grund dieses
				Problems ist die Verwendung des �bersetzten schwob-URL-Formulars.
				Wir k�mmern uns drum; bis dahin einfach das
				<a href="/unimut/schwob">un�bersetzte Formular</a>
				verwenden.""")
		self.sourceURL = request.form[self.processor].value
		if self.sourceURL.find("//")==-1:
			if self.sourceURL.startswith("/unimut"):
				self.sourceURL = umutil.urljoin(self.proxyURL, self.sourceURL)
			else:
				self.sourceURL = "http://"+self.sourceURL
		# Keine Ahnung, woher ein doppeltes http:// kommt -- aber es
		# scheint �fter vorzukommen
		if self.sourceURL.startswith("http://http://"):
			self.sourceURL = self.sourceURL[6:]

		if (not self.sourceURL.startswith("http://unimut")
			and "guwirgul" not in self.request.form):
			self.makeShutdownInfo()
			self.suppressTop = False
			return
		self.getDoc()

		self.doMeta()
		mat = re.search(r'''(?i)<base href=['"]?([^\s'">]+)''', self.data)
		if mat:
			self.sourceURL = mat.group(1)
			self.data = re.sub("(?is)<base href.*?>", "", self.data)
		if not self.isData:
			self.fixurls()
			self.fixBody()
			self.filterThroughWebschwob()

	def makeShutdownInfo(self):
		self.title = "Abgeschaltet"
		self.data = ["""
<p>Wir haben den offenen Betrieb des UNiMUT Schwobifying Proxy aus rechtlichen
Gr�nden eingestellt, d.h. alle Seiten au�er dem UNiMUT selbst werden nicht
mehr �bersetzt.  N�heres dazu auf der
<a href="/unimut/schwob.html">Infoseite</a> zum USP.</p>
"""]

	def filterThroughWebschwob(self):
		import tempfile
		fn = tempfile.mktemp()
		noProcessChunks = {}
		processChunks = []
		noProcessOpeners = re.compile(
			"""<(script|style|nomangle)""", re.I)
		noProcessClosers = re.compile("""</(script|style|nomangle)>""", re.I)

		pos = 0
		while 1:
			matStart = noProcessOpeners.search(self.data, pos)
			if not matStart:
				processChunks.append(self.data[pos:])
				break
			matEnd = noProcessClosers.search(self.data, matStart.end())
			if not matEnd:
				processChunks.append(self.data[pos:])
				break
			processChunks.append(self.data[pos:matStart.start()])
			label = "&schwobachunk%d;"%pos
			processChunks.append(label)
			noProcessChunks[label] = self.data[matStart.start():matEnd.end()]
			pos = matEnd.end()
		try:
			toTranslate = ("\n".join(processChunks)).encode(
				conf.defaultencoding, "replace")
			processor = os.path.join(conf.headpath, "cgi-bin/%s"
				%langFilters["schwob_url"])

			result = subprocess.run(
				[processor], input=toTranslate, stdout=subprocess.PIPE)
			self.data = result.stdout.decode(conf.defaultencoding, "replace")

			for label in list(noProcessChunks.keys()):
				self.data = self.data.replace(label, noProcessChunks[label])
		finally:
			try:
				os.unlink(fn)
			except os.error:
				pass

	def doMeta(self):
		mat = re.search("""(?is)<meta name=["']schwobcontrol["'] ([^>]*?)>""",
			self.data)
		if not mat:
			return
		if mat.group(1).find("nofooter")!=-1:
			self.showFooter = 0
		if mat.group(1).find("notranslate")!=-1:
			self.raiseForbidError()

	def mangleURL(self, matchobj):
		"""wird als callback von re.sub aus aufgerufen.  matchobj braucht
		mindestens drei Gruppen: den Tag vor der URL, die URL selbst und
		den Tag nach der URL.  Zur�ckgegeben wird der komplette Tag mit
		der umgebogenen URL.
		"""
		if matchobj.group(2).find("schwobnomangle")!=-1:
			return matchobj.group(0)
		if matchobj.group(2).startswith("mailto:"):
			return matchobj.group(0)
		url = matchobj.group(2)
		if os.path.splitext(url)[1] in noTranslateExtensions:
			return matchobj.group(0)
		else:
			newTag = matchobj.group(1)+self.proxyURL+("/schwob?%s="%self.processor
				)+urllib.parse.quote_plus(umutil.urljoin(
				self.sourceURL, url)).replace("%23", "#")+matchobj.group(3)
			return newTag

	def mangleActionURL(self, matchobj):
		if matchobj.group(2).startswith("mailto:"):
			return matchobj.group(0)
		elif re.search("(?i)POST", matchobj.group(0)):
			return matchobj.group(0)
		else:
			return matchobj.group(1)+self.proxyURL+"/schwob"+matchobj.group(3)+\
				('<input type="hidden" name="%s"'%self.processor
				)+'value="%s">'%umutil.urljoin(self.sourceURL, matchobj.group(2))

	def absolutifyURL(self, matchobj):
		return matchobj.group(1)+umutil.urljoin(self.sourceURL,
			matchobj.group(2))+matchobj.group(3)

	def mangleMany(self, matchobj):
			return re.sub(r"""(?si)(href\s*=\s*["']?)([^"' >]+?)(["' >]>)""",
				self.mangleURL, matchobj.group(0))

	def hackJavascript(self, matchobj):
		"""Ein verzweifelter Versuch, mit Javascript-Krampf klarzukommen --
		soll mit einem Matchobj auf eine (<script...>)(bla)(</script>)
		RE gerufen werden.
		"""
		return re.sub(r"""(["'])([^"']+?\.(?:gif|jpe?g))(["'])""",
				self.absolutifyURL,
			re.sub(r"""(?si)(href\s*=\s*["'])([^"'(]+)(["'])""",
				self.mangleURL,
			re.sub(r"""(?si)(src\s*=\s*["'])([^"']+)(["'])""",
				self.absolutifyURL, matchobj.group())))
	
	def fixurls(self):
		"""biegt die URLs in data auf pyschwob um bzw. wandelt sie in
		absolute um, weil die Dokumentenbasis anders wird.
		"""
		self.data = \
			re.sub("(?i)</head>",
			"<base href='%s'></head>"%self.sourceURL,
#			re.sub(r"""<script [^>]*src[^>]*>(</script>)?""",
#			"<!-- Javascript referece removed by USP to save our nerves-->",
			re.sub("\x254", "</script>",
			re.sub(r"(?si)(<script[^>]+?javascript[^>]*>)([^\x254])(\x254)",
			self.hackJavascript,
			re.sub("</script>", "\x254",
			re.sub(
			r"""(background-image:\s*url\()([^)]+)(\))""",
			self.absolutifyURL,
			re.sub(
			r"""(?si)(onmouse(?:over|out|up|down)\s*=\s*["'][^>]*?\.src="""
			r"""["'])([^"']+)(["'])""",
			self.absolutifyURL,
			re.sub(
			r"""(?si)(<link\s[^>]*?href\s*=\s*["']?)([^"' >]+?)"""
			"""(["' >][^>]*>)""",
			self.absolutifyURL,
			re.sub(
			r"""(?si)(<(?:body|td|table)\s[^>]*(?:backg?round|td)\s*=\s*["']?)"""
			r"""([^"' >]+?)(["' >][^>]*>)""",
			self.absolutifyURL,
			re.sub(
			r"""(?si)(<applet\s[^>]*?code\s*=\s*["']?)([^"' >]+?)"""
			r"""(["' ].*?>|>)""",
			self.absolutifyURL,
			re.sub("""(?si)(<meta[^>]+refresh[^>]+url\s*=\s*["']?)([^"' >]+?)"""
			r"""(["' ].*?>|>)""",
			self.mangleURL,
			re.sub(
			r"""(?si)(<(?:input|img|script)\s[^>]*?src\s*=\s*["']?)([^"' >]+?)"""
			r"""(["' ].*?>|>)""",
			self.absolutifyURL,
			re.sub(r"""(?si)(<frame\s[^>]*src\s*=\s*["']?)([^"' >]+?)"""
			"""(["' >][^>]*>)""",
			self.mangleURL,
			re.sub(r"""(?si)(<form\s[^>]*action\s*=\s*["']?)([^"' >]+?)"""
			"""(["' >][^>]*>)""",
			self.mangleActionURL,
			re.sub(r"""(?si)(<(?:a|area)\s[^>]*href\s*=\s*["']?)([^"' (>]+?)"""
			"""(["' >][^>]*>)""",
			self.mangleURL, self.data))))))))))))))

	def fixBody(self):
		self.data = re.sub(r"(?i)\s*</HTML>\s*$","",
			re.sub(r"(?i)\s*</BODY>\s*$","",self.data))

	def getTop(self):
		if self.suppressTop:
			return ""
		else:
			return umdoc.Document.getTop(self)

	def getFooter(self):
		if self.showFooter:
			return """<hr><p>Prozessiert vom <a href="%s/schwob">UNiMUT
				schwobyfing proxy</a>.<br>
				<font color="red">SeitenbetreiberInnen</font>: Bitte vorm
				Abschicken von Drohmails unsere
				<a href="%s/schwob.html#wut">kl&auml;renden Worte</a> lesen.  Danke.</p>
				<p><a href="%s">Unschwob</a></body>"""%(conf.homeurl,
					conf.homeurl, self.sourceURL)
		else:
			return "</body>"
	

	def raiseForbidError(self):
		raise conf.PermissionError("""Zielurl ist gesperrt --
				Dies geschah entweder auf Wunsch des Betreibers oder liegt daran,
				dass die URL als zur Uni Heidelberg geh�rig identifiziert wurde.
				Letztere sperren wir aus Sicherheitsgr�nden bis auf einen
				Satz von Ausnahmen.  Wenn die Seite,
				die du sehen willst, ohnehin nach au�en offen ist,
				sag uns Bescheid.  Wir tragen sie dann in die
				Ausnahmeliste ein.""")

	def getDoc(self):
		"""Das sollte irgendwann mal etwas eleganter gemacht werden --
		vor allem das header-management ist nicht so wirklich berauschend.
		"""
		form = self.request.form
		parlist = list(form.keys())
		try:
			parlist.remove(self.processor)
		except ValueError:
			pass
		if parlist:
			try:
				formTag = "?"+"&".join(map(lambda a, form=form:
					a+"="+urllib.parse.quote_plus(form[a].value), parlist))
			except AttributeError:
				raise conf.Error("Irgendwas war mit der Parameterliste nicht"+\
					" in Ordnung -- ein Wert fehlte.  POST-Requests, das nur "+\
					"nebenbei, machen wir so oder so nicht.")
		else:
			formTag = ""
		if checkPermission(self.sourceURL):
			if "guwirgul" not in form:
				self.raiseForbidError()
		try:
			u = SCHWOB_OPENER.open(workAroundUrllibBug(self.sourceURL+formTag))
		except urllib.error.URLError as msg:
			self.request.log("%s, %s, %s"%(self.sourceURL, formTag, msg))
			try:
				raise conf.Error(("Mit %s ist etwas nicht in Ordnung: %s.")%(
					self.sourceURL, str(msg.reason)))
			except AttributeError:
				raise conf.Error(("Mit %s ist etwas nicht in Ordnung: %s.")%(
					self.sourceURL, str(msg)))
		except urllib.error.HTTPError as msg:
			raise conf.Error("%s hat ein Problem auf HTTP-Ebene: %s"%self.sourceURL,
				str(msg))
		except IOError as msg:
			if isinstance(msg.strerror, socket.error):
				self.request.log("urlopen sagte: %s, url=%s"%(str(msg),
					self.sourceURL+formTag))
				raise conf.Error("Die Maschine am anderen Ende meldet sich nicht."
					" L�uft da auch wirklich ein http-Daemon?")
			else:
				raise conf.Error("Ich finde %s nicht"%self.sourceURL)
		except (ValueError, AssertionError):
			raise conf.Error(("Die URL %s ist "
				"keine URL -- oder wenn, verstehe ich sie nicht.  Der "
				"wahrscheinlichste Grund f�r diesen Fehler ist Javascript, "
				"das ich nicht verstehe.  Vielleicht hilft es, Javascript "
				"abzuschalten.")%self.sourceURL)
		except socket.error as msg:
			raise conf.Error(("Der Zugriff auf %s klappt aufgrund"
				" von tiefgr�ndigen technischen Problemen (namentlich: %s) nicht")%(
				self.sourceURL, str(msg)))

		self.contentType = u.headers["content-type"]
		self.isData = self.contentType.split("/")[0]!="text"
		if self.isData:
			raise conf.Redirect(self.sourceURL)
		self.sourceCoding = "iso-8859-1"  # http 1 default
		mat = re.search("charset=([^;]*)", self.contentType)
		if mat:
			self.sourceCoding = mat.group(1)
			# cut out the charset; yeah: we ought to fiddle in our defaultcharset
			# instead.
			self.contentType = self.contentType[:mat.start()
				]+self.contentType[mat.end():]

		self.data = u.read(100000).decode(self.sourceCoding, "replace")
		self.sourceURL = u.geturl()
		u.close()

	def translateVerbatim(self, form):
		self.showFooter = 0
		self.title = "�bersetzung"
		fn = tempfile.mktemp()
		try:
			toTranslate = form.getfirst("transVerb").encode(
				conf.defaultencoding, "replace")+b"\n"
			processor = os.path.join(conf.headpath, "cgi-bin/%s"
				%langFilters["schwob_url"])

			result = subprocess.run(
				[processor], input=toTranslate, stdout=subprocess.PIPE)

			self.data = ['<p>Direkte �bersetzung durch den <a href="'
				'/unimut/schwob.html">USP</a>:</p>'
				'<p style="padding:4pt; border: 4pt dotted green;box-shadow: 8pt 8pt 5pt #ccc;">',
				result.stdout.decode(conf.defaultencoding, "replace"),
				'</p>']
		finally:
			try:
				os.unlink(fn)
			except os.error:
				pass
		self.data.append('<p>Probiers nochmal:'
			'<form action="/unimut/schwob" method="post">'
			'<textarea name="transVerb" cols="60" rows="12">'
			'%s'
			'</textarea><br>'
			'<input type="submit" value="Schw�bisch">'
			'</form></p>'%re.sub("<[^>]*>", "", form["transVerb"].value))
