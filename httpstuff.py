"""
Common http handling stuff for both unimut_modpython and nph-unimut
"""

import os

def getMimeType(fileName):
	"""tries to guess the MIME type from a file name (or actually,
	its extension.
	"""
	ext = os.path.splitext(fileName.strip())[1]
	return {".html": "text/html", ".htm": "text/html",
		".txt": "text/plain",
		".gif": "image/gif",
		".png": "image/png",
		".jpg": "image/jpeg", ".jpeg": "image/jpeg",
		".gz": "application/gzip",
		".mp3": "audio/mpeg",
		".pdf": "application/pdf",
		".js": "application/javascript",
		".ico": "image/x-icon",
		".css": "text/css"}.get(ext, "application/octet-stream")
