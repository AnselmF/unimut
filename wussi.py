# -*- encoding: iso-8859-1 -*-
"""
Dieses Modul verarztet Wusstet-Ihr-schons.

Die Idee ist, dass es ein Dokument gibt, das zu gegebener Zeit in den
UM aktuell �bernommen wird.
"""

import umdoc, conf, re, time, umutil


class Document(umdoc.Document):

	def __init__(self, request, *args, **kwargs):
		umdoc.Document.__init__(*(self, request)+args, **kwargs)
		self.replaceAbbrevs = 1
		self.moreheaders.append("pragma: no-cache")
		if request.path_info=="%s/edit"%__name__:
			if "wusstxt" in request.form and request.prefs.isMaintainer:
				self.save(request.form["wusstxt"].value)
			self.title = "Wussis �ndern"
			self.makeForm()
		elif request.path_info.startswith("%s/move"%__name__):
			self.moveToUmakt()
		else:
			self.title = "Wusstet Ihr schon..."
			self.data.append(self.getHtml())

	def makeForm(self):
		self.data.append('<form action="/unimut/%s/edit" method="post">'%__name__)
		self.data.append('<textarea name="wusstxt" cols=60 rows=20 '+
			'wrap="virtual" style="width:100%">')
		try:
			with open(conf.wussipath, encoding=conf.defaultencoding) as f:
				self.data.append(umutil.textareaEscape(f.read()))
		except IOError:
			pass
		self.data.append('</textarea><input type="submit" value="Ok">')
		self.data.append('</form>')
		self.data.append("""<p>Kurzanleitung: Wussis auf jeden Fall mit einem
			#, nach M�glichkeit mit "...dass" oder �hnlichem anfangen, ansonsten
			einfach tippen, &lt;p&gt; und �hnliches brauchts nicht.  Wenn
			genug zusammengekommen sind, per""")
		self.data.append(
			'<a href="/unimut/%s/move?%s">In den UNiMUT aktuell</a> '
			'verschicken.</p>'%(
			__name__, str(time.time())))
		self.data.append(self.getHtml())
	
	def getHtml(self):
		with open(conf.wussipath, encoding=conf.defaultencoding) as f:
			rawwuss = f.read().strip()

		if not rawwuss:
			rawwuss = """#Die Wussis sind gerade auf den
				<a href="/unimut/aktuell">UNiMUT aktuell</a>
				geschoben worden und finden sich dort.  Komm doch in
				ein paar Tagen wieder."""
		wussis = re.split("(?m)^#", rawwuss)[1:]
		wussis = ["<p>%s</p>"%re.sub(
				r"^(\.\.\.[^\s]+)", r"<strong>\1</strong>", a) for a in wussis]
		adminlink = ""
		if self.request.prefs.isMaintainer:
			adminlink = \
				'<p align="right"><a href="/unimut/wussi/edit">�ndern</a></p>'
		return '%s\n<p align="right"><em>Walter I. Sch�nlein</em></p>%s'%(
			"\n".join(wussis), adminlink)

	def save(self, newContents):
		if not self.request.prefs.isMaintainer:
			raise conf.PermissionError("Das darfst du nicht")
		with open("/tmp/gratz", "w", encoding="utf-8") as f:
			f.write(newContents)
		with open(conf.wussipath, "w", encoding=conf.defaultencoding) as f:
			f.write(umutil.cleanTextarea(newContents))

	def moveToUmakt(self):
		import aktuell
		if not self.request.prefs.isMaintainer:
			raise conf.PermissionError("Das darfst du nicht")
		theArticle = aktuell.Aktuell()
		theArticle.date = time.strftime("%d.%m.%Y", time.localtime(time.time()))
		theArticle.title = "Wusstet Ihr schon..."
		self.request.prefs.isMaintainer = 0
		theArticle.body = self.getHtml()
		self.request.prefs.isMaintainer = 1
		theArticle.save()
		self.save("")
		raise conf.Redirect("/unimut/aktuell/editrec?rec=%s"%theArticle.key)
