#!/usr/bin/env python
# -*- encoding: iso-8859-1 -*-

import urllib.request as urlrequest
import urllib.error as urlerror
import re, string, tempfile, os, conf, socket

version=1.6
shortTextLimit = 1500

class Error(Exception):
	pass

socket.setdefaulttimeout(30)

dummres=[("Markt",1), (r"\b[Ee]ffi",1), (r"white\s*paper",2),
	("[gG]lobal",2), ("professionell",1), ("Leistung",1), ("[Oo]rient",0.5),
	(r"innere\s*Sicherheit",2), ("[zZ]ukunft",0.5), ("[sS]tandort",3),
	("Produkt",1), ("[Rr]ang",1), ("Nachfrage",1), ("[aA]ttraktiv",0.5),
	("[kK]onkurrenz",1), ("[Ii]nnov",1.5), ("[pP]rojekt",0.5),
	("F�hrung",2), ("Dienstleistung",1.5), ("Dienstleister",3.5),
	("[wW]irtschaft",1), ("[uU]msatz", 1),
	("[Mm]ultimedia",1), ("[Mm]anagement",1.5), ("Lebensgef�hl",0.5),
	("[Aa]kzeptanz",0.5), ("Kundenbindung",1.5), ("ergebnisoffen",2),
	("[sS]chlank",1), ("[Kk]unde",1), ("[Kk]onjunktur",1),
	("[Kk]reati",0.25), ("Kosten",1), ("[sS]trateg",0.3), ("[Ii]nternation",0.2),
	("Weltm�rkte",3), ("[tT]echnolog",1), ("Quantenspr",2), ("[sS]ynerg",1.5),
	("B�rsenwert",3), ("Vertrauen",0.3), ("[sS]truktur",0.25),
	("erwirtschafte",1), ("vorn",0.5), (r"lean\s*management",3),
	("Arbeitspl",0.75), ("Restrukturierung",2), ("Reengineering",4),
	(r"[Cc]orporate\s*[iI]dentity",3), ("[Ii]nnovat",1),
	("[bB]usiness\s*[pP]lan",4), ("Akquisition",2), ("coaching",2),
	("[oO]utsourc",3), ("[eE]rfolg",1), (r"\s[Ff]it",1.5),
	("[Rr]essource",0.5), ("[aA]nwendung",0.75), ("[Ww]irtschaftlich",1),
	("[wW]irtschaft",0.25), ("[vV]orne",0.33), ("[eE]ntwicklung",0.5),
	(r"[nN][�ae]+chstes\s*Jahrtausend",1.5), ("[pP]rojekt",1),
	("[bB]achelor",2), ("[dD]egree",3), ("Existenz",1), ("[Ff]lexib",1.5),
	("Mobilit[�ae]+t",2), ("Hemmnis",1), ("Schl�sselkompetenz", 2),
	("[qQ]alit", 1),  ("Kooperation", 1),  ("[eE]valu", 0.7),
	("[Zz]ertifizier", 2),  ("[aA]kkreditie", 2),  ("[wW]achstum", 0.5),
	("[eE]ntwick", 0.7),  ("Evaluation", 4),  ("[Pp]ersonal", 1),
	("[mM]ittelst", 1.5),  ("[cC]ance", 1),  ("ancengleichheit", 4),
	("Lebensziel", 2),  ("gestalten", 1.5),  ("[iI]nvesti", 1.5),
	("[bB]egab", 1),  ("Wohlstand", 0.5),  ("[mM]ittelm[a�](ss|�)", 2),
	("[aA]ufst(ei|ie)g", 1),  ("starr", 0.75),  ("[Aa]ttrakt", 1),
	("Schl�sseltechno", 2),  ("Spitzen", 3),  ("[Pp]rofil", 2.5),
	("zeitnah", 1.5),  ("[kK]onsolidier", 1),  ("[Mm]arketing", 2.5),
	("Handlungsbedarf", 1),  ("Leitkultur", 3),  (r"[Kk]now\s*how", 1),
	("[Ii]nterdisziplin", 0.75),  ("[Ww]issenstransfer", 1.5),
	("[Dd]esign", 1), ("[kK]ommunikati",0.5),
	]

def rawscore(text, patterns):
	"""patterns is a list of (regular expression, score) pairs.  For each
	occurence of a pattern, the corresponding score is added to a
	cumulative score which is then returned
	"""
	score = 0
	for pat, weight in patterns:
		score += len(re.compile(pat, re.MULTILINE).findall(text))*weight
	return score


def pdfToText(pdfData):
	srcF = tempfile.NamedTemporaryFile()
	srcF.write(pdfData)
	srcF.flush()
	return os.popen("%s %s -"%(os.path.join(conf.cgipath, "pdftotext"),
		srcF.name)).read()


_entdict={"auml":'�', "Auml":'�', "ouml":'�', "Ouml":'�', "uuml":'�',
	"Uuml":'�', "szlig":'�', "228":'�', "196":'�', "246":'�', "214":'�',
	"252":'�', "220":'�', "223":'�',}


def htmlToLatin1(text):
	"""ersetzt &matchobj; durch Latin-1 (jedenfalls f�r deutsche Texte,
	und was anderes machen wir eh nicht).
	"""
	from bs4 import BeautifulSoup
	return BeautifulSoup(text, "html.parser").text


def gettitle(text):
	"""pfriemelt HTML-Titel aus text raus und gibt None zur�ck, wenn
	es keinen gibt.
	"""
	mat = re.compile(r"<TITLE>(.+?)</TITLE>",re.MULTILINE |
		re.IGNORECASE | re.DOTALL).search(text)
	if mat:
		return string.strip(mat.group(1))


def dummyfun(signum,stackframe):
	raise IOError("Die Maschine am anderen Ende hat innerhalb von 60"+\
		" Sekunden nicht geantwortet.  Ich gebs auf.")


def cleanupHTML(text):
	"""entfernt HTML-Kopf und HTML-Tags aus text
	"""
	return re.sub("<[^>]*>", "",
		re.sub("(?s)<head>.*</head>", "", text))


def computeDummquotForString(text):
	"""gibt den Dummschw�tzquotienten f�r text zur�ck.
	"""
	if len(text)<shortTextLimit:
		raise ValueError("Text too short")
	return 32000*rawscore(text, dummres)/float(len(text))


def computeDummquotForURL(theurl):
	"""gibt den Dummschw�tzkoeffizienten des Dokuments bei theurl
	zur�ck (oder l�st einen IOError oder einen ValueError aus).
	"""
	try:
		uo = urlrequest.urlopen(theurl)
		try:
			major, minor = uo.info().get("content-type", "none/none").split("/")
			if major=="text":
				converter = htmlToLatin1
			elif minor.lower()=="pdf":
				converter = pdfToText
			else:
				raise Error("No handler for this mime type")
		except (ValueError, Error):
			raise conf.ParameterError("Unter %s ist kein verwertbares Dokument"%(
				theurl))
		cont = converter(uo.read())
	except (urlerror.HTTPError, urlerror.URLError):
		import traceback; traceback.print_exc()
		raise IOError("Document gone")
	title = gettitle(cont)
	if  title==None or title=="" :
		title = theurl
	return (computeDummquotForString(cleanupHTML(cont)), title)


def computeDummquotForFile(fName):
	"""gibt den Dummschw�tzquotienten f�r den Inhalt der durch fName
	bezeichneten Datei zur�ck.
	"""
	with open(fName, "r", encoding="utf-8") as f:
		return computeDummquotForString(f.read())


if __name__=="__main__":
	import getopt, sys
	opts, args = getopt.getopt(sys.argv[1:], "h")
	if ("-h", "") in opts:
		computeFun = computeDummquotForURL
	else:
		computeFun = computeDummquotForFile
	for arg in args:
		print(arg, computeFun(arg))
