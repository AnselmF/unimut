#!/usr/bin/env python
"""
Der zentrale Dispatcher für den WSGI-UNiMUT -- analysiert die Anfragen,
baut unser internes Request-Objekt und verteilt den Kram dann auf die
einzelnen Module.

Die Apache-Konfig für sowas sieht so aus:

  WSGIScriptAlias / /home/unimut/www/cgi-bin/wsgi_unimut.py
 	WSGIDaemonProcess dsworker user=unimut group=unimut processes=1 threads=3 maximum-requests=1000
	WSGIProcessGroup dsworker
"""

import cgi
import datetime
import importlib
import os
import re
import sys
from urllib import parse as urlparse

sys.path.append("/".join(__file__.split("/")[:-1]))

import abkuerz
import conf
import prefs
import redirect
import umbin
import umdoc
import umerr
import upload


LOGGING = True


def getHttpHeaders(environ):
	return dict(
		(key[5:], value)
		for key, value in environ.items()
		if key.startswith("HTTP_"))


class Request:
	
	def __init__(self, environ, startResponse):
		self.startResponse = startResponse
		self.requestMethod = environ.get("REQUEST_METHOD", "GET")
		self.httpHeaders = getHttpHeaders(environ)
	
		self.unparsedURI = environ["REQUEST_URI"]
		self.path_info = environ.get("PATH_INFO",
			re.sub(r"\?.*", "", self.unparsedURI))

		self.remoteHost = environ.get("REMOTE_HOST",
			environ.get("REMOTE_ADDR", "[XXX anonymus]"))

		self.path_info = re.sub("/+", "/", self.path_info)
		if self.path_info.startswith("/"):
			self.path_info = self.path_info[1:]
		if self.path_info.startswith("unimut/"):
			self.path_info = self.path_info[7:]
		if self.path_info=="unimut":
			self.path_info = "/"

		self.prefs = prefs.getPrefs(environ)
		self.form = cgi.FieldStorage(
			fp=environ["wsgi.input"],
			environ=environ,
			encoding=conf.defaultencoding)

		self.document = None

	def defaultaction(self):
		d = upload.getUploadedDoc(self, self.path_info)
		if d:
			self.document = d
			return
		source = os.path.join(conf.rootpath, self.path_info)

		if os.path.exists(source):
			if os.path.isdir(source):
				raise conf.Redirect(os.path.join("/unimut",
					os.path.join(self.path_info, "index.html")))
			if os.path.isfile(source) and not source.endswith(".html"):
				self.document = umbin.Document(self, sourcefile=source)
			else:
				self.document = umdoc.Document(self, sourcefile=source)
		else:
			raise conf.UnknownPath("URI not found")

	def cgiCompat(self):
		"""redirectet ein paar antike URL aus der Zeit, als das in CGI war.

		(ganz ehrlich: Solche URIs sollte es eigentlich nicht mehr geben.)
		"""
		if self.path_info.startswith("cgi-bin/abnjs.cgi"):
			self.document = redirect.Document(self, "/unimut/abkuerz/nojs?wort=%s"%
				environ["QUERY_STRING"])
		elif self.path_info.startswith("cgi-bin/pyschwob"):
			self.document = redirect.Document(self, "/unimut/schwob?schwob_url=%s"%
				urlparse.quote_plus(environ["QUERY_STRING"]))
		elif self.path_info.startswith("cgi-bin/aktuell.py"):
			self.document = redirect.Document(self, "/unimut/aktuell?%s"%
				environ["QUERY_STRING"])
		elif self.path_info.startswith("cgi-bin/pysearch") or \
			self.path_info.startswith("cgi-bin/searcnew.cgi"):
			self.document = redirect.Document(self, "/unimut/search?%s"%
				environ["QUERY_STRING"])
		elif self.path_info.startswith("cgi-bin/dummsch.py"):
			self.document = redirect.Document(self, "/unimut/dummschwaetz")
		elif self.path_info.startswith("cgi-bin/dummsch"):
			self.document = redirect.Document(self, "/unimut/dummschwaetz")
		else:
			raise conf.UnknownPath("CGI ist abgebrannt")

	def dispatch(self):
		"""findet ein python-Modul, um den Request zu behandeln und
		konstruiert dessen Document.

		Wenn so ein Modul nicht da ist, wird defaultaction aufgerufen.
		"""
		path = self.path_info
		module = path.split("/")[0]
		if module and re.match(r"\w+$", module):
			try:
				importlib.import_module(module)
				self.document = sys.modules[module].Document(self)
				return
			except conf.Error:
				raise
			except ImportError:
				# das zweite segment war kein modul, lass defaultaction nach einer
				# Datei suchen
				pass

		if module=="cgi-bin":
			self.cgiCompat()
			return

		self.defaultaction()

	def log(self, msg):
		if LOGGING:
			sys.stderr.write(f"[{datetime.datetime.now()}] {msg}")


def output(startResponse, doc):
	startResponse(
		"{} {}".format(*doc.status),
		doc.getResponseHeaders())

	if doc.headerOnly:
		return [b""]
	
	try:
		docData = doc.getData()
		if doc.replaceAbbrevs:
			docData = abkuerz.insabkNew(docData)

		stuff = doc.getTop()+\
			docData+\
			doc.getFooter()

		if isinstance(stuff, str):
			stuff = stuff.encode(doc.encoding)

		return [stuff]
	except Exception as exc:
		startResponse(
			"500 Internal Server Error",
			[("content-type", "text/plain;charset=utf-8")],
			sys.exc_info())
		return str(exc).encode("utf-8")


def application(environ, startResponse):
	try:
		request = Request(environ, startResponse)
		try:
			if "HTTP_HOST" in environ:
				if environ["HTTP_HOST"].startswith("www"):
					raise conf.Redirect(environ["REQUEST_URI"])

			request.dispatch()
			if not request.document:
				raise conf.Error("Diese URL hat mich verwirrt") # Shouldn't happen
		except conf.Redirect as newurl:
			request.document = redirect.Document(request, str(newurl))
		except conf.UnknownPath:
			request.document = umerr.notFoundErr(request)
		except conf.ParameterError as msg:
			request.document = umerr.parameterErr(request, message=msg)
		except conf.UserError as msg:
			request.document = umerr.userErr(request, message=msg)
		except conf.Error as msg:
			request.document = umerr.umErr(request, message=msg)
		except Exception as msg:
			import traceback; traceback.print_exc()
			request.document = umerr.umErr(request, message=msg)
		request.document.done()

		return output(startResponse, request.document)
	except Exception:
		# TODO: do a 500
		raise


if __name__=="__main__":
	import io

	for fragment in application({
		"REQUEST_URI": "/",
		"wsgi.input": io.BytesIO(),
	}, lambda stat, hdrs: sys.stdout.write(f"{stat}\n{hdrs}\n\n")):
		print(fragment)
