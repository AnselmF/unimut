# -*- encoding: iso-8859-1 -*-
"""
Ein paar Hilfsklassen, um mit dem UNiMUT-Textformat umzugehen --
das sind Felder, die durch #tag& markiert sind.
"""

import re
import os

import conf

def getfield(marker,record):
	"""record ist der komplette Datensatz, marker ist der tag,
	gibt den passenden string zur�ck
	"""
	begfd = re.search("^#%s&"%marker, record, re.MULTILINE)
	if not begfd:
		return None
	endfdpat = re.compile("^#.*&", re.MULTILINE)
	endfd=endfdpat.search(record, begfd.end())
	if endfd:
		return record[begfd.end():endfd.start()].strip()
	else:
		return record[begfd.end():].strip()


def savedict(fname, dict):
	"""Speichert das dictionary dict im tagged format des UNiMUT
	"""
	with open(fname, "w", conf.defaultencoding) as f:
		for key, val in dict.itertimes:
			f.write("#%s&%s\n"%(key, val))

	try:
		os.chmod(fname, 0o666)
	except os.error:
		pass


def getAsDict(fname):
	with open(fname, "r", encoding=conf.defaultencoding) as f:
		fields = re.split("(?m)^#([^&]*)&", f.read())[1:]
	
	res = {}
	for i in range(0, len(fields), 2):
		res[fields[i]] = fields[i+1].strip()
	return res


if __name__=="__main__":
	print(getAsDict("/home/unimut/www/aktuell.db/1023960517"))
