"""
Einige Zeilen zum Spielen mit logs
"""

import re, operator
from functools import reduce

def filteredText(fName, selectRes, stopRes):
	stopPatterns = list(map(re.compile, stopRes))
	selectPatterns = list(map(re.compile, selectRes))
	for line in open(fName):
		if not reduce(operator.or_, 
				[pat.search(line) for pat in selectPatterns]):
			continue
		for pat in stopPatterns:
			if pat.search(line):
				break
		else:
			yield line

class AccessLog:
	logLinePat = re.compile(r'(?P<ip>[\d.]+) '
			r'\[(?P<agent>.*?)\] '
			r'\[(?P<referrer>[^\]]*)\] '
			r'- - \[(?P<date>[^\]]*)\] '
			r'"GET (/unimut)?(?P<uri>[^ ]+)[^"]*" ')

	keyToIndex = {
		"ip": 0, "uri": 1, "date": 2, "agent": 3, "referrer": 4
	}
	def __init__(self, logFile="/var/www/apache/access.log"):
		self.logFile = logFile
		self.logEntries = []
		self._getLogLines()

	def _addLogLine(self, rawLine):
		mat = self.logLinePat.search(rawLine)
		if mat:
			self.logEntries.append((mat.group("ip"), mat.group("uri"),
				mat.group("date"), mat.group("agent"), mat.group("referrer")))
	
	def _getLogLines(self):
		for line in filteredText(self.logFile, 
				selectRes=[
					"GET"
				],
				stopRes=[
					"[Cc]rawl|[Bb]ot|[Ss]lurp|htdig",
					"Bloglines|Feedfetcher|Attensa",
					r'GET[^"]*\.(gif|js|jpg|png|ico|css)'
				]):
			self._addLogLine(line)
	
	def getEntries(self, dictKey="uri"):
		targetIndex = self.keyToIndex[dictKey]
		result = {}
		for rec in self.logEntries:
			result.setdefault(rec[targetIndex], []).append(rec)
		return result

if __name__=="__main__":
	l = AccessLog()
	reqs = l.getEntries()
	freqAndUri = [(len(v), k) for k,v in reqs.items()]
	freqAndUri.sort()
	print("\n".join(["%03d %s"%(freq, uri) for freq,uri in freqAndUri]))
