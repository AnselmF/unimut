"""
A pure-python implementation of the bisect-in-text algorithm employed
by look(1)
"""



import mmap
import os
import re


class Looker(object):
	"""A facade for a bisectable file.

	This is basically like look(1), except we don't support all the fancy
	case-folding and whitespace-ignoring options.

	The content of the file object passed in must be sorted according to
	the C locate string order.

	Construct this either with a file name or a binary file opened for
	reading.
	"""
	def __init__(self, f, lineSep=b"\n", encoding="utf-8"):
		if isinstance(f, str):
			f = open(f, "rb")

		self.f = f
		self.lineSep = lineSep
		self.encoding = encoding
		self.map = mmap.mmap(self.f.fileno(), 0, access=mmap.ACCESS_READ)
		self.dataLength = len(self.map)

	def _skipPastNewline(self, index):
		"""returns the index of the first character behind the next available
		newline.

		If there is no such character, a ValueError is raised.
		"""
		ind = self._skipLine(index)
		if ind==self.dataLength:
			raise ValueError("At end of file")
		return ind

	def _skipLine(self, index):
		"""as _skipPastNewline, but returns self.dataLength if no newline
		is found.
		"""
		ind = self.map.find(self.lineSep, index)+1
		if ind==0:
			return self.dataLength
		return ind

	def _compare(self, needle, front):
		"""returns -1, 0 or 1 when needle is smaller, equal to, or larger than
		map[front:], respectively.
		"""
		toCompare = self.map[front:front+len(needle)]
		if needle==toCompare:
			return 0
		elif needle<toCompare:
			return -1
		else:
			return 1

	def _binarySearch(self, needle):
		"""returns start and back index of a region within self.map that might
		contain needle at the start of a line.
		"""
		frontInd, backInd = 0, self.dataLength
		cur = frontInd+(backInd-frontInd)//2
		cur = self._skipPastNewline(cur)

		while cur<backInd and backInd>frontInd:
			if self._compare(needle, cur)>0:
				frontInd = cur
			else:
				backInd = cur
			cur = frontInd+(backInd-frontInd)//2
			cur = self._skipLine(cur)

		return frontInd, self._skipLine(backInd)

	def _collect(self, matchInd, needle, limit):
		"""returns a list of up to limit lines starting with needle.
		"""
		res = []
		while self._compare(needle, matchInd)==0 and limit:
			try:
				nextInd = self._skipPastNewline(matchInd)
			except ValueError: # at end of file, just add the rest of the file
				line = self.map[matchInd:]
				if line.endswith(self.lineSep):
					line = line[:-len(self.lineSep)]
				res.append(line.decode(self.encoding))
				break

			else:
				res.append(self.map[matchInd:nextInd-len(self.lineSep)
					].decode(self.encoding))
				matchInd = nextInd
				limit -= 1
		return res

	def look(self, needle, limit=40):
		"""returns a list of lines matching needle.

		The function does not return more than limit matches.
		"""
		if isinstance(needle, str):
			needle = needle.encode(self.encoding)

		try:
			startInd, windowEnd = self._binarySearch(needle)
		except ValueError:
			return []

		# Now that we have a small window, find the first instance within it;
		# We require it to be at the start of a line; therefore, we
		# check the startInd (which can be the start of the file) directly,
		# after the start of the file needle has to be behind a line separator.
		if self._compare(needle, startInd)==0:
			firstMatch = startInd
		else:
			try:
				firstMatch = self.map.find(self.lineSep+needle,
					max(0, startInd-len(self.lineSep)), windowEnd)
			except TypeError:
				# Workaround for python 2.5 bug with the third find arg
				startInd = max(0, startInd-len(self.lineSep))
				window = self.map[startInd:windowEnd]
				firstMatch = window.find(self.lineSep+needle)+startInd

			if firstMatch==-1:
				return []
			firstMatch = firstMatch+len(self.lineSep)

		return self._collect(firstMatch, needle, limit)
	
	def reSearch(self, needle, start=0, end=None):
		"""returns an re.MatchObject object for the first match of needle.

		This obviously does a linear search; it is just here so we don't
		need to expose the underlying string.

		You will probably want to use the multiline (?m) flag.  Also note
		that linesep is ignored for reSearch.
		"""
		if end is None:
			end = self.dataLength

		if isinstance(needle, str):
			needle = needle.encode(self.encoding)
		if isinstance(needle, bytes):
			needle = re.compile(needle)

		return needle.search(self.map, start, end)

	def reFindall(self, needle, limit=500):
		"""returns a list of re.MatchObjects for needle in the map.

		Warning: This will be matches over *bytes* even if you pass
		in a string.  You'll need to decode these in general.
		"""
		matches = []
		pos = 0
		for i in range(limit):
			mat = self.reSearch(needle, pos)
			if mat:
				matches.append(mat)
				pos = mat.end()
			else:
				break

		return matches


def _getTestSuite():
	"""returns a unittest suite for this module.

	It's in-file since I want to keep the thing in a single file.
	"""
	import contextlib
	import tempfile
	import unittest

	@contextlib.contextmanager
	def lookerForData(data):
		handle, name = tempfile.mkstemp()
		with os.fdopen(handle, "w", encoding="utf-8") as f:
			f.write(data)

		f = open(name, "rb")
		try:
			yield Looker(f)
		finally:
			f.close()
			os.unlink(name)

	class LowLevelTest(unittest.TestCase):
		def testSkipSimple(self):
			with lookerForData("abc\ndef\nh") as l:
				self.assertEqual(l._skipPastNewline(0), 4)
				self.assertEqual(l._skipPastNewline(2), 4)
				self.assertEqual(l._skipPastNewline(4), 8)

		def testFailProperEnd(self):
			with lookerForData("abc\ndef\nh") as l:
				self.assertRaises(ValueError,
					l._skipPastNewline, 8)

		def testFailBlankEnd(self):
			with lookerForData("abc\ndef") as l:
				self.assertRaises(ValueError,
					l._skipPastNewline, 4)


	class LookingTest(unittest.TestCase):
		def testLookSimple(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual(l.look("noo"), ["noo"])

		def testLookFirst(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual(l.look("ar"), ["aroo"])

		def testLookLast(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo\n") as l:
				self.assertEqual(l.look("zoo"), ["zoo"])

		def testLookNonASCII(self):
			with lookerForData('Plantae\nPlant\xeda\nPlat\xf3n') as l:
				self.assertEqual(l.look("Plant\xed"), ["Plant\xeda"])
				self.assertEqual(l.look("Plat"), ["Plat\xf3n"])
				self.assertEqual(l.look("Pla"), [
					'Plantae', 'Plant\xeda', 'Plat\xf3n'])

	class LookingAwayTest(unittest.TestCase):
		def testLookSimple(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual(l.look("!noo"), [])

		def testLookFirst(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual(l.look("nar"), [])

		def testLookLast(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo\n") as l:
				self.assertEqual(l.look("~zoo"), [])

	class NoCrashTest(unittest.TestCase):
		def testNotSorted(self):
			with lookerForData("patz\ngatz\n\x90rks\nabot\n") as l:
				self.assertEqual(l.look("x"), [])

	class ReTest(unittest.TestCase):
		def testBasic(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual(l.reSearch("r.o").group(), b'roo')

		def testAtend(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual(l.reSearch("(?m)^z..").group(), b'zoo')

		def testMulti(self):
			with lookerForData("aroo\nfoo\nnoo\nzoo") as l:
				self.assertEqual([m.group() for m in l.reFindall("(?m)^.oo")],
					[b'foo', b'noo', b'zoo'])

	l = locals()
	tests = [l[name] for name in l
		if isinstance(l[name], type) and issubclass(l[name], unittest.TestCase)]
	prefix = "test"

#	tests, prefix  = [LookingTest], "testLookLast"

	loader = unittest.TestLoader()
	loader.testMethodPrefix = prefix
	suite = unittest.TestSuite([loader.loadTestsFromTestCase(t)
		for t in tests])
	return suite


def _test():
	import unittest, doctest
	import looker
	suite = _getTestSuite()
	suite.addTest(doctest.DocTestSuite(looker))
	unittest.TextTestRunner().run(suite)


if __name__=="__main__":
	_test()
