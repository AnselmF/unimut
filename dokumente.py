# -*- coding: iso-8859-1 -*-
"""
Noch eine ziemlich minimale Ableitung von umdoc:  in dokumente lagert
Zeug, das einfach unver�ndert an den/die BenutzerIn �bertragen werden
soll."""

import os, sys, conf, glob, re
import umdoc, httpstuff
import conf


class Document(umdoc.Document):

	def __init__(self, request, *args, **kwargs):
		umdoc.Document.__init__(self, request, *args, **kwargs)
		sourcefile = os.path.join(conf.rootpath, request.path_info)
		if not os.path.exists(sourcefile):
			raise conf.UnknownPath("Nicht gefunden")

		if os.path.isfile(sourcefile):
			self.sourcefile = sourcefile
			self.content_type = httpstuff.getMimeType(sourcefile)
			self.decorate = 0
			self.binary = not self.content_type.startswith("text")

			if self.binary:
				with open(self.sourcefile, "rb") as f:
					self.data = f.read()
			else:
				with open(self.sourcefile, "r", encoding=conf.defaultencoding) as f:
					self.data = f.read()
				self.data = re.sub("(?i)</body>", """<hr><p>Die Rechte an diesem
					Dokument verbleiben bei der/dem AutorIn.  Der UNiMUT distanziert
					sich von allem, was hier drin steht.
					Insbesondere gilt unser <a href="/impressum.html">Impressum</a>
					f�r diese Seiten <strong>nicht</strong>.  Wenn irgendwer sich
					durch irgendwas hier verletzt f�hlt, nehmen wir es nat�rlich
					sofort vom Netz.</p></body>""",
					re.sub("(?si)(<body.*?>)",
					r"""\1<p><font color="red">Dies hier ist ein Dokument,
					das nicht vom UNiMUT geschrieben wurde.  Der UNiMUT findet das,
					was hier steht, bestimmt entweder bescheuert oder total gut.</font>
					</p>""", self.data))
		else:
			raise conf.UnknownPath("Ich mach keine Directories")

	def getTop(self):
		if self.content_type=="text/plain":
			return re.sub(r"(?m)^\s+","",
			"""Das hier ist ein Text, den der UNiMUT nicht direkt         I
				geschrieben hat, den wir aber entweder total schei�e oder  I|
				beeindruckend gut finden.                                  I|
				============================================================|
				 ------------------------------------------------------------
				""")
		else:
			return ""


	def getFooter(self):
		if self.content_type=="text/plain":
			return re.sub(r"(?m)^\s+","","""\n ----
				Die Rechte f�r dieses Dokument verbleiben bei der/dem AutorIn.  Der
				UNiMUT distanziert sich von allem, was hier drin steht.\n""")
		else:
			return ""


	def index(self):
		"""
		gibt ein Inhaltsverzeichnis des Dokumentenverzeichnis"""

		self.title = "UNiMUT -- Dokumente"
		self.date = os.path.getmtime(os.path.join(conf.rootpath,"dokumente"))
		self.getTop = lambda self=self:umdoc.Document.getTop(self)

		self.data = ["""<p>Hier finden sich Geschichten, die aus verschiedenen
			Gr�nden von anderen Servern gespiegelt sind.  Wahrscheinlich
			sind die Links unten nicht so aufschlussreich.  Trotzdem:
			<ul>"""]
		os.chdir(os.path.join(conf.rootpath,"dokumente"))
		files = glob.glob("*")
		files.sort()
		for a in files:
			self.data.append('<li><a href="/dokumente/%s">%s</a></li>'%(a,a))
		self.data.append("</ul>")
