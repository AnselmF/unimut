# -*- encoding: iso-8859-1 -*-
"""
Dieses Modul verwaltet die Ersti-Einf�hrungen.

(das macht der B�rodienst jetzt irgendwo auf den stura-Seiten)

Am Ende jedes Semesters sollte die Ausgabe dieses Skripts gespeichert
und nach features geschoben werden.  Anschlie�end m�ssen mit der
clearVars-Methode von EinfCollection die alten Zeiten entfernt werden.

Na ja, de facto sollte dann wohl jeweils die einf.[so|wi]se.template-
Datei nach einf.list verschoben werden.  In einf.db ist ein README dazu.
"""

import re, os, sys

import conf, umdoc, umutil


class Einf(dict):
	def __init__(self, fach=None, savedString=None):
		dict.__init__(self)
		self.keys = ["fach", "fixFs", "varFs", "varInst", "fixInst", "common",
			"link"]
		self.defaults = [None, "", "", "", "", "0", ""]
		for key, val in zip(self.keys, self.defaults):
			self[key] = val
		if not fach is None:
			if fach=="":
				raise conf.ParameterError("Fach darf nicht leer sein")
			self["fach"] = fach  # "Immutable"
		elif not savedString is None:
			for key, val in zip(self.keys, savedString.split("\t")):
				self[key] = val.strip()
		else:
			raise conf.Error("Attempted to create empty Einf")
	
	def __str__(self):
		return "\t".join([self[key].replace("\t", " ")
			for key in self.keys]).replace("\n", " ").replace("\r", "")

	def __cmp__(self, other):
		if self["fach"]<other["fach"]:
			return -1
		elif self["fach"]==other["fach"]:
			return 0
		else:
			return 1

	def setField(self, key, value):
		if not key in self:
			raise KeyError("Invalid Field: %s"%key)
		self[key] = value

	def _getInfoFor(self, fixKey, varKey):
			if self[fixKey]:
				varsList = re.split(r"(\d+)\s*=", self[varKey])[1:]
				varDict = {}
				for ind in range(len(varsList)/2):
					varDict[int(varsList[ind*2])] = varsList[ind*2+1].strip()
				return re.sub(r"\$(\d+)", lambda mat, vd=varDict:
					vd.get(int(mat.group(1)), "<em>Termin noch nicht bekannt</em>"),
					self[fixKey])
			else:
				return "&nbsp;"

	def _asHtmlReal(self):
		fsInfo = self._getInfoFor("fixFs", "varFs")
		instInfo = self._getInfoFor("fixInst", "varInst")
		# Hack, hack, hack
		if instInfo.find("Termin noch nicht bekannt")!=-1:
			instInfo = ('Siehe <a href="http://www.uni-heidelberg.de/studium'
				'/imstudium/beginn/angebote.html">ZSW-Seite</a>')
		if int(self["common"]):
			datePart = '<td width="75%%" colspan="2">%s</td>'%fsInfo
		else:
			datePart = ('<td width="35%%">%s</td>'
				'<td width="40%%">%s</td>')%(fsInfo, instInfo)
		if self["link"]:
			fachPart = '<a href="%s">%s</a>'%(self["link"], self["fach"])
		else:
			fachPart = self["fach"]
		return '<tr><td width="25%%">%s</td>%s</tr>'%(fachPart, datePart)

	def asHtml(self):
		try:
			return self._asHtmlReal()
		except ValueError:
			return ""

	def getEditForm(self):
		commoncheck = ""
		try:
			if int(self["common"]):
				commoncheck = "checked"
		except ValueError:
			pass
		valdict = {"commoncheck": commoncheck,
			"targScript": "/unimut/"+__name__+"/savevals"}
		for key in self.keys:
			valdict[key] = umutil.textareaEscape(self[key])
		return ('<form action="%(targScript)s" method="post">'
			'<input type="hidden" name="fach" value="%(fach)s">'
			'<table><tr><td>'
			'Fachschaft/variabel:<br>'
			'<textarea name="varFs" cols="40" rows="5" wrap="virtual">'
			'%(varFs)s</textarea></td><td>'
			'Fachschaft/fix:<br>'
			'<textarea name="fixFs" cols="40" rows="5" wrap="virtual">'
			'%(fixFs)s</textarea></td></tr>'
			'<tr><td>'
			'Institut/variabel:<br>'
			'<textarea name="varInst" cols="40" rows="5" wrap="virtual">'
			'%(varInst)s</textarea></td><td>'
			'Institut/fix:<br>'
			'<textarea name="fixInst" cols="40" rows="5" wrap="virtual">'
			'%(fixInst)s</textarea></td></tr>'
			'<tr><td><input type="checkbox" name="common" value="1"%(commoncheck)s>'
			'Gemeinsame Veranstaltung</td><td>Link:<br>'
			'<input type="text" name="link" value="%(link)s" size="40">'
			'</td></tr>'
			'<tr><td><input type="submit" value="ok"></td>'
			'</tr></table></form>'
			'<p>Im variablen Teil k�nnen per 1=23.3., 2=37.8. oder auch'
			' 27=superprof Variablen belegt werden, die dann im fixen Teil'
			' durch $1, $2 oder auch $27 referenziert werden.  Z.b. ergibt'
			' ein 1=gro� 2=klein im variablen Teil und ein $1e Leute'
			' sollen nicht auf $2e Leute spucken: $3 im fixen Teil'
			' etwas wie "gro�e Leute sollen nicht auf kleine Leute'
			' spucken: Termin noch nicht bekannt" (Letzeres, weil das'
			' f�r unbelegte Variablen eingesetzt wird).</p>'
			' <p><strong>Wichtig:</strong> Um Terminbelegungen oder sowas zu'
			' <em>l�schen</em>, ein <em>Leerzeichen</em> in das entsprechende'
			' Feld schreiben.  Es einfach ganz leer lassen reicht <em>nicht</em>.</p>'
			' <p>Und: <em>nie</em> Termine in Fachschaft/fix oder Institut/fix'
			' schreiben.  Der Rechner kann nicht Termine von Adressen unterscheiden,'
			' und so kommen dann die l�ngst vergangenen Termine ins n�chste Semester,'
			' was viel Verwirrung stiften wird.</p>')%valdict

	def saveEdits(self, form):
		for key in self.keys:
			if key in form:
				self[key] = umutil.cleanTextarea(form[key].value)
		if "common" not in form:
			self["common"] = "0"


class EinfCollection(list):

	def __init__(self, srcName=None):
		list.__init__(self)
		self.index = {}
		if not srcName is None:
			self.loadFrom(srcName)

	def append(self, item):
		list.append(self, item)
		self.index[item["fach"]] = item

	def insort(self, item):
		import bisect
		bisect.insort(self, item)

	def getByName(self, key):
		return self.index[key]

	def loadFrom(self, inFName):
		with open(inFName, encoding=conf.defaultencoding) as f:
			for l in f.readlines():
				self.append(Einf(savedString=l))
	
	def saveTo(self, outFName):
		wList = []
		for einf in self:
			wList.append(str(einf))
		if os.path.exists(outFName):
			os.rename(outFName, outFName+".bak")
		with open(outFName, "w", encoding=conf.defaultencoding) as f:
			f.write("\n".join(wList)+"\n")
	
	def asHtml(self):
		return ('<table class="einflist">'
			'<tr><th>Fach</th><th>Einf�hrung der Fachschaft</th>'
			'<th>Einf�hrung des Instituts</th></tr>'
			'%s</table>'%"\n".join([e.asHtml() for e in self]))


class Document(umdoc.Document):

	def __init__(self, request, *args, **kwargs):
		raise conf.Redirect("http://www.stura.uni-heidelberg.de/referate-arbeitskreise/oeffentlichkeitsarbeit-agitation/erstsemester-orientierungseinheiten.html")
		umdoc.Document.__init__(*(self,request)+args, **kwargs)
		self.suppressTermine = 1
		self.replaceAbbrevs = 1
		self.einfListPath = os.path.join(conf.einfPath, "einf.list")
		self.einfs = EinfCollection(self.einfListPath)
		path = request.path_info
		if path==__name__:
			self.showList()
		elif path==__name__+"/edit":
			self.edit(request.form)
		elif path==__name__+"/savevals":
			self.savevals(request.form)
		else:
			raise conf.UnknownPath
	
	def showList(self):
		with open(
				os.path.join(conf.einfPath, "title"),
				encoding=conf.defaultencoding) as f:
			self.title = f.read()
		with open(os.path.join(conf.einfPath, "preblurb.html"),
				encoding=conf.defaultencoding) as f:
			self.data.append(f.read())

		self.data.append(self.einfs.asHtml())

		with open(os.path.join(conf.einfPath, "postblurb.html"),
				encoding=conf.defaultencoding) as f:
			self.data.append(f.read())

	def getFileEditForm(self, fName):
		targFName = os.path.join(conf.einfPath, os.path.basename(fName))
		with open(targFName, encoding=conf.defaultencoding) as f:
			valdict = {
				"tx": umutil.textareaEscape(f.read()),
				"fach": os.path.basename(fName),
				"targScript": "/unimut/"+__name__+"/savevals"}
		return ('<form action="%(targScript)s" method="post">'
			'<input type="hidden" name="fach" value="%(fach)s">'
			'<textarea name="tx" cols="80" rows="10" wrap="virtual">'
			'%(tx)s</textarea>'
			'<br><input type="submit" value="ok">')%valdict

	def saveEditedFile(self, form):
		fname = os.path.join(conf.einfPath, os.path.basename(form["fach"].value))
		tx = umutil.cleanTextarea(form["tx"].value)
		with open(fname, "w", encoding=conf.defaultencoding) as f:
			f.write(tx)


def parseFromHtml(htmlTx):
	htmlTx = re.sub(r"\s+", " ", re.sub("&nbsp;", " ", htmlTx))
	einfs = EinfCollection()
	for einfTx in re.findall("(?s)<tr>(.*?)</tr>", htmlTx):
		parts = re.findall("(?s)<td[^>]*>(.*?)</td>", einfTx)
		mat = re.search('<a href="(.[^"]*)"[^>]*>(.*)</a>', parts[0])
		if mat:
			_, fach = mat.groups()[0], mat.groups()[1][:-1]
		else:
			_, fach = "None", parts[0][:-1]
		einf = Einf(fach=fach)
		if len(parts)==2:
			einf.setField("common", "1")
			einf.setField("fixFs", parts[1])
		elif len(parts)==3:
			einf.setField("fixFs", parts[1])
			einf.setField("fixInst", parts[2])
		else:
			print("Invalid Entry: %s"%einfTx)
		einfs.append(einf)
	einfs.saveTo("einf.list")


if __name__=="__main__":
	#parseFromHtml(open("zw").read())
	e = EinfCollection(sys.argv[1])
	for einf in e:
		einf["varFs"] = ""
		einf["varInst"] = ""
	e.saveTo(sys.argv[2])
