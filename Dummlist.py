"""
Hier werden Anfragen an die Dummschwätzliste verarbeitet -- auch hier
werden alte Klassen gewrappt, weswegen das ein bisschen doof ist.
"""

import pickle, math, os, time
import urllib.parse as urlparse

import dummrating, umdoc, conf

verybig=100000 # größer sollte kein DQ sein


class Dummdok:
	"""
	Ein Dokument mit Ranking, Titel und URL"""

	def __init__(self,url):
		self.url = url
		self.rating = None
		self.title = None
		self.failcount = 0
		self.version = None

	def compute(self):
		try:
			self.rating, self.title = dummrating.computeDummquotForURL(self.url)
			self.version = dummrating.version
			self.failcount = 0
		except KeyboardInterrupt:
			raise KeyboardInterrupt("^C is da")
		except ValueError:
			self.failcount = self.failcount+1
			raise ValueError("Invalid Text")
		except:
			import traceback; traceback.print_exc()
			self.failcount = self.failcount+1
		return self

	def getUrl(self, encoded=0):
		if encoded:
			return urlparse.quote_plus(self.url)
		else:
			return self.url


class Dummlist:
	"""
	Die ganze Liste von Dummschwätz-gerankten Dokumenten
	Diese Klasse sollte die Liste immer sortiert halten"""


	def __init__(self, source=None, forMaintainer=False):
		self.forMaintainer = forMaintainer
		if source:
			self.source = source
		else:
			self.source = conf.dummpath
		try:
			with open(self.source, "rb") as f:
				self.dummlist = pickle.load(f, encoding="iso-8859-1")
		except IOError:
			self.dummlist = []


	def clean(self):
		toRemove = []
		for d in self.dummlist:
			if d.failcount>2:
				toRemove.append(d)
		for d in toRemove:
			self.dummlist.remove(d)


	def add(self, url):
		for d in self.dummlist:
			if url==d.url:
				break
		else:
			d = Dummdok(url)
			self.dummlist.append(d)
		try:
			d.compute()
		except ValueError:
			d.failcount += 1
			raise conf.Error("Dokument nicht beurteilbar (wahrscheinlich zu kurz)")
		if d.rating==None:
			self.dummlist.remove(d)
			return
		self.dummlist.sort(key=lambda item: -item.rating)
		rank = 1
		for a in self.dummlist:
			a.rank = rank
			rank = rank+1


	def remove(self, url=None, dok=None):
		try:
			if url:
				for a in self.dummlist:
					if a.url==url:
						self.dummlist.remove(a)
			else:
				self.dummlist.remove(dok)
		except ValueError:
			raise conf.Error("Dokument nicht in DB")


	def save(self):
		self.dummlist.sort(key=lambda item: item.rating)
		with open(self.source, "wb") as f:
			pickle.dump(self.dummlist, f, encoding="iso-8859-1")
		try:
			os.chmod(self.source, 0o666)
		except os.error:
			pass


	def gethist(self):
		"""
		gibt ein Histogramm der Liste zurück -- jedes bin wird durch ein
		Tupel (minval,maxval,items_in_bin) beschrieben."""

		dl = [a for a in self.dummlist if a.rating!=None]
		_,lmax = 0,dl[0].rating
		binsize = lmax/10
		logbinsz = math.floor(math.log10(binsize))
		binsize = math.pow(10, logbinsz)/2
		try:
			histsize = int(math.ceil(lmax/binsize)+1)
		except ZeroDivisionError:
			histsize = 1
			binsize = lmax+1
		histo = [0]*histsize
		for a in dl:
			idx = int(math.floor(a.rating/binsize))
			histo[idx] = histo[idx]+1
		for i in range(len(histo)):
			histo[i] = (binsize*i,binsize*(i+1),histo[i])
		return histo

		
	def htmlHisto(self):
		histo = self.gethist()
		scf = 400/float(max([a[2] for a in histo]))
		reslist = ["<TABLE>"]
		for a in histo:
			reslist.append('<TR><TD>'+
				'<a href="/unimut/dummschwaetz/interval?lower=%f&upper=%f">'%(
				a[0],a[1]))
			reslist.append("%3.1f...%3.1f</a></TD>\n"%(a[0],a[1]))
			reslist.append("<TD><STRONG>%d</STRONG></TD>"%(int(math.floor(a[2]))))
			reslist.append('<TD><HR NOSHADE ALIGN="LEFT" SIZE=10 WIDTH=%d>'%(
				int(math.floor(a[2]*scf))))
			reslist.append("</TD></TR>\n")
		reslist.append("</TABLE>")
		return "".join(reslist)


	def htmlListe(self, lower=0, upper=verybig):
		"""
		formatiert die Einträge in dummlist als HTML-Tabelle"""

		reslist = ["<TABLE BORDER=1 WIDTH=100%><TR><TH>URL/Titel</TH>"+\
			"<TH>DQ<SUB>version</SUB></TH></TR>"]
		for a in self.dummlist:
			if a.rating<lower or a.rating>upper:
				continue
			bargraphstr='<HR NOSHADE ALIGN="LEFT" COLOR="RED" SIZE=6 '+\
				'WIDTH=%d>'%int(a.rating*1.5)
			printdict = a.__dict__
			printdict['bgs'] = bargraphstr
			if self.forMaintainer:
				printdict['update'] = ('<a href="/unimut/dummschwaetz/addurl'+
					'?addurl=%s">Update</a>'%a.getUrl(1))+\
					(' <a href="/unimut/dummschwaetz/removeurl'+
					'?removeurl=%s">Remove</a>'%a.getUrl(1))
			else:
				printdict['update'] = ""
			reslist.append("""<TR><TD><STRONG>%(rank)s</STRONG>:
				<a href="%(url)s">%(title)s<BR>%(bgs)s</a></TD><TD ALIGN="CENTER">
				%(rating)3.1f<SUB>%(version)1.2f</SUB>%(update)s</TD></TR>"""%printdict)
		reslist.append("</TABLE>")
		return "\n".join(reslist)


	def __getitem__(self,ind):
		return self.dummlist[ind]


	def __len__(self,ind):
		return len(self.dummlist)


	def __delitem__(self,ind):
		del self.dummlist[ind]


	def __bool__(self):
		return self.dummlist==[]


class Document(umdoc.Document):

	def __init__(self,request,*args,**kwargs):
		umdoc.Document.__init__(*(self,request)+args, **kwargs)

		self.suppressTermine = 1
		self.dl = Dummlist()
		try:
			self.date = os.path.getmtime(conf.dummpath)
		except:
			self.date = time.time()
		self.footstuff.append("<p>Wissenschaftlich und objektiv, deshalb nicht"+
			" dem Pressegesetz unterworfen.</p>")

		if request.path_info.endswith("addurl"):
			self.addurl(request.form)
		elif request.path_info.endswith("removeurl"):
			self.removeURL(request.form)
		elif request.path_info.endswith("theurl"):
			self.oneResult(request.form)
		elif request.path_info.endswith("interval"):
			self.interval(request.form)
		else:
			self.mainpage()

	def addurl(self,form):
		raise conf.Error("Hinzufügen haben wir abgeschaltet – das ist"
			" zu viel Ärger im heutigen Netz.")
		if "addurl" not in form:
			raise conf.Error("Du hast das Formular falsch ausgefüllt.")
		self.dl.add(form["addurl"].value)
		self.dl.save()

		self.title = "Eintrag hinzugefügt"
		self.date = time.time()
		self.moreheaders.append("Refresh: 2;URL=/unimut/dummschwaetz")
		self.data = """<P>Der Eintrag ist in der URL-Liste.  Der Effekt auf das
		<a href="/unimut/dummschwaetz">Ranking</a>
		sollte eigentlich auch ohne ein explizites Reload zu sehen sein.  Wer
		ein paar Sekunden wartet, wird automatisch an die betreffende Seite
		gebracht.</P>"""

	def removeURL(self, form):
		if not self.request.prefs.isMaintainer:
			raise conf.PermissionError("Das darfst du nicht")
		if "removeurl" not in form:
			conf.Error,"Du hast das Formular falsch ausgefüllt."
		self.dl.remove(form["removeurl"].value)
		self.dl.save()

		self.title = "Eintrag gelöscht"
		self.date = time.time()
		self.moreheaders.append("Refresh: 2;URL=/unimut/dummschwaetz")
		self.data = """<p>Der Eintrag %s wurde aus der URL-Liste entfernt.
		Der Effekt auf das
		<a href="/unimut/dummschwaetz">Ranking</a>
		sollte eigentlich auch ohne ein explizites Reload zu sehen sein.  Wer
		ein paar Sekunden wartet, wird automatisch an die betreffende Seite
		gebracht.</p>"""%form["removeurl"].value

	def formular(self):
		return """<a name="enter">
			<P><STRONG>Beurteilung einer Webseite:</STRONG><BR></a>
			<FORM METHOD="GET" ACTION="/unimut/dummschwaetz/theurl">
			URL:<INPUT TYPE="TEXT" name="theurl" SIZE=60><BR>
			<INPUT TYPE="SUBMIT" VALUE="Go"> <INPUT TYPE="RESET" VALUE="Clear">
			</FORM>"""

	def mainpage(self):
		self.title = "Das neoliberale Dummschwätzranking"

		self.data.append(self.formular())
		self.data.append(
			"""<H2>Aktuelle Ergebnisse</H2>
			<P>Das folgende Histogramm stellt die Verteilung der Dummschwätzquotienten
			für die bisher in die Liste aufgenommenen Dokumente dar.
			Näheres zur wissenschaftlichen Relevanz des Dummschwätzquotienten
			findet sich in <a href="/unimut/archiv/um154.html#art12">Laguna,
			Schätzer (1998)</a>.</P>
			<P>Ein Klick auf
			die Beschriftung gibt eine Tabelle der in der entsprechenden Quantile
			enthaltenen Dokumente.  NutzerInnen mit einer schnellen Netzwerkverbindung
			und einem stabilen Browser
			steht auch die <a href="/dummschwaetz/interval?lower=0">ganze Tabelle</a>
			(groß!) auf einer Seite zur Verfügung.</P>""")
		self.data.append(self.dl.htmlHisto())
		self.data.append("""<H2>Literatur</H2>
			<P>Laguna, R.J., Schätzer, M.A., 1998: <EM>Postmoderne Linguistik</EM>,
			UNiMUT <STRONG>154</STRONG>, 5</P>
			<p>Anonymus, 2003: <em>Die Wahrheit über den Dummschwätzquotienten</em>,
			<a href="/unimut/features/dummschwaetz-wahrheit.html">http://unimut.uni-hd.de/unimut/features/dummschwaetz-wahrheit.html</a>.</p>""")
	
	def oneResult(self,form):
		"""
		Ein einzelnes Resultat berechnen und ausgeben"""

		try:
			d = Dummdok(form["theurl"].value)
		except KeyError:
			raise conf.Error("Hier wird nicht betrogen")
			
		self.title = "Das neoliberale Dummschwätzrating: Bewertung von %s"%d.url
		try:
			d.compute()
			self.data.append("""<P>Das Dokument unter</P><P ALIGN="CENTER">%(url)s</P>
			<P>hat den wissenschaftlich ermittelten Dummschwätzquotienten
			DQ<SUB>%(version)1.2f</SUB></P>
			<P ALIGN="CENTER"><FONT SIZE=+3>%(rating)3.1f</FONT></P>"""%vars(d))
			self.data.append("""<P>Wenn du dieses Ergebnis der Nachwelt erhalten
			willst, <a href="/unimut/dummschwaetz/addurl?addurl=%s">"""%(
			urlparse.quote_plus(d.url)))
			self.data.append('füge es der Dummschw&auml;tz-Urlliste hinzu.</a></P>')
		except (IOError, TypeError) as exc:
			self.data = [f"""<P ALIGN="CENTER">Sorry --
				unter der URL finde ich nichts Verwertbares ({exc}).<BR>
				Probiers nochmal:</P>"""]
		except ValueError:
			self.data = ["""<P ALIGN="CENTER">Ooops --
				Dieses Dokument kann -- vermutlich aufgrund von manglender
				Information -- nicht gerankt werden.<BR>
				Probiers nochmal:</P>"""]
		self.data.append(self.formular())

	def interval(self,form):
		"""
		Gibt eine Tabelle der Ergebnisse in einem Intervall von
		Dummschwätzquotienten aus"""

		if "lower" in form:
			lower = float(form["lower"].value)
		else:
			raise conf.ParameterError("Parameter lower fehlt")
		if "upper" in form:
			upper = float(form["upper"].value)
			upstring = repr(upper)
		else:
			upper = verybig
			upstring = "unendlich"

		self.title = "Dummschwätzranking: Von "+repr(lower)+" bis "+upstring
		self.data.append("<P>Folgende Einträge mit DQ="+repr(lower)+"..."+upstring)
		self.data.append("""finden sich im Augenblick in der Datenbank des
			<a href="/unimut/dummschwaetz">Projekts Dummschwätzquotient</a>:
			</P>""")
		self.data.append(self.dl.htmlListe(lower,upper))
		self.data.append(self.formular())


if __name__=="__main__":
	dl = Dummlist()
#	dl.remove("http://erotik-expert.de/")
#	dl.save()
#	print dl.htmlHisto()
	for d in dl:
		print("Re-computing %s"%d.getUrl())
		try:
			d.compute()
		except ValueError:
			pass
	dl.clean()
	dl.save()
