# -*- encoding: iso-8859-1 -*-
"""
Ein mixin f�r Document-Klassen, die XML erzeugen wollen
"""

class XMLMixin:
	"""Die einbettende Klasse kriegt ein xmldata-Attribut (das sie auch
	manipulieren darf, wenn sie wei�, was sie tut).  Das fertige
	Dokument kann sie mit getXMLDoc ziehen.

	Wir wollen hier eigentlich nur noch Unicode-Strings sehen.
	Alles andere kann funktionieren, kann aber auch nicht.
	"""
	def __init__(self):
		self._elStack = []
		self.xmldata = [u'<?xml version="1.0"?>']

	def openElement(self, elName, atts=None):
		attString = ""
		if atts is not None:
			attString = " "+(u" ".join([u'%s="%s"'%(attName, attVal) 
				for attName, attVal in atts.items()]))
		self.xmldata.append(u"<%s%s>"%(elName, attString))
		self._elStack.append(elName)
	
	def closeElement(self):
		self.xmldata.append("</%s>"%self._elStack.pop())
	
	def addElement(self, elName, atts=None, content=None):
		self.openElement(elName, atts)
		self.xmldata.append(content)
		self.closeElement()
	
	def getXMLDoc(self):
		return u"\n".join(self.xmldata)
