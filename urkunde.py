#!/usr/bin/env python
# -*- encoding: iso-8859-1 -*-

import random, os, tempfile, re


fillers = {
"orga": [("Die", "Stiftung"),
	("Der", "Fonds"),
	("Der", "Club"),
	("Der", "Verein"),],

"name": ["Sid Vicious",
	"Peter Dullmer-J�rgen Piefke",
	"Roman von Hommelhoff",],

"ehre": ["Umwelt- und Friedens",
	"Friedens- und Umwelt",
	"Innovation"],
	
"preis": [("die", "Medallie"),
	("den", "Preis"),
	("den", "Award"),
	("das", "Fellowship")],

"verdienst": ["besondere Verdienste zur F�rderung",
	"ausgezeichnete Leistungen im Bereich",
	"herausragende Errungenschaften um die Bef�rderung",
],

"quatsch": ["der trans-sozialen Marktwirtschaft",
	"der zygotischen An�mie",
	"der ab�otische Rhotoptie",
	"digitaler Business-L�sungen",
	],
}


templateText = r"""\input german.sty
\font\big=cmssbx10 at 24pt
\font\extrabig=cmssbx10 at 37pt
\font\normal=cmssi10
\nopagenumbers

\centerline{\big %(orga-art)s %(name)s-%(orga)s}
\vskip 1ex plus 1fil
\centerline{\normal verleiht %(preis-art)s}
\vskip 1ex plus 1fil
\centerline{\big %(ehre)s-%(preis)s}
\vskip 1ex plus 0.7fil
\centerline{\normal f�r}
\vskip 1ex plus 0.7fil
{\leftskip=0cm plus 1fil\rightskip=0cm plus 1fil\parfillskip=0pt
\baselineskip=30pt
\big %(verdienst)s %(quatsch)s\par}
\vskip 1ex plus 1fil
\centerline{\normal im Jahr 2004}
\vskip 1ex plus 1fil
\centerline{\normal an}
\vskip 1ex plus 1fil
\centerline{\extrabig Markus Demleitner}
\vskip 3ex plus 3fil
\line{Harry Potter\hfil Der kleine Drache Grizu}
\vskip 0pt plus 0.3fil
\eject\end
"""

latin1ToTeX = {
"�":'"a',
"�":'"o',
"�":'"u',
"�":'"A',
"�":'"O',
"�":'"U',
"�":'"s',
}

def generateParts(fillers):
	parts = {}
	for key in fillers:
		sel = random.choice(fillers[key])
		if type(sel)==type(""):
			parts[key] = sel
		else:
			parts[key] = sel[-1]
			parts[key+"-art"] = sel[0]
	return parts

def generateText(templateText, fillers):
	return templateText%generateParts(fillers)

def texToPdf(texSource, filesToLinkBackTo=[]):
	owd = os.getcwd()
	wdName = tempfile.mktemp("texToPs")
	try:
		os.makedirs(wdName)
		os.chdir(wdName)
		for fname in filesToLinkBackTo:
			print(fname)
			os.symlink(os.path.join(owd, fname), fname)
		f = open("tmp.tex", "w", encoding="iso-8859-1")
		f.write(re.sub("("+"|".join(latin1ToTeX)+")",
			lambda mat: latin1ToTeX[mat.group(1)],
			texSource))
		f.write("\n")
		f.close()
		os.system("tex tmp.tex")
		# todo: subprocess nehmen
		pdfCode = os.popen("dvips -o - tmp.dvi | ps2pdf - -").read()
	finally:
		os.chdir(owd)
		os.system("rm -rf '%s'"%wdName)
	return pdfCode

if __name__=="__main__":
	open("zw.pdf", "w").write(texToPdf(generateText(templateText, fillers)))
