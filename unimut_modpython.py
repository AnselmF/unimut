# -*- coding: iso-8859-1 -*-
"""
Der zentrale Dispatcher f�r den UNiMUT -- analysiert die Anfragen
und verteilt sie auf die einzelnen Module.

Diese Version ist zur Verwendung mit mod_python gedacht.  F�r den
nackten Apache ist nph-unimut.py da.
"""

from mod_python import apache, util
import os, sys, re, cgi, string, urllib
import conf, time, traceback, umutil, upload
import localconf
# !!! Nicht prefs importieren (siehe unten)


import UserString

class StringWithValue(UserString.UserString):
  def __init__(self, val):
    UserString.UserString.__init__(self, val)
    self.value = val


class ValueWithValue:
	def __init__(self, val):
		self.value = val


class MyFieldStorage(util.FieldStorage):


	def __getitem__(self, key):
		val = util.FieldStorage.__getitem__(self, key)
		if hasattr(val, "file"):
			return val
		else:
			return ValueWithValue(val)


class Request:
	
	def __init__(self, mPReq):
		self._modpyServer = mPReq.server
		self.requestMethod = mPReq.method

		self.unparsedURI = mPReq.unparsed_uri
		self.path_info = re.sub(r"\?.*", "", self.unparsedURI)

		self.httpHeaders = mPReq.headers_in

		self.remoteHost = mPReq.connection.remote_host
		if not self.remoteHost:
			self.remoteHost = mPReq.connection.remote_addr[0]

# How do I know if uri is valid?
#		elif os.environ.has_key("PATH_INFO"):
#			self.path_info = os.environ["PATH_INFO"]

		self.query_args = mPReq.args

		self.path_info = re.sub("/+","/",self.path_info)
		if self.path_info.startswith("/"):
			self.path_info = self.path_info[1:]
		if self.path_info.startswith("unimut/"):
			self.path_info = self.path_info[7:]
		if self.path_info=="unimut":
			self.path_info = "/"

		self.gzipOk = None

		try:
			if req.headers_in.has_key("ACCEPT-ENCODING"):
				if req.headers_in["ACCEPT-ENCODING"].find("deflate")!=-1:
					self.gzipOk = 1
			if req.headers_in.has_key("USER-AGENT"):
				if req.headers_in["USER-AGENT"].find("MSIE")!=-1:
					self.gzipOk = None
		except:
			pass

		self.form = MyFieldStorage(mPReq)

		self.document = None


	def defaultaction(self):
		d = upload.getUploadedDoc(self, self.path_info)
		if d:
			self.document = d
			return
		source = os.path.join(conf.rootpath,self.path_info)
		if os.path.exists(source):
			if os.path.isdir(source):
				raise conf.Redirect(os.path.join("/unimut",
					os.path.join(self.path_info,"index.html")))
			if os.path.isfile(source) and not source.endswith(".html"):
				import umbin
				self.document = umbin.Document(self,sourcefile=source)
			else:
				import umdoc
				self.document = umdoc.Document(self,sourcefile=source)
		else:
			raise conf.UnknownPath,"URI not found"


	def cgiCompat(self):
		"""behandelt ein paar requests des alten Stils.
		"""
		import redirect
		if self.path_info.startswith("cgi-bin/abnjs.cgi"):
			self.document = redirect.Document(self,"/unimut/abkuerz/nojs?wort=%s"%
				self.query_args)
		elif self.path_info.startswith("cgi-bin/pyschwob"):
			self.document = redirect.Document(self,"/unimut/schwob?schwob_url=%s"%
				urllib.quote_plus(self.query_args))
		elif self.path_info.startswith("cgi-bin/aktuell.py"):
			self.document = redirect.Document(self,"/unimut/aktuell?%s"%
				self.query_args)
		elif self.path_info.startswith("cgi-bin/pysearch") or \
			self.path_info.startswith("cgi-bin/searcnew.cgi"):
			self.document = redirect.Document(self,"/unimut/search?%s"%
				self.query_args)
		elif self.path_info.startswith("cgi-bin/dummsch.py"):
			self.document = redirect.Document(self,"/unimut/dummschwaetz")
		elif self.path_info.startswith("cgi-bin/dummsch"):
			self.document = redirect.Document(self,"/unimut/dummschwaetz")
		else:
			raise conf.UnknownPath,"CGI ist abgebrannt"


	def dispatch(self):
		path = self.path_info
		module = string.split(path,"/")[0]
		if localconf.isMirror:
			if module=="robots.txt":
				import robots
				self.document = robots.Document(self)
				return
			if module!="schwob":
				if {"pix":1, "img":1, "images": 1, "pics": 1, "icons": 1
					}.has_key(module):
					raise conf.Error, "Un�bersetzter Pfad -- Mist!"
				raise conf.Redirect, umutil.urljoin(conf.homeurl, self.unparsedURI)

		if module and re.match(r"\w+$",module):
			try:
				exec "import %s"%module in {}
				self.document = sys.modules[module].Document(self)
				return
			except (ImportError, SyntaxError):
				pass
		if module=="cgi-bin":
			self.cgiCompat()
			return
		self.defaultaction()

	def log(self, s):
		apache.log_error(s, apache.APLOG_ERR, self._modpyServer)


def output(doc, req):
		"""das hier ist ein h�sslicher Hack, weil mod_python mir nicht
		erlaubt, getHead() zu verwenden (jedenfalls nicht wirklich)
		und ich drum wild in den Eingeweiden von Document rumgrabbeln
		muss.  Sollte mal anders werden
		"""
		# Das hier zuerst machen, sonst ist der header schon drau�en,
		# bevor hier drin b�se Exceptions kommen.
		if not doc.headerOnly:
			docData = doc.getData()
			if doc.replaceAbbrevs:
				import abkuerz
				docData = abkuerz.insabkNew(docData)
			doGzip = doc.request.gzipOk and not doc.binary

			stuff = doc.getTop()+\
				docData+\
				doc.getFooter()
			if doGzip:
				import zlib
				doc.moreheaders.append("Content-encoding: deflate")
				stuff = zlib.compress(stuff)
			doc.moreheaders.append("Content-length: %d"%len(stuff))
		req.status = doc.status[0]
		req.content_type = doc.content_type
		if doc.date:
			req.headers_out["Last-Modified"] = time.strftime(
				"%a, %d %b %Y %H:%M:%S GMT",time.gmtime(doc.date))
		for h in doc.moreheaders:
			key, val = map(string.strip, h.split(":", 1))
			req.headers_out[key] = val
		req.send_http_header()
		if not doc.headerOnly:
			req.write(stuff)


def handler(req):
	# so -- das ist jetzt eine Krankheit.  Ich darf hier prefs nicht
	# importieren, um den Cookie hier reinzukriegen...
	try:
		if req.headers_in.has_key("COOKIE"):
			os.environ["HTTP_COOKIE"] = req.headers_in["COOKIE"]
			import prefs
			prefs.makePrefs()
		else:
			import prefs
			prefs.setDefaults()
		try:
			request = Request(req)
			if req.headers_in.has_key("HOST") and not localconf.isMirror:
				if req.headers_in["HOST"].startswith("www"):
					raise conf.Redirect, req.unparsed_uri
		#	import patProtest
		#	if patProtest.checkCookie(request):
			request.dispatch()
			if not request.document:
				raise conf.Error,"Diese URL hat mich verwirrt" # Shouldn't happen
		except conf.Redirect,newurl:
			import redirect
			request.document = redirect.Document(request, str(newurl))
		except conf.UnknownPath:
			import umerr
			request.document = umerr.notFoundErr(Request(req))
		except conf.ParameterError, msg:
			import umerr
			request.document = umerr.parameterErr(Request(req), message=msg)
		except conf.UserError, msg:
			import umerr
			request.document = umerr.userErr(Request(req), message=msg)
		except conf.Error,msg:
			import umerr
			request.document = umerr.umErr(Request(req),message=msg)
		request.document.done()
		output(request.document, req)
		del request.document # Sollte memory leak fixen
		del request
	except:
		import StringIO
		strFile = StringIO.StringIO()
		traceback.print_exc(None, strFile)
		offurl = ""
		offpar = ""
		try:
			offurl = req.path_info
			f = MyFieldStorage(req)
			offpar = "  ".join(["%s=%s"%(n, f[n]) for n in f.keys()])
			if req.headers_in.has_key("REFERER"):
				refstr = "\nfrom %s"%req.headers_in["REFERER"]
			else:
				refstr = ""
			hoststr = " [host einf�gen]"
			apache.log_error("%s ---- %s/%s%s %s\n%s\n"%(
				time.asctime(time.localtime(time.time())),
				request.path_info,
				`[(a, request.form[a]) for a in request.form.keys()]`,
				refstr, hoststr, strFile.getvalue()), apache.APLOG_CRIT,
				req.server)
		except (UnboundLocalError, ValueError), msg:
			apache.log_error("So kaputt: %s"%strFile.getvalue(),
				apache.APLOG_CRIT, req.server)
			apache.log_error("Zweiter Fehler: %s"%msg, apache.APLOG_CRIT, req.server)
		req.status = 500
		req.content_type = "text/html"
		req.send_http_header()
		try:
			req.write("""<HEAD><TITLE>Uff: Fehler</TITLE></HEAD>
				<BODY>
				<H1>Schwerer Fehler</H1>
				<P>Mir gehts schlecht: Deine Anfrage hat mich so verwirrt, dass
				ich nur noch diese Meldung ausspucken kann.  Damit anderen Leuten
				das Schicksal, sowas Schlimmes zu sehen erspart bleibt, solltest
				du dem <a href="mailto:%s">UNiMUT-Webmenschen</a> Bescheid sagen und
				auf jeden Fall Folgendes in die Mail schreiben: </p>
				<p>URL: %s</p>
				<p>URI: %s</p>
				<p>Par: %s</p>
				<PRE>%s</PRE>
				<p>Vielen Dank.</p></body>"""%(conf.maintainer, offurl,
				req.unparsed_uri,
				offpar,
				re.sub("\n","<BR>",
				re.sub(">","&gt;",
				re.sub("<","&lt;",
				strFile.getvalue())))))
		except IOError:
			apache.log_error("Client hat die Verbindung geschlossen.  Mist.",
				apache.APLOG_WARNING, req.server)
	return apache.OK
