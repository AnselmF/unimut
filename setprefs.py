# -*- encoding: iso-8859-1 -*-
"""
Die Logik zur Auswertung evenuteller Cookies sowie zum Setzen des Cookies
"""

import os, time
from urllib import request as urlrequest

import conf, umdoc, umutil


class Document(umdoc.Document):
	
	def __init__(self, request):
		umdoc.Document.__init__(self, request)
		if request.path_info.endswith("filledform"):
			self.handleForm(self.request.form)
		elif request.path_info.endswith("giveright"):
			self.giveright(self.request.form)
		elif request.path_info.endswith("tipform"):
			self.serveTipForm()
		elif request.path_info.endswith("printwand"):
			self.printWand(self.request.form)
		elif request.path_info.endswith("settip"):
			self.setTip(self.request.form)
		else:
			self.prefsDiag()

	def serveTipForm(self):
		if not self.request.prefs.isMaintainer:
			raise conf.PermissionError("Das darfst du nicht")
		self.title = "Tip setzen"
		self.data.extend([
			'<form method="post" action="/unimut/%s/settip">'%__name__,
			'<textarea cols="72" name="aktuTip">'
				'%s</textarea>'%umutil.getAktuellTip(),
			'<br><input type="submit" value="Ok"></form>'])

	def printWand(self, form):
		"""das war einmal.  Und das lief extern, weil die Kiste, auf der der
		UNiMUT-Server lief, zu alt war f�r die Wandzeitungsproduktion.

		Inzwischen w�re das wahrscheinlich sehr einfach, aber wer will das noch?
		"""
		if not self.request.prefs.isMaintainer:
			raise conf.PermissionError("Das darfst du nicht")
		if "print" in form:
			self.title = "Wandzeitung wird gedruckt"
			self.data = ("<p>Die Wandzeitung sollte demn�chst am Kopierer"
				" rauskommen.  Wenn nicht, ruft Demi (3248) an und probierts nicht"
				" einfach nochmal (wir wollen nicht drei�ig vermurkste"
				" Druckauftr�ge)</p>\n"
				"<p>Vergesst nicht, wieder A4-Papier einzulegen und am"
				" gr�nen R�dchen einzustellen, wenn ihr fertig seid.</p>")
			stuff = urlrequest.urlopen("http://tucana.cl.uni-heidelberg.de/"
				"cgi-bin/makeWand.py").read()
			os.popen('gs -sPAPERSIZE=a3 -sDEVICE=ljet4 -sOutputFile=- '
				' -dBATCH -dNOPAUSE -q - | lpr -Pkopierer', "w").write(stuff)
		else:
			self.title = "Wandzeitung drucken"
			self.data = ('<p><em>Das hier hat nur Sinn, wenn ihr gerade im'
				' ZFB seid.</em></p>\n'
				'<p>Legt A3-Papier in den oberen Schacht des Kopierers ein'
				' und stellt am gr�nen R�dchen A3 ein.'
				' Folgt dann <a href="/unimut/%s/printwand?print=yes">'
				'diesem Link</a>.  Wenn alles gut geht, kommt'
				' die aktuelle Wandzeitung danach aus dem Kopierer.</p>')%__name__
		
	def setTip(self, form):
		if not self.request.prefs.isMaintainer:
			raise conf.PermissionError("Das darfst du nicht")
		if "aktuTip" in form:
			umutil.setAktuellTip(form["aktuTip"].value)
		else:
			umutil.setAktuellTip("")
		self.title = "Tipp gesetzt"
		tip = umutil.getAktuellTip()
		if tip:
			self.data.append(
				"<p>Beim n�chsten Mailing wird Folgendes mitverschickt:</p>")
			self.data.append("<pre>%s</pre>"%tip)
		else:
			self.data.append("<p>Beim n�chsten Mailing wird kein Tipp"
				" verschickt.</p>")
		self.data.append('<p><a href="%s/tipform">Nochmal �ndern</a></p>'%__name__)

	def setPrefs(self):
		"""encodes the userpreference in a string, puts it into a header field
			 and outputs the current settings"""
		cookie = ""
		self.title = "Voreinstellungen speichern"
		reslist = ["<h1>Voreinstellungen</h1>"]
		reslist.append(
			"""<p>Ich versuche gerade, die Voreinstellungen zu speichern.
				Wenn das nicht funktioniert, ist dein Browser wahrscheinlich
				so eingestellt, dass er keine Cookies von diesem Server nimmt,
				oder der Cookie wurde von irgendwem zwischendrin aufgegessen.
				So oder so, es ist nicht unser Fehler.</p>
				<p>Die augenblicklichen Voreinstellungen sind:
				<ul>""")
		
		prefs = self.request.prefs
		if self.request.prefs.isMaintainer:
			cookie = cookie+"#"
			reslist.append("<li>Du hast Schreibrecht</li>")
		cookie = cookie+"a"+repr(prefs.numArt)
		reslist.append("<li>UM aktuell-Artikel pro Seite: %d</li>"%
			prefs.numArt)

		if prefs.eMailAddress:
			if prefs.eMailAddress=="forget":
				self.moreheaders.append(
					"Set-Cookie: email=%s;path=/;expires=%s;"%(prefs.eMailAddress,
						time.strftime("%A, %d-%b-%Y %H:%M:%S %Z",
							time.gmtime(time.time()-61400000))))
				reslist.append("<li>E-Mail-Adresse: Vergessen</li>")
			else:
				reslist.append("<li>E-Mail-Adresse: %s</li>"%prefs.eMailAddress)
				self.moreheaders.append(
					"Set-Cookie: email=%s;path=/;expires=%s;"%(prefs.eMailAddress,
						time.strftime("%A, %d-%b-%Y %H:%M:%S %Z",
							time.gmtime(time.time()+61400000))))
		self.moreheaders.append(
			"Set-Cookie: prefs=%s;path=/;expires=%s;"%(cookie,
			time.strftime(
			"%A, %d-%b-%Y %H:%M:%S %Z",time.gmtime(time.time()+61400000))))
		reslist.append("</ul>")
		self.data = "\n".join(reslist)
		self.date = time.time()
		return 0

	def prefsDiag(self):
		"""
		gibt den Dialog zur�ck, mit dem mensch die Voreinstellungen setzen kann"""

		self.title = "Voreinstellungen"
		reslist = ["""<p>Mit folgenden Einstellungen k�nnt ihr den UNiMUT
			etwas personalisieren.  Die Daten, die dabei anfallen,
			werden wir nicht weitergeben.  Wenn ihr keine E-Mail-Adressen
			angebt, ist es auch nicht m�glich, damit "Sessions" zu rekonstruieren,
			da keine eindeutigen Kennungen vergeben werden (wir versuchen
			so einen Quatsch aber nicht mal).</p>
			<p>Die E-Mail-Adresse braucht ihr nur anzugeben, wenn ihr
			die Benachrichtigungs-Funktion bei den Terminen benutzen
			wollt.</p>
			<P>Damit das funktioniert, m�ssen wir einen Cookie bei euch speichern.
			Wer nicht wei�,
			worum es da geht, kann sich bei den
			<a href="http://www.junkbusters.com/ht/en/cookies.html">Junkbusters
			informieren</a>."""]

		prefs = self.request.prefs
		reslist.append(
			"""<FORM ACTION="/setprefs/filledform" METHOD="GET">
			<P><INPUT TYPE="HIDDEN" NAME="/setprefs" VALUE="yes">
			<TABLE BORDER=3>
			<TR><TD COLSPAN=2>
			<p>Eigenes Stylesheet:<br><input type="text" size="60" name="stylesheet"
				value="%(stylesheet)s"><br> (Funktioniert noch nicht)</p>
			</TD></TR>
			<TR><TD VALIGN="TOP" colspan=2>
			<P>Im UNiMUT aktuell
			<INPUT TYPE="INT" SIZE=4 NAME="prefsnumart" VALUE="%(numArt)d">
			Artikel auf einmal anzeigen</P>
			</TD>
			</TR>
			<TABLE>
			"""%{"numArt":prefs.numArt,
				"stylesheet": "http://unimut.fsk.uni-heidelberg.de/unimut/umstyle.css"})
		reslist.append(
			'<P ALIGN="CENTER"><INPUT TYPE="SUBMIT" VALUE="OK"></P></FORM>')
		reslist.append("""<PRE> \n \n \n \n \n \n \n \n</PRE>
			<FORM METHOD="POST" ACTION="/setprefs/giveright">
			<TABLE BORDER=3 CELLPADDING=10 ALIGN="CENTER">
				<TR><TD><P>Schreibrecht f�r UNiMUT aktuell. Passwort:
				<INPUT TYPE="password" NAME="geethatscool" size=10>&nbsp;
				<INPUT TYPE="SUBMIT" VALUE="OK"><BR>
				(zum Entfernen des Schreibrechts einfach falsches Passwort eingeben)</P>
				</TD></TR>
			</TABLE>
			</FORM>""")
		self.data = "\n".join(reslist)

	def handleForm(self,form):
		"""liest die Voreinstellungen aus dem Formular und setzt das cookie
		"""
		global numArt,menuFsize,black
		global eMailAddress
		try:
			numArt = int(form["prefsnumart"].value)
			if "email" in form:
				eMailAddress = form["email"].value
		except (KeyError,ValueError):
			raise conf.Error('Formular ist falsch ausgef�llt oder du hast '+\
				'beschissen. Versuchs nochmal.')
		self.setPrefs()

	def giveright(self, form):
		if "geethatscool" not in form:
			raise conf.Error("Na ja, das war ein schwacher Versuch.")
		if form["geethatscool"].value=="josua":
			self.request.prefs.isMaintainer = 1
		else:
			self.request.prefs.isMaintainer = None
		self.setPrefs()
