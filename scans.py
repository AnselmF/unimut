# -*- coding: iso-8859-1 -*-
"""
Scans f�r alte UNiMUTe als GIFs oder PS rausgeben
"""

import subprocess
import os
import re

import conf
import umdoc


class Document(umdoc.Document):
	
	def __init__(self,request):
		umdoc.Document.__init__(self,request)
		mat = re.match(
			"scans/(?P<format>(p(s|df)/)?)um(?P<id>\d\d\d)(.(?P<pg>\d\d))?",
			request.path_info)
		if not mat:
			raise conf.ParameterError("Syntaxfehler in den Parametern.")

		self.binary = 1

		if mat.group("format")=="ps/":
			self.doPS(mat.group("id"))
			return

		if mat.group("format")=="pdf/":
			self.doPS(mat.group("id"),asPDF=1)
			return

		if "scale" in request.form:
			scale = int(request.form["scale"].value)
		else:
			scale = 3
		vol = int(mat.group("id"))
		page = int(mat.group("pg"))

		source = os.path.join(conf.headpath,"scans/um%03d.%02d"%(vol,page))
		if not os.path.exists(source):
			raise conf.UnknownPath("Das finde ich nicht.")

		self.data = subprocess.run([
			os.path.join(conf.cgipath, "t2gif"),
			"-p",
			"-scale",
			str(scale),
			source],
			capture_output=True,
			check=True).stdout
		self.content_type = "image/png"
		self.sourcefile = source

	def doPS(self,id,asPDF=0):
		import glob

		self.content_type = "application/pdf"
		progname = "t2pdf"
		scanpat = os.path.join(conf.headpath,"scans/um%s.*"%id)
		pagelist = glob.glob(scanpat)
		if not pagelist:
			raise conf.UnknownPath("UNiMUT %s hat keine scans"%id)
		self.data =  subprocess.run([os.path.join(conf.cgipath, progname)
			]+pagelist, capture_output=True, check=True).stdout
		self.sourcefile = pagelist[0]

	def getTop(self):
		return b""

	def getFooter(self):
		return b""
