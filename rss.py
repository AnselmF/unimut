# -*- encoding: iso-8859-1 -*-
"""
Hier erzeugen wir ein RSS-Dokument aus ein paar Teilen des UNiMUT.
"""

# crippled for python 2.1 -- use -unicode as soon as you can.
# eieiei.  Ich glaube, das portiere ich nicht mehr...

import os, time
import xmlmixin, umdoc, conf, umutil


class Document(umdoc.Document, xmlmixin.XMLMixin):
	def __init__(self, request, *args, **kwargs):
		umdoc.Document.__init__(self, request, *args, **kwargs)
		xmlmixin.XMLMixin.__init__(self)
		self.linksInChannel = []
		self.date = time.time()
		self.content_type = "text/xml"
		self.xmldata.append('<rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"'
			' xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'
			' xmlns="http://purl.org/rss/1.0/">')
		self._addAktuellItems()
		self._addTerminItems()
		self._addChannel()
		self.xmldata.append("</rdf:RDF>")

	def _addLinks(self):
		for link in self.linksInChannel:
			self.addElement("rdf:li", atts={"rdf:resource": link})

	def _addChannel(self):
		self.openElement("channel", {'rdf:about': 'http://unimut.uni-hd.de'})
		self.addElement("title", content="Heute am UNiMUT aktuell")
		self.addElement("link", content="http://unimut.uni-hd.de/aktuell")
		self.addElement("description", content="UNiMUT -- Zeitung (an) der"
			"Uni Heidelberg")
		self.openElement("items")
		self.openElement("rdf:Seq")
		self._addLinks()
		self.closeElement()
		self.closeElement()
		self.closeElement()

	def _addRssItem(self, link, title, description, date=None):
		self.openElement("item", {"rdf:about": link})
		self.addElement("link", content=link)
		self.addElement("title", content=title)
		self.addElement("description", content=description)
		self.linksInChannel.append(link)
		if date is not None:
			self.addElement("dc:date", content=time.strftime(
				"%Y-%m-%dT%H:%M:%S+00:00", time.gmtime(date)))
		self.closeElement()

	def _addAktuellItems(self):
		import aktuell
		# Nur aktuells der letzten sieben Tage
		thresh = time.time()-24*3600*7
		flist = filter(lambda a, thresh=thresh: a>thresh,
			map(int, aktuell.getArtList()[-5:]))
		for artNo in flist:
			art = aktuell.Aktuell(artNo)
			self._addRssItem(
				link = os.path.join(conf.homeurl, "aktuell/%d"%artNo),
				title = art.title,
				description = umutil.killHtml(art.body
					)[:300]+"...",
				date=artNo)

	def _addTerminItems(self):
		return
		import UmTerminListe
		utl = UmTerminListe.UmTerminListe()
		for term in utl.getSortedData(None)[0]:
			self._addRssItem(
				link = term.getUmURL(),
				title = "Termin: "+umutil.killHtml(term["kurztext"
					]),
				description = "(%s, %s, %s)"%(term["datum"], term["zeit"],
					umutil.killHtml(term["ort"])))
	
	def getData(self):
		return self.getXMLDoc()
