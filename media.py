# -*- coding: iso-8859-1 -*-
"""
Dieses Modul managet gro�e bin�re Dateien, die im separaten Verzeichnis
.../media liegen.
"""

import sys, os, conf
import umdoc, httpstuff

class Document(umdoc.Document):
	
	def __init__(self, request):
		sourcefile = os.path.join(conf.headpath, request.path_info)
		if not os.path.exists(sourcefile):
			raise conf.UnknownPath("Das finde ich nicht.")
		umdoc.Document.__init__(self,request)
		with open(sourcefile, "rb") as f:
			self.data = f.read()
		self.sourcefile = sourcefile
		self.binary = 1
		self.content_type = httpstuff.getMimeType(sourcefile)

	def getTop(self):
		return b""

	def getFooter(self):
		return b""
