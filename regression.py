"""
Ein paar Regressionstests für den UNiMUT-Kram.

Ordentliche Unittests kriegt der Code in diesem Leben nicht mehr, aber fürs
Porten habe ich das hier.

Um einen Test zu definieren, leite eine Klasse von UnimutTest ab.  Im
Idealfall musst du nur request_url und expectation (eine Liste von strings,
die in der Ausgabe sein sollen) ändern.

Warnung: anders als der Rest von diesem Code soll das hier alles in
utf-8 sein.
"""

import io
import os
import re
import sys
import unittest

import conf
import wsgi_unimut
wsgi_unimut.LOGGING = False

class UnimutTest(unittest.TestCase):
	"""Die Basisklasse von allen Tests gegen den UNiMUT.

	Sie ruft in einem class-fixture das WSGI-Interface mit ein paar
	absoluten Basics auf, insbesondere request_url (ohne host- und query-part)
	und, wenn gewünscht, query_string.

	Das ist automatisch ein Test dabei, der auf alle strings oder bytes
	(muss aber in sich homogen sein) in expectations testet.

	Um Maintainer-Seiten zu produzieren: cookie auf etwas mit prefs=# drin setzen.
	"""
	request_url = None
	query_string = ""
	expectations = []
	cookie = None

	@classmethod
	def no_write(cls, stuff):
		raise NotImplementedError("Unimut doesn't write")

	@classmethod
	def start_response(cls, status, headers, exc_info=None):
		if exc_info:
			raise
		cls.status = status
		cls.response_headers = dict((k.lower(), v) for (k,v) in headers)
		return cls.no_write

	@classmethod
	def setUpClass(cls):
		if cls.request_url is None:
			return
		environ = {
			"REMOTE_HOST": "The Test Suite",
			"REQUEST_URI": cls.request_url,
			"QUERY_STRING": cls.query_string,
			"wsgi.input": io.BytesIO()}
		
		if cls.cookie:
			environ["HTTP_COOKIE"] = cls.cookie

		cls.response = b"".join(frag
			for frag in wsgi_unimut.application(environ, cls.start_response))
		
	def _guess_payload_charset(self):
		mat = re.search(r"charset\s*=(\s*[\d\w-]+)",
			self.response_headers["content-type"])
		if mat:
			return mat.group(1)
		else:
			return "iso-8859-1"

	def _get_response_as_string(self):
		return self.response.decode(self._guess_payload_charset())

	def test_strings_present(self):
		if not self.expectations:
			return

		if isinstance(self.expectations[0], str):
			content = self._get_response_as_string()
		else:
			content = self.response

		try:
			for frag in self.expectations:
				assert frag in content, f"'{frag}' missing"
		except AssertionError:
			with open("failed.data", "wb") as f:
				f.write(self.response)
			raise


class RootForwardTest(UnimutTest):
	request_url = "/"
	expectations = [
		'<h1>Nicht mehr hier...</h1>',
		'<a href="/unimut/wussi">Wusstet Ihr schon...</a><br>']
	
	def test_redirection(self):
		self.assertEqual(self.status[:4], "301 ")
		self.assertEqual(self.response_headers["location"],
			"http://umtest/unimut/index.html")


class RootTest(UnimutTest):
	request_url = "/unimut/index.html"

	expectations = [
		'http-equiv="Content-Type" content="text/html;charset=iso-8859-1"/>',
		'<p id=linkline><a href="/unimut/index.html">[Home]</a>',
		'<h2>UNiMUT im Winterschlaf</h2>']

	def test_status(self):
		self.assertEqual(self.status[:4], "200 ")
		
	def test_encoding_declared(self):
		self.assertEqual(
			self.response_headers["content-type"],
			"text/html;charset=iso-8859-1")


class UnprefixedRootTest(RootTest):
	request_url = "/index.html"


class AbkueTest(UnimutTest):
	request_url = "abkuerz"

	expectations = [
		'<dt>BuFaK</dt>',
		'<a href="/unimut/abkuerz/nojs?wort=ZaPF" ']


class FourOhFourTest(UnimutTest):
	request_url = "jesus.html"

	expectations = [
		'Ich weiß nicht, was ich mit der URL "jesus.html" anfangen',
		'<a href="/unimut/wussi">']

	def test_status(self):
		self.assertEqual(self.status[:4], "404 ")

	def test_encoding_declared(self):
		self.assertEqual(
			self.response_headers["content-type"],
			"text/html;charset=iso-8859-1")


class AktuellTest(UnimutTest):
	request_url = "unimut/aktuell"

	expectations = [
		'<title>UNiMUT aktuell</title>',
		'<p class="subhead">']


class AktuellAdminTest(UnimutTest):
	request_url = "unimut/aktuell"
	cookie = 'prefs=#'

	expectations = [
		'<a href="/unimut/wussi/edit">Wussis ändern</a><br>',
		'<a href="/unimut/aktuell/newrec">Neuer UNiMUT aktuell</a><br>',
		'showAdminMenu']


class AktuellTocTest(UnimutTest):
	request_url = "unimut/aktuell/toc"
	expectations = ['<li><a href="/unimut/aktuell/850474800">RCDS Karlsruhe']



class AktuellMonthTocTest(UnimutTest):
	request_url = "/aktuell/toc"
	query_string = "month=Oct+06"

	expectations = ["font size='-1'>Die Endlichkeit der Exzellenz</font>",
		'<a href="/unimut/aktuell/1161126380">Exzellenzuni - Ende der']


class AktuellArticleTest(UnimutTest):
	request_url = "/unimut/aktuell/864727200"

	expectations = ['Allein der \nTitel "Lehrevaluation und']


class BinaryTest(UnimutTest):
	request_url = "/unimut/images/umtitle.png"

	expectations = [b"PNG", b"IHDR"]

	def test_media_type(self):
		self.assertEqual(
			self.response_headers["content-type"], "image/png")


class SetPrefsFormTest(UnimutTest):
	request_url = "unimut/setprefs"

	expectations = [
		"Damit das funktioniert, müssen wir einen Cookie bei euch speichern"]


class SetPrefsRunTest(UnimutTest):
	request_url = "setprefs/giveright"
	query_string = "geethatscool=josua"

	expectations = [
		"<li>Du hast Schreibrecht</li>"]

	def test_cookie(self):
		self.assertEqual(
			self.response_headers["set-cookie"].split(";")[0],
			"prefs=#a5")


class DummschwaetzTest(UnimutTest):
	request_url= "dummschwaetz"

	expectations = [
		'a href="/unimut/dummschwaetz/interval?lower=110.000000&upper=115',
		'<p>Anonymus, 2003: <em>Die Wahrheit']


class DummschwaetzRankTest(UnimutTest):
	request_url= "dummschwaetz/theurl"
	query_string = "theurl=http%3A%2F%2Fblog.tfiu.de"

	expectations = ["Das Dokument unter", "http://blog.tfiu.de",
		"wissenschaftlich ermittelten Dummschwätzquotienten"]


class DummschwaetzAddTest(UnimutTest):
	request_url= "dummschwaetz/addurl"
	query_string = "addurl=http%3A%2F%2Fblog.tfiu.de"

	expectations = [
		"Hinzufügen haben wir abgeschaltet",
	]


class WahlkrampfTest(UnimutTest):
	request_url = "/unimut/gags/wahlkrampf09"

	expectations = ["Anständige Menschen löhnen?"]


class JuraletteTest(UnimutTest):
	request_url = "/unimut/gags/juralette"

	expectations = ["Das Juralette ist ein Spezialservice des UNiMUT"]


class ArchivTocTest(UnimutTest):
	request_url = "/archiv"

	expectations = [
		'<tr><td><a href="/archiv/um030">UNiMUT 30</a></td><td>(9.1.91)</td>']


class ArchivOldIssueTest(UnimutTest):
	request_url = "/archiv/um015"

	expectations = [
		'li><a href="/unimut/archiv/browse/um015.01">Machen Mensen krank?']


class ArchivNewIssueTest(UnimutTest):
	request_url = "/archiv/um077"

	expectations = [
		'<LI><a href="#art6">"Das (Universitäts-)Boot ist voll!"</a>',
		'<H2>Ungeziefer an der Uni?</H2>']


class ArchivThumbnailTest(UnimutTest):
	request_url = "/unimut/scans/um015.01?scale=16"

	expectations = [b"PNG", b"Tm(A\001"]


class ArchivImageTest(UnimutTest):
	request_url = "/unimut/scans/um015.01"

	expectations = [b"PNG", b"|RDF=\0"]


class ArchivPDFTest(UnimutTest):
	request_url = "/unimut/scans/pdf/um015"

	expectations = [b"%PDF-1.1", b"-E$nDs8W-!s8W%"]

	def test_content_type(self):
		self.assertEqual(
			self.response_headers["content-type"],
			"application/pdf")


class SchwerpunktIndexTest(UnimutTest):
	request_url = "/unimut/schwerpunkt"

	expectations = [
		'href="/unimut/schwerpunkt/anzeigen/unabh%C3%A4ngiges+', # Ja: utf-8
		'">Schwerpunkt Rankings</a><']


class SchwerpunktDisplayTest(UnimutTest):
	request_url = "/unimut/schwerpunkt/anzeigen/Rankings"

	expectations = [
		'<td valign="top">Wenn alle ranken, kann es auch der UNiMUT ',
		'a href="/aktuell/1118819047#Ranking">'
	]


class SearchTest(UnimutTest):
	request_url = "/unimut/search"
	query_string = "pattern=Fl%f6he"

	expectations = ["UNiMUT 126 vom 16.4.97",
		"...rauchvergiftung und <strong>flöhe",
		' href="/unimut/archiv/um126.html">']


class EmptySearchTest(UnimutTest):
	request_url = "/unimut/search"
	query_string = "pattern=krawusel"

	expectations = ["<p>Nichts passendes gefunden.</p>"]


class SchwobIndexTest(UnimutTest):
	request_url = "/unimut/schwob"

	expectations = ["<title>Nicht mehr hier</title>"]

	def test_redirection(self):
		self.assertEqual(self.status[:4], "301 ")
		self.assertTrue(self.response_headers["location"].endswith("/schwob.html"))


class SchwobImmediateTest(UnimutTest):
	request_url = "/unimut/schwob"

	query_string = "transVerb=Die+Erde+war+w%FCst+und+leer"

	expectations = ["Die Erd war wüsch und ler",
		'<form action="/unimut/schwob"']


class SchwobRemoteTest(UnimutTest):
	request_url = "/unimut/schwob"

	query_string = f"schwob_url={conf.homeurl}/archiv/um106&guwirgul=True"

	expectations = ["kommd und lauschd weise Worde über", "zererale Ischämie"]


class SchwobDenyTest(UnimutTest):
	request_url = "/unimut/schwob"

	query_string = "schwob_url=https://www.bundestag.de"

	expectations = ['<h1 id="maintitle">Abgeschaltet</h1>']


class TextDokumenteTest(UnimutTest):
	request_url = "/unimut/dokumente/mannheim.erklaerung.txt"

	expectations = ["den der UNiMUT nicht direkt         I",
		"den 4.12.97 wird die Universität Mannheim durch die",
		"UNiMUT distanziert sich von allem, was hier"]


class HTMLDokumenteTest(UnimutTest):
	request_url = "/unimut/dokumente/spiegel-artikel.html"

	expectations = ["Studenten-Magazine wie Unimut aus Heidelberg",
		'font color="red">Dies hier ist ein Dokument',
		"Dokument verbleiben bei der/dem AutorIn."]


class MediaTest(UnimutTest):
	request_url = "/media/2001-pisa.pdf"

	expectations = [b"%PDF-1.2", b"/JobTicketContents"]

	def test_content_type(self):
		self.assertEqual(
			self.response_headers["content-type"],
			"application/pdf")

# verstecke unsere Basisklasse vor der unittest-Discovery (ansonsten wärs
# auch nicht wild: ein erfolgreicher Test mehr halt).
del UnimutTest


def main():
	if len(sys.argv)==2:
		class_name = sys.argv[1].split(".")[-1]
		test_class = getattr(sys.modules["__main__"], class_name)
		suite = unittest.defaultTestLoader.loadTestsFromTestCase(test_class)
	else:
		suite = unittest.TestLoader().loadTestsFromModule(
			sys.modules["__main__"])

	runner = unittest.TextTestRunner(
		verbosity=int(os.environ.get("TEST_VERBOSITY", 1)))



	result = runner.run(suite)
	if result.errors or result.failures:
		sys.exit(1)
	else:
		sys.exit(0)


if __name__=="__main__":
	main()
