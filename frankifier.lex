%{
        /*
	   Deutsch nach Fr�nkisch �bersetzer
        */
%}
end 	[ \.,!\?\)\(\n]
%%
\<[^>]*\> ECHO;
"&".[^;]*";" ECHO;
Wochen             printf("Woche");
" ge"/[d-gk]       printf(" ");
" ge"/[^"be"h"rn"] printf(" g");
"Ein"/{end}	printf("An");
"Das"/{end}	printf("Des");
" das"/{end}	printf(" des ");
" eines"/{end}  printf(" von a ");
" dieser"/{end} printf(" von dene ");
" des"/{end}    printf(" vom ");
" diesem " printf(" dem ");
" richtig" printf(" recht");
" heb"/("t"|"en") printf(" lupf");
" halten " printf(" heben ");
" etwas"/{end} printf(" ebbes");
" auch"/{end}  printf(" aa");
" n"[ao]"ch"/{end} printf(" no");
"jetzt"     printf("etzad" );
"sp�ter"    printf("nochert");
" Guten Tag" printf(" Gr�� Gott");
"hat"      printf("hod");
"habe"     printf("hob");
"haben"    printf("hobm");
", klar?"  printf(", host mi?");
"ein"e?    printf("a");
"en Sie"   printf("ens");
"jenige"[rsmn]? ;
"welche"[rsmn]? printf("wo");
[Ww]"essen"   printf("%cem sei",yytext[0]);
"he"?"runter" printf("runder");
"hin"("ab"|"unter") printf("runder");
"hinein"     printf("nei");
[Dd]"as"     printf("%ces",yytext[0]);
da?"ran"   printf("dro");
" den"/{end}  printf(" den");
" nein"/{end} printf(" naa");
" sehr"/{end} printf(" fei");
"nicht mehr"  printf("nemmer");
" die"/{end}  printf(" de");
[Nn]"ichts"/{end}  printf("%cix",yytext[0]);
[Nn]"icht"    printf("%ced",yytext[0]);
"tiell"    printf("ziell");
"tion"     printf("zion");
"ach"      printf("ach");
[Mm]"ei"("n"|"ne"|"ns"|"nen")/{end}  printf("%cei",yytext[0]);
[Kk]"ei"("n"|"ne"|"ns"|"nen")/{end}  printf("%ca",yytext[0]);
"einge"	printf("eig");
"Etwas"/{end} printf("Aweng");
"etwas"/{end} printf("aweng");
"bisschen" printf("bisserl");
"gar nicht"/{end}       printf("fei ned");
"�berhaupt nicht"/{end} printf("fei gar ned");
[Aa]"uf jeden Fall"     printf("%cuf aller F�ll",yytext[0]);
"ange"	   printf("ang'");
"anke"     printf("ongg");
"ank"      printf("ong");
"nk"       printf("ng");
"uch"      printf("uch");
"au"       printf("au");
"rs"/{end} printf("rsch");
"ck"       printf("gg");
"eu"       printf("ei");
"ie"/(b|"hs") printf("ia");
"ja"       printf("jo");
"ph"       printf("f");
[Tt]h      printf("%ch",yytext[0]);
"ut"/t?e   printf("uad");
"zu"/[^bdegl-prtz] printf("z");
"p"        printf("b");
"t"        printf("d");
"en wir"/{end} printf("emer");
"st du"/{end} printf("st");
[ DMSdms]"ich"/{end} printf("%ci",yytext[0]);
"mal"/{end} printf("mol");
[bd-gmk]e/{end}  printf("%c",yytext[0]);
("st "[Dd]u|"st")/{end} printf("sch");
"en"/{end}  printf("e");
"!!""!"*    printf(", fei werkli!");
"!"         printf("!");

"Br�tchen"   printf("Weggla");
"Glas"/{end} printf("Gloos");
"Hose"n?     printf("Husn");
%%