# -*- encoding: iso-8859-1 -*-
import os, time
import umdoc

warnStuff="""<head>
<title>Rettet den UNiMUT</title>
<style type="text/css">
<!--
p {text-align:center;
	margin-top: 0.5cm;
	word-spacing:150%%;
	margin-left:150px;
	margin-right:150px}
h1 {text-align: center}
-->
</style>
</head>
<body bgcolor="black" text="white" link="#FF9966" vlink="#FF9966" alink="FF9966">
<h1>Rettet den UNiMUT</h1>
<p>Am 27. August debattiert das Europ�ische Parlament �ber die Patentierbarkeit
von Software und Algorithmen.  W�rden solche Patente in der EU durchsetzbar, m�sste
der UNiMUT vermutlich zumachen, da er gewiss eine Menge <a href="http://patinfo.ffii.org/patente.html">frivoler und trivialer
Patente</a> verletzt ("one-click-shopping" oder die 35-Stunden-Woche).
Wissen k�nnen wir das nicht -- es ist wirklich nicht zu glauben, welcher
Killefit patentiert wird.</p>
<p>Noch ist Zeit, die Freiheit der Software zu verteidigen.  Informiert
euch <a href="http://swpat.ffii.org/group/demo/index.en.html">beim Forum
InformatikerInnen f�r Frieden und gesellschaftliche Verantwortung</a>
und unterschreibt die <a href="http://petition.eurolinux.org/">Petition gegen
Softwarepatente</a>. Es mag auch lohnend sein, <a href="http://wwwdb.europarl.eu.int/ep5/owa/p_meps.short_list?ilg=DE&ictry=DE&ipolgrp=&iorig=">EuropaparlamentarierInnen</a> anzurufen und von ihrer Ahnungslosigkeit erschreckt zu sein.
Der Tipp der Redaktion ist die Neckargem�nderin Diemut Theato von der CDU,
Tel. 06223 3477.</p>
<p>Wenn ihr den UNiMUT normal verwenden wollt, m�sst ihr den Cookie, der
mit dieser Seite kommt, akzeptieren und k�nnt danach den <a href="%s">UNiMUT</a> normal verwenden.  Er expiret in der Nacht vom 27.8. auf
den 28.8., danach bleibt davon nichts mehr auf eurem Rechner.  Wenn ihr das
nicht wollt, m�sst ihr wohl am 28.8. wiederkommen.  Und derweil eifrig
protestieren.</p>
</body>
"""

class Document(umdoc.Document):
	def __init__(self, *args):
		umdoc.Document.__init__(self, *args)
		self.moreheaders.append("Set-Cookie: warnseen=yes;path=/;expires="
			"Thu 28-8-2003 12:00:00 GMT")

	def getData(self):
		return warnStuff%self.request.unparsedURI

	def getTop(self):
		return ""
	
	def getFooter(self):
		return ""


def checkCookie(request):
	if time.time()>1062028800.0:
		return 1
	if os.environ.has_key("HTTP_COOKIE"):
		if os.environ["HTTP_COOKIE"].find("warnseen")!=-1:
			return 1
	request.document = Document(request)
	return 0
